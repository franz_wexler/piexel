﻿
Co zostało skąd splagiatowane?
==============================
-- PORTAL 2 (gra Valve)--
Katapulty
-- GIVE UP (gra Flash) --
Wieżyczki, zapadające się platformy, schemat wejścia i wyjścia, spadające kolce
-- DON'T TOUCH THE SPIKES (gra na Androida) --
Schemat kolorów, minimalistyczny wygląd
-- 99 PROBLEMS (gra na Androida) --
Animacja śmierci gracza
-- VVVVVV (gra Flash z portem do C++) --
Tryb VVVVVV, przenośniki, linie zmiany grawitacji
-- Windows 95 (system operacyjny) --
System pomocy nieprzekazujący realnej pomocy
WIĘKSZOSC NIWYMIENIONYCH POWYŻEJ RZECZY ZOSTAŁA STWORZONA PRZEZ AUTORA SAMODZIELNIE

Notatki o tłumaczeniu slangu informatyka na polski
==================================================
"Balonik mowy" to oficjalne tłumaczenie "speech balloon" (patrz 'https://pl.wikipedia.org/wiki/Dymek_(komiks)')
"Przenośnik" to istniejące słowo (patrz 'https://pl.wikipedia.org/wiki/Przeno%C5%9Bnik')
"Restartować" jest polskim słowem (patrz 'http://sjp.pwn.pl/so/restartowac;4503923.html')
