Piexel levels/debug/ directory                                 
==============================

First levels are smaller than others because tile sizes
have changed. Really first levels use 'P' instead of 'B'
for blocks because this has been changed during rewriting
stolen engine from http://www.nandnor.net/?p=64. Also,
that's why first maps have different proportions. They
have been changed from 13:10 to 16:9 very early.

Entrance and exit are made in very complicated way. From
simple point where player was spawned, it became industrial
door with z-index, player_rel_inter and autoopen_distance.
If you play an early map and door is near ceiling, you
probably are in y = -24. Use cheats to go down.

Because of the new way of reading levels, this README is
also a valid level. To avoid errors, here's an easy path
to follow

                                                      E

       S
	  
BBRBBBBBBBBBRBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
Ignore warning messages of all undefined elements. And
watch out for catapult on your way.

Please DO NOT edit these files without permission.
