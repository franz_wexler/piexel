
level = std = None


def init(**kw):
	global level, std
	level = kw['level']
	std = kw['std']
	
triggered1 = False
def trigger1(**kw):
	global triggered1
	if not triggered1:
		level.schedule_dialog((('player', 19.1), ('omni_circle', 19.2),
							   ('player', 19.3), ('omni_circle', 19.4),
							   ('player', 19.5), ('omni_circle', 19.6),
							   ('player', 19.7), ('omni_circle', 19.8)))
		triggered1 = True

triggered2 = False
def trigger2(**kw):
	global triggered2
	if not triggered2:
		level.schedule_dialog((('player', 20), ('omni_circle', 20.1),
							   ('player', 20.2), ('omni_circle', 20.3)))
		triggered2 = True
		