
def init(**kw):
	level = kw['level']
	def p_collapse(b):
		def collapse():
			b.on_step()
		return collapse
	def p_revive(b):
		def revive():
			b.revive()
		return revive
		
	for i, b in enumerate(level.collapsing_platforms):
		level.schedule_event(20 + 2*i, p_collapse(b))
		level.schedule_event(60 + 2*i, p_revive(b))
