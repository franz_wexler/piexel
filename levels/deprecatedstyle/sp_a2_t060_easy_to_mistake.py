
def init(**kw):
	level = kw['level']
	def p_fire(b):
		def fire():
			b.fire()
		return fire
	for i, b in enumerate(level.greedy_spikes):
		level.schedule_event(20+i, p_fire(b))
