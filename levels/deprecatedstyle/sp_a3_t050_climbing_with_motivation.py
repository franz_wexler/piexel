level = None

spikeblock = None

def init(**kw):
	global level
	level = kw['level']
	global spikeblock
	spikeblock = kw['std'].MovingGroup(level.moving_spikes[0], level.moving_blocks[0])


t = False

def trigger1(**kw):
	global t
	t = True

def trigger2(**kw):
	if t: spikeblock.enable()
		
def playerdeath(**kw):
	global t
	t = False
	spikeblock.disable()
		