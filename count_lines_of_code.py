""" Python script counting lines of source code of Piexel Project """

from os.path import getsize
from glob import glob


def get_counters(paths):
    files = []
    for path in paths:
        for file in glob(path):
            files.append(file)

    lines_counter = 0
    characters_counter = 0
    files_counter = 0
    size_counter = 0

    for file in files:
        with open(file) as stream:
            read = str(stream.read())
            lines_counter += read.count("\n")
            characters_counter += len(read)
            files_counter += 1
            size_counter += getsize(file)

    return lines_counter, characters_counter, files_counter, size_counter


def bytes_to_megabytes(x):
    return round(x / 1000000, 3)


py_paths = ("./game.py",
            "./lib/*.py",
            "./levels/*/*.py",
            __file__)
py_lines, py_characters, py_files, py_size = get_counters(py_paths)

lvl_paths = ("./levels/*/*.txt",
             "./levels/*/*.ini")
lvl_lines, lvl_characters, lvl_files, lvl_size = get_counters(lvl_paths)

svs_paths = ("./save/*.ini",)
svs_lines, svs_characters, svs_files, svs_size = get_counters(svs_paths)

counters = {'py_lines_counter': py_lines,
            'py_characters_counter': py_characters,
            'py_files_counter': py_files,
            'py_size_counter': py_size,
            'py_size_counter_MB': "{}MB".format(bytes_to_megabytes(py_size)),
            'lvl_lines_counter': lvl_lines,
            'lvl_characters_counter': lvl_characters,
            'lvl_files_counter': lvl_files,
            'lvl_size_counter': lvl_size,
            'lvl_size_counter_MB': "{}MB".format(bytes_to_megabytes(lvl_size)),
            'svs_lines_counter': svs_lines,
            'svs_characters_counter': svs_characters,
            'svs_files_counter': svs_files,
            'svs_size_counter': svs_size,
            'svs_size_counter_MB': "{}MB".format(bytes_to_megabytes(svs_size))}


info = """\
[Python code]       [Levels]            [Saves]
files = {py_files_counter:<12}files = {lvl_files_counter:<12}files = {svs_files_counter:<12}
lines = {py_lines_counter:<12}lines = {lvl_lines_counter:<12}lines = {svs_lines_counter:<12}
characters = {py_characters_counter:<7}characters = {lvl_characters_counter:<7}characters = {svs_characters_counter:<7}
bytes = {py_size_counter_MB:<12}bytes = {lvl_size_counter_MB:<12}bytes = {svs_size_counter_MB:<12}\
"""  # one of the most strange strings ever created

info_filled = info.format(**counters)

if __name__ == '__main__':
    print(info_filled)
