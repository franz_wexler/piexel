from cx_Freeze import setup, Executable

import sys
base = 'Win32GUI' if sys.platform=='win32' else None

executables = [
    Executable('piexel.py', base=base, icon='piexel.ico', shortcutName='Piexel')
]

shortcut_table = [
    ("DesktopShortcut",        # Shortcut
     "DesktopFolder",          # Directory_
     "Tool",           # Name
     "TARGETDIR",              # Component_
     "[TARGETDIR]\sample.exe",# Target
     None,                     # Arguments
     None,                     # Description
     None,                     # Hotkey
     None,                     # Icon
     None,                     # IconIndex
     None,                     # ShowCmd
     "TARGETDIR",               # WkDir
     )
    ]

buildOptions = dict(packages = [], excludes = ['save/config.ini'], include_files=['font', 'levels', 'music', 'res', 'save'], optimize=2)
buildMsiOptions = dict(upgrade_code='{08038D4A-E337-4EBE-A981-AAE9E16D8D0E}', add_to_path=False)

setup(name='Piexel',
      version = '0.9',
      description = 'A game of square',
      author = 'Franz Wexler',
      options = dict(build_exe = buildOptions, bdist_msi = buildMsiOptions),
      executables = executables
      )
