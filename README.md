PIEXEL
======

A short game about a square who wants to discover why his memories are lost.

To run Piexel, you need to have installed:

  1. [Python](http://python.org/) version >= 3.3 (game has been tested with 3.3, 3.4 and 3.5)
  2. [Pygame](http://pygame.org/) compatible with installed Python version

If you need help configuring your system, the following tutorial can be helpful:  
https://skellykiernan.wordpress.com/2015/01/04/python-pygame-install/  
To run simply double click on file `piexel.pyw`

If you have Python 3.6 and game crashes on start, try downloading newest game source instead ([Download repository](https://bitbucket.org/franz_wexler/piexel/downloads)). Some bug fixes are not released.

This project is now dead. Code quality is terrible and I don't think it will ever be alive again.
