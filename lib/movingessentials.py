from pygame import Rect
from .easeanim import Anim2D, ease_in_out, ease_out_bounce
from .const import TILE_EDGE
from .entities import Entity, draw_patternized
from .io import EntryDoor, ExitDoor
from .archived import CollapsingPlatform, GreedySpike, Mover


# __all__ = ['MovingSpikeUp', 'MovingSpikeDown', 'MovingSpikeLeft', 'MovingSpikeRight', 'MovingBlock', 'MovinPlatform',
#            'MovingGroup', 'MovingEntryDoor', 'MovingExitDoor', 'MovingCollapsingPlatform']


class AnimatedMovingEntity(Entity):
    def __init__(self, level, graphic_str, x, y, w=TILE_EDGE, h=TILE_EDGE, diff_x=0, diff_y=0, anim_time=40,
                 anim_func=ease_in_out, anim_rfunc=ease_in_out, loop=False, anim_auto_start=0):
        Entity.__init__(self, level, getattr(level.graphic, graphic_str) if graphic_str else None, (x, y, w, h))
        self.anim = Anim2D((x, x + diff_x), (y, y + diff_y), anim_time, anim_func, anim_rfunc)
        if loop:
            self.anim.frame_0_end = self.anim.enable
            self.anim.frame_max_end = self.anim.disable
        if loop or anim_auto_start:
            def enable_moving_entity():
                self.anim.enable()
            level.schedule_event(anim_auto_start, enable_moving_entity)

    def update(self):
        self.anim.update()
        self.rect.x, self.rect.y = self.anim.vals

    @property
    def xvel(self):
        return self.anim.x.next_change()

    @property
    def yvel(self):
        return self.anim.y.next_change()

    def reduce_condition(self, other):
        return (type(self) == type(other) and self.anim.x.begin == other.anim.x.begin and
                self.anim.x.end == other.anim.x.end)


class MovingSpike(AnimatedMovingEntity):
    texture = None

    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE, diff_x=0, diff_y=0, anim_time=40, anim_func=ease_in_out,
                 anim_rfunc=ease_in_out, loop=False, aas=False):
        AnimatedMovingEntity.__init__(self, level, self.texture, x, y, w, h, diff_x, diff_y, anim_time, anim_func,
                                      anim_rfunc, loop, aas)

        level.moving_spikes.append(self)
    draw = draw_patternized


class MovingSpikeUp(MovingSpike):
    texture = 'spike_up'


class MovingSpikeDown(MovingSpike):
    texture = 'spike_down'


class MovingSpikeLeft(MovingSpike):
    texture = 'spike_left'


class MovingSpikeRight(MovingSpike):
    texture = 'spike_right'


class MovingBlock(AnimatedMovingEntity):
    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE, diff_x=0, diff_y=0, anim_time=40, anim_func=ease_in_out,
                 anim_rfunc=ease_in_out, loop=False, aas=False):
        AnimatedMovingEntity.__init__(self, level, 'tile_surface', x, y, w, h, diff_x, diff_y, anim_time, anim_func,
                                      anim_rfunc, loop, aas)
        self.outer_rect = Rect(x, y, w, h)
        level.moving_blocks.append(self)
    draw = draw_patternized

    def update(self):
        AnimatedMovingEntity.update(self)
        self.outer_rect.x = self.rect.x
        self.outer_rect.y = self.rect.y - 1

    def reduce_update(self):
        self.outer_rect = Rect(self.rect.x, self.rect.y-1, self.rect.w, self.rect.h+2)


class MovinPlatform(AnimatedMovingEntity):
    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE, diff_x=0, diff_y=0, anim_time=40, anim_func=ease_in_out,
                 anim_rfunc=ease_in_out, loop=False, aas=False):
        AnimatedMovingEntity.__init__(self, level, 'platform_surface', x, y, w, h, diff_x, diff_y, anim_time, anim_func,
                                      anim_rfunc, loop, aas)
        self.outer_rect = Rect(x, y, w, h)
        level.movin_platforms.append(self)
    draw = draw_patternized

    def update(self):
        AnimatedMovingEntity.update(self)
        self.outer_rect.x = self.rect.x
        self.outer_rect.y = self.rect.y - 1

    def reduce_update(self):
        self.outer_rect = Rect(self.rect.x, self.rect.y-1, self.rect.w, self.rect.h+2)


class MovingEntryDoor(EntryDoor, AnimatedMovingEntity):
    def __init__(self, level, x, y, diff_x=0, diff_y=0, anim_time=40, anim_func=ease_in_out, anim_rfunc=ease_in_out,
                 loop=False, aas=0):
        AnimatedMovingEntity.__init__(self, level, None, x, y, 0, 0, diff_x, diff_y, anim_time, anim_func, anim_rfunc,
                                      loop, aas)
        EntryDoor.__init__(self, level, x, y)

    def update(self):
        AnimatedMovingEntity.update(self)
        self.frame_rect = Rect(self.rect.x+TILE_EDGE/2, self.rect.y, self.rect.w, self.rect.h)
        EntryDoor.update(self)

    draw = EntryDoor.draw


class MovingExitDoor(ExitDoor, AnimatedMovingEntity):  # nearly the same as MovingEntryDoor, but who cares?
    def __init__(self, level, x, y, diff_x=0, diff_y=0, anim_time=40, anim_func=ease_in_out, anim_rfunc=ease_in_out,
                 loop=False, aas=0):
        AnimatedMovingEntity.__init__(self, level, None, x, y, 0, 0, diff_x, diff_y, anim_time, anim_func, anim_rfunc,
                                      loop, aas)
        ExitDoor.__init__(self, level, x, y)

    def update(self):
        AnimatedMovingEntity.update(self)
        self.frame_rect = Rect(self.rect.x+TILE_EDGE/2, self.rect.y, self.rect.w, self.rect.h)
        ExitDoor.update(self)

    draw = ExitDoor.draw


class MovingGroup:
    def __init__(self, *items):
        self.items = items

    def update(self):
        for i in self.items:
            i.update()

    def draw(self, dest):
        for i in self.items:
            i.draw(dest)

    def halt(self):
        for i in self.items:
            i.anim.halt()

    def enable(self):
        for i in self.items:
            i.anim.enable()

    def disable(self):
        for i in self.items:
            i.anim.disable()

    @property
    def anim(self):
        return None if len(self.items) == 0 else self.items[0].anim

    @anim.setter
    def anim(self, val):
        for i in self.items:
            i.anim = val


class MovingCollapsingPlatform(CollapsingPlatform, AnimatedMovingEntity):
    def __init__(self, level, x, y, diff_x=0, diff_y=0, anim_time=40, anim_func=ease_in_out,
                 anim_rfunc=ease_in_out, loop=False, aas=0, collapse_delay=8):
        AnimatedMovingEntity.__init__(self, level, None, x, y, 0, 0, diff_x, diff_y, anim_time, anim_func, anim_rfunc,
                                      loop, aas)

        CollapsingPlatform.__init__(self, level, x, y, collapse_delay)

    def update(self):
        AnimatedMovingEntity.update(self)
        CollapsingPlatform.update(self)


class GreedySpikeStandEx(Entity):
    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE):
        Entity.__init__(self, level, level.graphic.greedy_stand, (x, y, w, h))
        level.updatables.append(self)

    def update(self):
        pass

    draw = draw_patternized


class MovingGreedySpike(GreedySpike, AnimatedMovingEntity):
    def __init__(self, level, x, y, diff_x=0, diff_y=0, anim_time=40, anim_func=ease_in_out,
                 anim_rfunc=ease_in_out, loop=False, aas=0, vel=14):
        AnimatedMovingEntity.__init__(self, level, None, x, y, 0, 0, diff_x, diff_y, anim_time, anim_func, anim_rfunc,
                                      loop, aas)

        GreedySpike.__init__(self, level, x, y, vel)

    def update(self):
        self.anim.update()
        self.rect.x, self.init_y = self.anim.vals

        GreedySpike.update(self)
