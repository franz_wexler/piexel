from pygame import mouse, cursors


def compile_cursor(cur):
    hotspot = None
    for y in range(len(cur)):
        for x in range(len(cur[y])):
            if cur[y][x] in ['x', ',', 'O']:
                hotspot = x, y
                break
        if hotspot is not None:
            break
    if hotspot is None:
        raise Exception("No hotspot specified for cursor")
    s2 = []
    for line in cur:
        s2.append(line.replace('x', 'X').replace(',', '.').replace('O', 'o'))
    cursor, mask = cursors.compile(s2, 'X', '.', 'o')
    size = len(cur[0]), len(cur)
    return size, hotspot, cursor, mask


arrow = ("x                       ",
         "XX                      ",
         "X.X                     ",
         "X..X                    ",
         "X...X                   ",
         "X....X                  ",
         "X.....X                 ",
         "X......X                ",
         "X.......X               ",
         "X........X              ",
         "X.........X             ",
         "X..........X            ",
         "X......XXXXX            ",
         "X...X..X                ",
         "X..X X..X               ",
         "X.X  X..X               ",
         "XX    X..X              ",
         "      X..X              ",
         "       XX               ",
         "                        ",
         "                        ",
         "                        ",
         "                        ",
         "                        ")
arrow = compile_cursor(arrow)

helpsign = ("x                       ",
            "XX                      ",
            "X.X                     ",
            "X..X                    ",
            "X...X                   ",
            "X....X                  ",
            "X.....X                 ",
            "X......X                ",
            "X.......X               ",
            "X........X              ",
            "X.........X             ",
            "X..........X            ",
            "X......XXXXX   XXXX     ",
            "X...X..X      X....X    ",
            "X..X X..X     X.XX..X   ",
            "X.X  X..X      X X..X   ",
            "XX    X..X      X..X    ",
            "      X..X     X..X     ",
            "       XX      X..X     ",
            "                XX      ",
            "               X..X     ",
            "               X..X     ",
            "                XX      ",
            "                        ")
helpsign = compile_cursor(helpsign)

hand = ("     xX                 ",
        "    X..X                ",
        "    X..X                ",
        "    X..X                ",
        "    X..X                ",
        "    X..XXX              ",
        "    X..X..XXX           ",
        "    X..X..X..XX         ",
        "    X..X..X..X.X        ",
        " XX X..X..X..X..X       ",
        "X..XX........X..X       ",
        "X...X...........X       ",
        " X..............X       ",
        "  X.............X       ",
        "  X.............X       ",
        "   X...........X        ",
        "   X...........X        ",
        "    X..........X        ",
        "    X.........X         ",
        "     X........X         ",
        "     X........X         ",
        "     XXXXXXXXXX         ",
        "                        ",
        "                        ")
hand = compile_cursor(hand)

hourglass = ("x                       ",
             "XX            XXXXXXXXXX",
             "X.X           XX......XX",
             "X..X          XXXXXXXXXX",
             "X...X          X......X ",
             "X....X         X......X ",
             "X.....X        X...X..X ",
             "X......X       XX.X..XX ",
             "X.......X       XX..XX  ",
             "X........X       XX.X   ",
             "X.........X     XX..XX  ",
             "X..........X   XX....XX ",
             "X......XXXXX   X..X...X ",
             "X...X..X       X.X.X..X ",
             "X..X X..X      XX.X.X.X ",
             "X.X  X..X     XXXXXXXXXX",
             "XX    X..X    XX......XX",
             "      X..X    XXXXXXXXXX",
             "       XX               ",
             "                        ",
             "                        ",
             "                        ",
             "                        ",
             "                        ")
hourglass = compile_cursor(hourglass)

cursors_map = {'arrow': arrow, 'help': helpsign, 'hand': hand, 'hourglass': hourglass}

last_cursor = None


def set_cursor(cur):
    global last_cursor
    if cur != last_cursor:
        mouse.set_cursor(*(cursors_map[cur]))
        last_cursor = cur


def get_cursor():
    return last_cursor


def set_loading():
    # cursor will return to normal state on next Game update
    set_cursor('hourglass')
