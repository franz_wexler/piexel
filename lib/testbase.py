from pygame import Rect, Color
from itertools import zip_longest
from collections import Iterable
from .const import TILE_EDGE
from .graphic import draw_tile_pattern, rendertext


__all__ = ['Block', 'LiedBlock', 'Platform', 'BarrierBlock', 'SpikeUp', 'SpikeDown', 'SpikeLeft', 'SpikeRight',
           'LiedSpikeUp', 'LiedSpikeDown', 'LiedSpikeLeft', 'LiedSpikeRight', 'BackgroundTile',
           'BackgroundText', 'BigBackgroundText', 'TileText', 'BigTileText', 'InvisiblePlatform']


class Lie:
    texture = None
    shift = (0, 0)

    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE):
        if (isinstance(self.texture, str)) or (not isinstance(self.texture, Iterable)):
            self.texture = (self.texture,)
        if not (len(self.shift) > 0 and isinstance(self.shift[0], Iterable)):
            self.shift = (self.shift,)
        assert len(self.texture) >= len(self.shift)
        for texture, shift in zip_longest(self.texture, self.shift, fillvalue=(0, 0)):
            draw_tile_pattern(level.bg, getattr(level.graphic, texture), Rect(x+shift[0], y+shift[1], w, h))


class InvisibleBlock:
    __slots__ = ['rect']

    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE):
        self.rect = Rect(x, y, w, h)

        level.blocks.append(self)


class Block:
    __slots__ = ['rect']

    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE):
        self.rect = Rect(x, y, w, h)

        draw_tile_pattern(level.bg, level.graphic.tile_surface, self.rect)

        level.blocks.append(self)


class LiedBlock(Lie):
    texture = 'tile_surface'


class InvisiblePlatform:
    __slots__ = ['rect']

    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE//2):
        self.rect = Rect(x, y, w, h)

        level.platforms.append(self)


class Platform:
    __slots__ = ['rect']

    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE//2):
        self.rect = Rect(x, y, w, h)

        draw_tile_pattern(level.bg, level.graphic.platform_surface, self.rect)

        level.platforms.append(self)


class BarrierBlock:
    __slots__ = ['rect']

    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE):
        self.rect = Rect(x, y, w, h)

        level.barrier_blocks.append(self)


class Spike:
    __slots__ = ['level', 'rect']

    texture = None

    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE):
        self.level = level
        self.rect = Rect(x, y, w, h)

        if self.texture is not None:
            draw_tile_pattern(level.bg, getattr(level.graphic, self.texture), self.rect)

        level.spikes.append(self)


class SpikeUp(Spike):
    texture = 'spike_up'


class SpikeDown(Spike):
    texture = 'spike_down'


class SpikeLeft(Spike):
    texture = 'spike_left'


class SpikeRight(Spike):
    texture = 'spike_right'


class LiedSpikeUp(Lie):
    texture = 'spike_up'


class LiedSpikeDown(Lie):
    texture = 'spike_down'


class LiedSpikeLeft(Lie):
    texture = 'spike_left'


class LiedSpikeRight(Lie):
    texture = 'spike_right'


class BackgroundTile(Lie):
    texture = 'bg_tile_surface'


class BackgroundText:
    color = 'bg_tile_color'
    size = 1

    def __init__(self, level, x, y, text):
        if not self.color.startswith('#'):
            self.color = getattr(level.graphic, self.color)
        level.bg.blit(rendertext(text, self.size, self.color), (x, y))


class BigBackgroundText(BackgroundText):
    size = 5


class TileText:
    color = 'accent_color'
    size = 1

    def __init__(self, level, x, y, text):
        if not self.color.startswith('#'):
            self.color = getattr(level.graphic, self.color)
        self.color = Color(self.color)
        Block(level, x, y)
        level.afterdraw.append(self)
        self.level, self.x, self.y, self.text = level, x, y, text

    def afterdraw(self):
        self.level.bg.blit(rendertext(self.text, self.size, self.color), (self.x, self.y))


class BigTileText(TileText):
    size = 5
