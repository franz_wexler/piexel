from ctypes import windll, c_bool, c_uint
from os import getpid


GetPriorityClass = windll.kernel32.GetPriorityClass
SetPriorityClass = windll.kernel32.SetPriorityClass
OpenProcess = windll.kernel32.OpenProcess
CloseHandle = windll.kernel32.CloseHandle


IDLE_PRIORITY_CLASS = 0x0040
BELOW_NORMAL_PRIORITY_CLASS = 0x4000
NORMAL_PRIORITY_CLASS = 0x0020
ABOVE_NORMAL_PRIORITY_CLASS = 0x8000
HIGH_PRIORITY_CLASS = 0x0080
REALTIME_PRIORITY_CLASS = 0x0100


priorities_map = {'idle': IDLE_PRIORITY_CLASS, 'belownormal': BELOW_NORMAL_PRIORITY_CLASS,
                  'normal': NORMAL_PRIORITY_CLASS, 'abovenormal': ABOVE_NORMAL_PRIORITY_CLASS,
                  'high': HIGH_PRIORITY_CLASS, 'realtime': REALTIME_PRIORITY_CLASS}


__shouldClose = [False]


def get_process_handle(process, inherit=False):
    __shouldClose[0] = True
    if not process:
        process = getpid()
    return OpenProcess(c_uint(0x0200 | 0x0400), c_bool(inherit), c_uint(process))


def set_priority_by_id(priority, process=None, inherit=False):
    return set_priority(priority, get_process_handle(process, inherit))


def set_priority(priority, process=None, inherit=False):
    if process is None:
        process = get_process_handle(None, inherit)
    result = SetPriorityClass(process, c_uint(priority)) != 0
    if __shouldClose:
        CloseHandle(process)
        __shouldClose[0] = False
    return result
