from pygame import Rect
from itertools import chain
from math import sin, cos, atan2, degrees, pi
from .pydiff import isclose
from .const import TILE_EDGE
from .entities import Entity
from .particles import spread_particles
from .util import blit_rotated, toval, randd, rect_collideline, randrange_float, tiledv
from .testbase import Spike
from random import random


__all__ = ['PlasmaCannon']


class PlasmaCannon(Entity):
    """ *BOOM* """

    def __init__(self, level, x, y, angle_speed=0.02, bullet_speed=15,
                 shoot_delay=18):
        Entity.__init__(self, level, level.graphic.plasma_cannon, (x, y, TILE_EDGE*3, TILE_EDGE))
        self.riffle_graphic = level.graphic.cannon_riffle
        self.bullet_graphic = level.graphic.bullet

        self.current_angle = 0.0  # in RADIANS
        self.angle_speed = angle_speed

        self.bullet_speed = bullet_speed
        self.shoot_delay = shoot_delay
        self.current_delay = 0

        self.idle_anim_dir = 0
        self.min_angle = pi * 0.1  # the rightmost angle
        self.max_angle = pi * 0.9  # the leftmost angle
        self.current_angle = randrange_float(self.min_angle, self.max_angle, pi*0.1)

        Spike(level, x, y, TILE_EDGE*3)
        level.cannons.append(self)

    def shoot(self):
        """Shoots a bullet in direction of current_angle"""
        f_xvel = self.bullet_speed * cos(self.current_angle)
        f_yvel = self.bullet_speed * sin(self.current_angle)
        bullet = Bullet(self.level, self.rect.centerx, self.rect.centery, -degrees(self.current_angle), f_xvel, f_yvel)
        return bullet

    def update(self):
        for b in chain(self.level.blocks, self.level.barrier_blocks, self.level.moving_platforms):
            if rect_collideline(b.rect, self.rect.center, self.level.player.rect.center):
                if self.idle_anim_dir == 0:
                    self.current_angle += self.angle_speed / 2
                    if self.current_angle >= self.max_angle:
                        self.current_angle = self.max_angle
                        self.idle_anim_dir = 1
                elif self.idle_anim_dir == 1:
                    self.current_angle -= self.angle_speed / 2
                    if self.current_angle <= self.min_angle:
                        self.current_angle = self.min_angle
                        self.idle_anim_dir = 0
                self.current_delay = 0
                break
        else:
            target_point = self.level.player.rect.center
            target_angle = atan2(target_point[1] - self.rect.centery, target_point[0] - self.rect.centerx)

            self.current_angle = toval(self.current_angle, self.angle_speed, target_angle)

            self.current_delay += 1
            if self.current_delay >= self.shoot_delay and isclose(self.current_angle, target_angle, abs_tol=0.5):
                self.shoot()
                self.current_delay = 0

    def draw(self, dest):
        dest.blit(self.image, self.rect)
        blit_rotated(dest, self.riffle_graphic, self.rect.center, -degrees(self.current_angle) + 90)


class Bullet:
    def __init__(self, level, x, y, angle, xvel, yvel):
        self.level = level
        self.rect = Rect(0, 0, TILE_EDGE, TILE_EDGE)
        self.rect.centerx, self.rect.centery = x, y
        self.f_x, self.f_y = self.rect.x, self.rect.y
        self.angle = angle
        self.xvel, self.yvel = tiledv(xvel), tiledv(yvel)
        self.rotation = randd() * random() * 2

        level.bullets.append(self)

    def update(self):
        self.f_x += self.xvel
        self.rect.x = self.f_x
        for b in chain(self.level.blocks, self.level.moving_blocks,
                       (self.level.player,)):
            if b.rect.colliderect(self.rect):
                self.kill()
                if hasattr(b, 'init_kill'):
                    b.init_kill()
                spread_particles(self.level, self.level.graphic.bullet_debris, self.rect.center,
                                 self.xvel/3, self.yvel/3, 'x')
                return
        self.f_y += self.yvel
        self.rect.y = self.f_y
        for b in chain(self.level.blocks, (self.level.player,)):
            if b.rect.colliderect(self.rect):
                self.kill()
                if hasattr(b, 'init_kill'):
                    b.init_kill()
                spread_particles(self.level, self.level.graphic.bullet_debris, self.rect.center,
                                 self.xvel/3, self.yvel/3, 'y')
                return
        self.angle += self.rotation

    def draw(self, dest):
        blit_rotated(dest, self.level.graphic.bullet, self.rect.center, self.angle)

    def kill(self):
        self.level.bullets.remove(self)
