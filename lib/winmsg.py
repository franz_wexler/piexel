import ctypes

# see https://msdn.microsoft.com/en-us/library/windows/desktop/ms645505.aspx for more flags

MB_ABORTRETRYIGNORE = 0x02
MB_CANCELTRYCONTINUE = 0x06
MB_OKCANCEL = 0x01
MB_RETRYCANCEL = 0x05
MB_YESNO = 0x04
MB_YESNOCANCEL = 0x03
MB_HELP = 0x04000
MB_OK = 0x0


def get_buttons_flags(buttons=()):
    def has_all(*args):
        for i in args:
            if i not in buttons:
                return False
        return True

    if has_all('abort', 'retry', 'ignore'):
        return MB_ABORTRETRYIGNORE
    if has_all('cancel', 'tryagain', 'continue'):
        return MB_CANCELTRYCONTINUE
    if has_all('cancel'):
        if has_all('ok'):
            return MB_OKCANCEL
        if has_all('retry'):
            return MB_RETRYCANCEL
        if has_all('yes', 'no'):
            return MB_YESNOCANCEL
    else:
        if has_all('yes', 'no'):
            return MB_YESNO
        if has_all('ok'):
            return MB_OK
    if has_all('help'):
        return MB_HELP
    return 0x0


MB_ICONWARNING = 0x030
MB_ICONINFORMATION = 0x040
MB_ICONQUESTION = 0x020
MB_ICONERROR = 0x010


def get_icon(icon_str):
    icon_str = str(icon_str).lower()
    return {'none': 0x0, 'warning': MB_ICONWARNING, 'information': MB_ICONINFORMATION, 'question': MB_ICONQUESTION,
            'error': MB_ICONERROR}[icon_str]


MB_DEFBUTTON1 = 0x0
MB_DEFBUTTON2 = 0x0100
MB_DEFBUTTON3 = 0x0200
MB_DEFBUTTON4 = 0x0300


def get_def_button(i):
    return (MB_DEFBUTTON1, MB_DEFBUTTON2, MB_DEFBUTTON3, MB_DEFBUTTON4)[i]


IDOK = 1
IDCANCEL = 2
IDABORT = 3
IDRETRY = 4
IDIGNORE = 5
IDYES = 6
IDNO = 7
IDTRYAGAIN = 10
IDCONTINUE = 11


def get_id_string(id_):
    return {0: 'undefined', IDOK: 'ok', IDCANCEL: 'cancel', IDABORT: 'abort', IDRETRY: 'retry', IDIGNORE: 'ignore',
            IDYES: 'yes', IDNO: 'no', IDTRYAGAIN: 'tryagain', IDCONTINUE: 'continue'}[id_]


def get_id_bool(id_):
    if id_ in (IDOK, IDRETRY, IDYES, IDTRYAGAIN, IDCONTINUE):  # 'positive' answers
        return True
    return False


def msgbox(title="Piexel", message="", flags=0x0):
    return ctypes.windll.user32.MessageBoxW(None, message, title, flags)


def message_box(title="Piexel", message="", buttons=('ok',), icon=None, default=0, returntype=str):
    r = get_buttons_flags(buttons)
    r |= get_icon(icon)
    r |= get_def_button(default)

    result = ctypes.windll.user32.MessageBoxW(None, message, title, r)

    if returntype == str:
        return get_id_string(result)
    elif returntype == bool:
        return get_id_bool(result)
    elif returntype == int:
        return result
    else:
        raise ValueError(repr(returntype))
