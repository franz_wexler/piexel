from pygame import Rect
from .const import TILE_EDGE


__all__ = ['PlayerTriggerMacroContinuous', 'PlayerTriggerMacroOnce']


class PlayerTrigger:
    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE, func=None, elsefunc=None):
        self.level = level
        self.rect = Rect(x, y, w, h)
        self.func, self.elsefunc = func, elsefunc

        level.triggers.append(self)


class PlayerTriggerMacroContinuous(PlayerTrigger):
    def update(self):
        if self.func is not None and self.rect.colliderect(self.level.player.rect):
            getattr(self.level.macro, self.func).__call__()
        elif self.elsefunc is not None:
            getattr(self.level.macro, self.elsefunc).__call__()  # call elsefunc continuously


class PlayerTriggerMacroOnce(PlayerTrigger):
    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE, func=None, elsefunc=None):
        PlayerTrigger.__init__(self, level, x, y, w, h, func, elsefunc)
        self.rel_player = 0

    def update(self):
        if self.rect.colliderect(self.level.player.rect):
            if self.rel_player == 0:
                self.rel_player = 1
                if self.func is not None:
                    getattr(self.level.macro, self.func).__call__()
        elif self.rel_player == 1:  # call elsefunc only once
            self.rel_player = 0
            if self.elsefunc is not None:
                getattr(self.level.macro, self.elsefunc).__call__()
