from .const import TILE_EDGE
from .entities import GravityDynamic
from .util import log


__all__ = ['MacroRecorder']


class MacroRecorder:
    # Note: this class doesn't work well, is very glitchy and should be used only by devs who can repair its errors

    def __init__(self, game):
        self.game = game

        self.time = 0
        self.recording = True

        self.g_up = self.g_down = self.g_left = self.g_right = False

        self.motion = []

        self.game.level.player.goto_entrance()
        self.game.level.reset_to_initial()

    def update(self):
        if self.recording:
            change = {}
            for attr in ('g_up', 'g_down', 'g_left', 'g_right'):
                if getattr(self.game.level.player, attr) != getattr(self, attr):
                    change[attr] = getattr(self.game.level.player, attr)
            if change != {}:
                self.motion.append((self.time, change))
                for key, value in change.items():
                    setattr(self, key, value)
            self.time += 1

    def reset(self):
        self.time = 0
        self.g_up = self.g_down = self.g_left = self.g_right = False

    def play(self):
        self.game.level.reset_to_initial()

        log("[INFO] Scheduling motion <{}>".format(self.motion))
        self.recording = False
        self.game.level.player.goto_entrance()
        self.game.level.player.schedule_motion(self.motion)
        self.game.macro_recorder = None  # destroy self to avoid weird bugs
