"""Level Loading Library"""
from configparser import RawConfigParser
from .util import log
from .const import TILE_EDGE
from .testelem import std
from .util import reduce_rects
from .pydiff import import_file
from .locale import f, m


__all__ = ['CorruptedMapError', 'extract_level_name', 'parse_level_save', 'parse_tiledef', 'parse_macro', 'reduce_all']


class CorruptedMapError(ValueError):
    pass


def extract_level_name(level, filepath):
    i = filepath.rfind('/')
    if i == -1:
        i = 0
    e = filepath.rfind('.')
    if e == -1:
        pass
    return filepath[i+1:e]


def parse_tiledef(level, def_filepath):
    data = RawConfigParser()
    data.optionxform = lambda item: item  # make parsing case-sensitive
    data.read(def_filepath)
    for section in ('Label', 'Tiledef'):
        if section not in data:
            raise CorruptedMapError("{} is missing [{}] header".format(def_filepath, section))
    return data


def parse_level_save(level, filepath):
    with open(filepath) as f:
        log("[INFO] Attempting to load map from {} as Standard PP Map Format".format(repr(filepath)))

        content = str(f.read()).split(";")[0].splitlines()
    for y, row in enumerate(content):
        y *= TILE_EDGE
        for x, ent in enumerate(row):
            x *= TILE_EDGE
            if ent not in ' \n\t':
                try:
                    code = level.data['Tiledef'][ent]
                except KeyError:
                    log(" [WARN] Unknown entity {} found".format(repr(ent)))
                else:
                    exec(code, {'x': x, 'y': y, 'level': level, 'T': TILE_EDGE, 'std': std, 'f': f, 'm': m,
                                'macro': level.macro})


def parse_macro(level, macro_filepath):
    if macro_filepath is not None:
        return import_file(macro_filepath)
    else:
        return None


def reduce_all(*lists):
    for l in lists:
        reduce_rects(l)

