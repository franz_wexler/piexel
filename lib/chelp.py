from pygame import draw, Surface, Color
from .const import TILE_EDGE, SCREEN_SIZE
from .easeanim import Anim, ease_out
from .graphic import get_tooltip
from .locale import t


class ToolTip:
    def __init__(self, game, rect, text, typ='ui', anim_time=15):
        self.game = game
        self.orect = rect.copy()
        self.rect = rect.copy()
        self.text = t(text)

        self.surf = get_tooltip(self.text)
        self.rect.size = self.surf.get_size()
        self.rect.x, self.rect.y = rect.x, rect.bottom

        if typ == 'ui':
            self.rect.x += TILE_EDGE
            self.rect.y -= TILE_EDGE / 2
        elif typ == 'el':
            self.rect.x += TILE_EDGE / 4
            self.rect.y -= TILE_EDGE / 8
        else:
            raise ValueError("Invalid `typ` for ToolTip.__init__(): {}".format(repr(typ)))

        if self.rect.bottom > SCREEN_SIZE[1]:
            self.rect.bottom = rect.y + TILE_EDGE/2
        if self.rect.left < 0:
            self.rect.left = 0
        elif self.rect.right > SCREEN_SIZE[0]:
            self.rect.right = SCREEN_SIZE[0]

        self.anim = Anim(0, self.surf.get_height(), anim_time, ease_out)
        self.anim.enable()

        self.dismissed = False

    def update(self):
        self.anim.update()

    def draw(self, dest):
        if self.anim.frame != self.anim.time and self.anim.frame != 0:
            if self.anim.dir == 1:
                v = self.anim.val
                s = Surface((self.rect.width, v))
                s.blit(self.surf, (0, 0))
                draw.line(s, Color('#CCCCCC'), (0, v-TILE_EDGE//8), (self.rect.width, v-TILE_EDGE//8), TILE_EDGE//8)
                dest.blit(s, self.rect)
            elif self.anim.dir == 2:
                self.surf.set_alpha(self.anim.val)
                dest.blit(self.surf, self.rect)
        elif self.dismissed:
            self.game.tooltip = None  # suicide
        else:
            dest.blit(self.surf, self.rect)

    def dismiss(self):
        self.dismissed = True
        self.anim.end = 255
        self.anim.disable()

