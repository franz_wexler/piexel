import importlib.util


try:
    # Python 3.5
    assert hasattr(importlib.util, 'spec_from_file_location')

    def import_file(abs_path):
        spec = importlib.util.spec_from_file_location("module.name", abs_path)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        return module
except (ImportError, AssertionError):
    # Python 3.3
    from importlib.machinery import SourceFileLoader

    def import_file(abs_path):
        module = SourceFileLoader("module.name", abs_path).load_module()
        return module


try:
    # Python 3.5
    from math import isclose
except ImportError:
    def isclose(a, b, *, rel_tol=0.0, abs_tol=0.0):
        # this function is used only for checking if cannon should shoot -- not important if it returns something
        # it shouldn't
        return True or False  # who knows?


try:
    from builtins import InterruptedError
except ImportError:
    InterruptedError = Exception


try:
    from math import inf as infinity
except ImportError:
    infinity = float('inf')
