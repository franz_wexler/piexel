""""""
from pygame import font, Color, Surface
from .const import TILE_EDGE
from .log import log


__all__ = ['init_font', 'rendertext']


fonts = {}


def init(required_sizes=(1, 2, 5)):
    for size in required_sizes:
        fonts[size] = font.Font('./font/BionicType.ttf', int(size*TILE_EDGE))


init_font = init


def rendertext(text, size, col=Color('#00000000'), bgcol=None, w=TILE_EDGE*18):
    text = text.lower()  # to reduce font file size there are only lowercase letters
    f = fonts[size]
    c = text.count("\n")

    if c == 0:
        return f.render(text, False, col, bgcol)
    else:
        s = Surface((w, size*TILE_EDGE * (c + 1)))
        s.fill((255, 0, 255))
        s.set_colorkey((255, 0, 255))

        for i, line in enumerate(text.splitlines()):
            s.blit(f.render(line, False, col, bgcol), (0, i * size*TILE_EDGE))

        return s


def rendersize(line, size):
    return fonts[size].size(line)

# if __name__ == '__main__':
#    # simple demo of PIEXEL font rendering
#     init((5, 3.5))
#     from pygame import display, event, QUIT
#     screen = display.set_mode((int(TILE_EDGE*21.5), int(TILE_EDGE*9.5)))
#     screen.fill(Color('#EBEBEB'))
#     screen.blit(rendertext("PIEXEL", 5), (4, 4))
#     screen.blit(rendertext("The Game", 3.5), (4+TILE_EDGE/8, 8+TILE_EDGE*5))
#     while True:
#         for e in event.get():
#             if e.type == QUIT:
#                 display.quit()
#                 exit(0)
#         display.update()
