from .const import DISPLAY_MESSAGES


def pass_(*args, **kwargs):
    pass


log = print if DISPLAY_MESSAGES else pass_

