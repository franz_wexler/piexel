from pygame import display, mixer, FULLSCREEN, K_w, K_a, K_s, K_d, K_UP, K_LEFT, K_DOWN, K_RIGHT
from configparser import ConfigParser
from os.path import isfile
from collections import namedtuple


from .winpriority import set_priority, priorities_map

VALID_LANGS = ('en', 'pl')
VALID_PRIORITIES = ('normal', 'abovenormal', 'high', 'realtime')
VALID_QUALITIES = ('low', 'medium', 'high')
VALID_CONTROLSTYLES = ('wasd', 'arrows')


QLOW, QMEDIUM, QHIGH = range(3)


class Config:
    def __init__(self, filepath='save/config.ini', template_filepath='save/config_template.ini'):
        self.filepath = filepath

        if not isfile(filepath):
            filepath = template_filepath

        self.template_filepath = template_filepath

        self.p = ConfigParser()
        self.p.read(filepath)

    @property
    def lang(self):
        v = self.p['Config']['Language'].lower()
        assert v in VALID_LANGS
        return v

    @lang.setter
    def lang(self, val):
        val = str(val)
        assert val in VALID_LANGS
        self.p['Config']['Language'] = val

    @property
    def tpriority(self):
        v = self.p['Config']['TaskPriority'].lower()
        assert v in VALID_PRIORITIES
        return v

    @tpriority.setter
    def tpriority(self, val):
        assert val in VALID_PRIORITIES
        self.p['Config']['TaskPriority'] = val

    @property
    def fullscreen(self):
        v = self.p['Config'].getboolean('FullScreen')
        return v

    @fullscreen.setter
    def fullscreen(self, val):
        self.p['Config']['FullScreen'] = str(val).lower()

    @property
    def tile_edge(self):
        return int(self.p['Config']['UnitSize'])

    @tile_edge.setter
    def tile_edge(self, val):
        self.p['Config']['UnitSize'] = str(int(val))

    @property
    def control_style(self):
        v = self.p['Config']['ControlStyle'].lower()
        assert v in VALID_CONTROLSTYLES
        return v

    @control_style.setter
    def control_style(self, val):
        assert val in VALID_CONTROLSTYLES
        self.p['Config']['ControlStyle'] = val

    @property
    def kw(self):
        return {'en': ("[w]", "[up arrow]"), 'pl': ("[w]", "klawisz strzałki w górę")
                }[LANG][self.control_style == 'arrows']

    @property
    def ka(self):
        return {'en': ("[a]", "[left arrow]"), 'pl': ("[a]", "klawisz strzałki w lewo")
                }[LANG][self.control_style == 'arrows']

    @property
    def ks(self):
        return {'en': ("[s]", "[down arrow]"), 'pl': ("[s]", "klawisz strzałki w dół")
                }[LANG][self.control_style == 'arrows']

    @property
    def kd(self):
        return {'en': ("[d]", "[right arrow]"), 'pl': ("[d]", "klawisz strzałki w prawo")
                }[LANG][self.control_style == 'arrows']

    @property
    def k_w(self):
        return (K_w, K_UP)[self.control_style == 'arrows']

    @property
    def k_a(self):
        return (K_a, K_LEFT)[self.control_style == 'arrows']

    @property
    def k_s(self):
        return (K_s, K_DOWN)[self.control_style == 'arrows']

    @property
    def k_d(self):
        return (K_d, K_RIGHT)[self.control_style == 'arrows']

    @property
    def quality(self):
        v = self.p['Config']['Quality'].lower()
        assert v in VALID_QUALITIES
        return v

    @quality.setter
    def quality(self, val):
        assert val in VALID_QUALITIES
        self.p['Config']['Quality'] = val

    @property
    def target_fps(self):
        return 60 if self.q > QLOW else 30

    @property
    def target_ups(self):
        return 1 if self.q > QLOW else 2

    @property
    def q(self):
        return {'low': 0, 'medium': 1, 'high': 2}[self.quality]

    @property
    def music(self):
        v = self.p['Config'].getboolean('Music')
        return v

    @music.setter
    def music(self, val):
        self.p['Config']['Music'] = str(val).lower()

    @property
    def dev_mode(self):
        v = self.p['Config'].getboolean('DevMode')
        return v

    @dev_mode.setter
    def dev_mode(self, val):
        self.p['Config']['DevMode'] = str(val).lower()

    @property
    def death1mode_unlocked(self):
        v = self.p['Config'].getboolean('Death1ModeUnlocked')
        return v

    @death1mode_unlocked.setter
    def death1mode_unlocked(self, val):
        self.p['Config']['Death1ModeUnlocked'] = str(val).lower()

    @property
    def open_paused(self):
        if 'OpenInMenu' in self.p['Config']:
            return self.p['Config'].getboolean('OpenInMenu')
        return False

    @open_paused.setter
    def open_paused(self, val):
        self.p['Config']['OpenInMenu'] = str(val).lower()

    @property
    def active_game(self):
        if 'ActiveGame' not in self.p['Config']:
            return None
        v = self.p['Config']['ActiveGame']
        if v.lower() == 'none':
            return None
        return v

    @active_game.setter
    def active_game(self, val):
        self.p['Config']['ActiveGame'] = str(val)

    def save(self, filepath=None):
        if filepath is None:
            filepath = self.filepath
        with open(filepath, 'w') as f:
            self.p.write(f)

    def apply(self, level):
        need_restart = (self.lang != LANG or self.tpriority != TPRIORITY or
                        self.tile_edge != TILE_EDGE or self.dev_mode != DEVMODE)
        if need_restart:
            if level.game.menu is None:
                self.open_paused = True
                self.save()
            level.game.break_mainloop = True

        elif bool(display.get_surface().get_flags() & FULLSCREEN) != self.fullscreen:
            if not level.game.toggle_fullscreen():
                from .uit import fullscreen_error
                level.error_box = fullscreen_error(level)
                level.settings_menu.widgets[3].lclick_event(namedtuple('event', ('pos',))((0, 0)))
                self.fullscreen = False
                self.save()

        level.game.target_fps = self.target_fps
        level.game.target_tick_updates = self.target_ups

        set_priority(priorities_map[self.tpriority])

        if mixer.get_init():
            if self.music:
                level.game.load_soundtrack()
            else:
                mixer.music.stop()


config = Config()


MAP_SIZE = (64, 36)
TILE_EDGE = config.tile_edge
SCREEN_SIZE = (MAP_SIZE[0] * TILE_EDGE, MAP_SIZE[1] * TILE_EDGE)


DISPLAY_MESSAGES = config.dev_mode


LANG = config.lang

QUALITY = config.quality

DEVMODE = config.dev_mode

TPRIORITY = config.tpriority
if config.tpriority != 'normal':
    set_priority(priorities_map[config.tpriority])
