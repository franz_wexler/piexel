from inspect import signature


def num_args(func):
    return len(signature(func).parameters)


def anim_linear(t, b, c, d):
    return c * t / d + b


def ease_out(t, b, c, d):
    t /= d
    return -c * t * (t - 2) + b


def ease_in_out(t, b, c, d):
    t /= d * 0.5
    if t < 1:
        return c * 0.5 * t ** 2 + b
    t -= 1
    return -c / 2 * (t * (t - 2) - 1) + b


def half_return_eased(t, b, c, d):
    t /= d * 0.5
    if t < 1:
        return c * 0.5 * t ** 2 + b
    t -= 1
    return -c * 0.5 * (t ** 2 - 1) + b


def ease_in_bounce(t, b, c, d):
    return c - ease_out_bounce(d - t, 0, c, d) + b


def ease_out_bounce(t, b, c, d):
    t /= d
    if t < 1 / 2.75:
        return c * (7.5625 * t ** 2) + b
    elif t < 2 / 2.75:
        t -= 1.5 / 2.75
        return c * (7.5625 * t ** 2 + 0.75) + b
    elif t < 2.5 / 2.75:
        t -= 2.25 / 2.75
        return c * (7.5625 * t ** 2 + 0.9375) + b
    else:
        t -= 2.625 / 2.75
        return c * (7.5625 * t ** 2 + 0.984375) + b


def ease_out_back(t, b, c, d, s=1.70158):
    t = t / d - 1
    return c * (t * t * ((s + 1) * t + s) + 1) + b


class Anim:
    __slots__ = ['begin', 'end', 'time', 'frame', 'dir', 'func', 'rfunc', '_f', 'frame_0_end', 'frame_max_end']

    def __init__(self, begin, end, time, func=ease_in_out, rfunc=ease_in_out):
        self.begin, self.end, self.time = begin, end, time
        self.frame = 0  # frame of animation
        self.dir = 0  # 0 for none, 1 for increase, 2 for decrease
        self.func, self.rfunc = func, rfunc  # functions calcing progress of animation
        self._f = self.func  # function used before halting

        self.frame_max_end = None  # function executed if frame reached maximal time
        self.frame_0_end = None  # function executed if frame reached 0

    def update(self, step=1):
        if self.dir == 1:  # increase
            self._f = self.func
            self.frame += step
            if self.frame > self.time:
                self.frame = self.time
                self.dir = 0  # stop
                if self.frame_max_end is not None:
                    if num_args(self.frame_max_end) == 0:
                        self.frame_max_end()
                    else:
                        self.frame_max_end(self)
        elif self.dir == 2:  # decrease
            self._f = self.rfunc
            self.frame -= step
            if self.frame < 0:
                self.frame = 0
                self.dir = 0  # stop
                if self.frame_0_end is not None:
                    if num_args(self.frame_0_end) == 0:
                        self.frame_0_end()
                    else:
                        self.frame_0_end(self)

    def next_change(self):
        if self.dir == 1:
            d = +1 if self.frame + 1 < self.time else 0
        elif self.dir == 2:
            d = -1 if self.frame - 1 > 0 else 0
        else:
            d = 0
        if 1 <= self.dir <= 2:
            return self._f(self.frame + d, self.begin, self.diff, self.time) - self.val
        return 0

    @property
    def diff(self):
        return self.end - self.begin

    @property
    def val(self):
        return self._f(self.frame, self.begin, self.diff, self.time)

    def halt(self):
        self.dir = 0

    def enable(self):
        self.dir = 1

    def disable(self):
        self.dir = 2

    def reset(self):
        self.frame = 0

    def __repr__(self):
        return "<Anim frame {} of {} with value {} from range({}, {})>".format(self.frame, self.time, self.val,
                                                                               self.begin, self.end)


class Anim2D:
    __slots__ = ['x', 'y']

    def __init__(self, anim_x, anim_y, time=None, func=ease_in_out, rfunc=ease_in_out):
        self.x = anim_x if isinstance(anim_x, Anim) else (Anim(*anim_x) if len(anim_x) >= 3 else
                                                          Anim(*anim_x, time=time, func=func, rfunc=rfunc))
        self.y = anim_y if isinstance(anim_y, Anim) else (Anim(*anim_y) if len(anim_y) >= 3 else
                                                          Anim(*anim_y, time=time, func=func, rfunc=rfunc))

    @property
    def frame_0_end(self):
        assert self.x.frame_0_end is self.y.frame_0_end
        return self.x.frame_0_end

    @frame_0_end.setter
    def frame_0_end(self, val):
        self.x.frame_0_end = self.y.frame_0_end = val

    @property
    def frame_max_end(self):
        assert self.x.frame_max_end is self.y.frame_max_end
        return self.x.frame_max_end

    @frame_max_end.setter
    def frame_max_end(self, val):
        self.x.frame_max_end = self.y.frame_max_end = val

    def update(self, step=1):
        self.x.update(step)
        self.y.update(step)

    @property
    def begin(self):
        assert self.x.begin == self.y.begin
        return self.x.begin

    @begin.setter
    def begin(self, val):
        self.x.begin = self.y.begin = val

    @property
    def end(self):
        assert self.x.end == self.y.end
        return self.x.end

    @end.setter
    def end(self, val):
        self.x.end = self.y.end = val

    @property
    def dir(self):
        assert self.x.dir == self.y.dir
        return self.x.dir

    @dir.setter
    def dir(self, val):
        self.x.dir = self.y.dir = val

    @property
    def frame(self):
        assert self.x.frame == self.y.frame
        return self.x.frame

    @property
    def time(self):
        assert self.x.time == self.y.time
        return self.x.time

    @time.setter
    def time(self, val):
        self.x.time = self.y.time = val

    @property
    def val(self):
        assert self.x.val == self.y.val
        return self.x.val

    @property
    def vals(self):
        return self.x.val, self.y.val

    def halt(self):
        self.x.dir = self.y.dir = 0

    def enable(self):
        self.x.dir = self.y.dir = 1

    def disable(self):
        self.x.dir = self.y.dir = 2

    def reset(self):
        self.x.frame = self.y.frame = 0

    def __repr__(self):
        return "<Anim2D {} {}>".format(self.x, self.y)
