from pygame import sprite, Rect, draw
import random
from .util import blit_rotated, extval, randd, tiledv
from .const import QLOW, QMEDIUM, QHIGH, config


class Particle(sprite.Sprite):
    __slots__ = ['level', 'image', 'rect', 'angle', 'lifetime', 'bounding', 'shadeout_step']

    def __init__(self, level, graphic, rect, angle=0, lifetime=-1, shadeout=True, bounding=None):
        """Particle(game, graphic, rect, angle=0, lifetime=-1, shadeout=True, bounding=screen.get_rect()) -> Particle"""
        sprite.Sprite.__init__(self)
        self.level = level
        self.image, self.rect = graphic, Rect(rect)
        self.angle = angle
        self.lifetime = lifetime
        self.bounding = bounding if bounding is not None else level.game.screen.get_rect()
        self.shadeout_step = 255 / self.lifetime if shadeout else 0

        level.particles.append(self)

    def draw(self, dest):
        blit_rotated(dest, self.image, self.rect.center, self.angle)

    def update(self):
        if len(self.level.particles) >= self.level.max_entities:
            self.kill()
            return
        self.image.set_alpha(self.image.get_alpha() - self.shadeout_step)
        if 0 <= self.lifetime < 1 or not self.bounding.colliderect(self.rect):
            self.kill()
        elif self.lifetime > 0:
            self.lifetime -= 1

    def kill(self):
        sprite.Sprite.kill(self)
        self.level.particles.remove(self)


class FunctionalParticle(Particle):
    __slots__ = ['xvel', 'yvel', 'rotation', 'gravity', 'air_friction']

    def __init__(self, level, graphic, x, y, w, h, angle, xvel, yvel, rotation, **kw):
        """FunctionalProjectile(game, graphic, posx, posy, width, height, angle, xvel, yvel, rotation, **kw)

        Available **kw options:
          lifetime: number (frames) -- Time after which particle disappears, -1 for infinite
          shadeout: bool -- If True, particle will disappear smoothly
          gravity: number (px/frame) -- Speed of accelerating particle with gravity
          air_friction: number (px/frame) -- Force with which particle is slowed
          randomize: bool -- If True, partcle initial values will slightly change
        """
        Particle.__init__(self, level, graphic, (x, y, w, h), angle,
                          extval(kw, 'lifetime', -1), extval(kw, 'shadeout', False))
        self.xvel, self.yvel = tiledv(xvel), tiledv(yvel)
        self.rotation = rotation
        self.gravity = extval(kw, 'gravity', 0.2)
        self.air_friction = extval(kw, 'air_friction', 0.01)
        if extval(kw, 'randomize', True):
            self.xvel += random.random()*randd()
            self.yvel += random.random()*randd()
            self.lifetime += random.random()*10*randd()
            self.rotation += random.random()*2*randd()

    def update(self):
        Particle.update(self)
        self.angle += self.rotation
        if self.xvel >= self.air_friction:
            self.xvel -= self.air_friction
        elif self.xvel <= -self.air_friction:
            self.xvel += self.air_friction
        else:
            self.xvel = 0
        self.rect.x += self.xvel
        self.yvel += self.gravity
        self.rect.y += self.yvel


def collapse_particles(game, graphic, rect, share_graphic=False):
    """collapse_particles(game, graphic, rect, share_graphic='auto') -> list of 9 FunctionalParticles"""
    if share_graphic:
        graphic = graphic.copy()
    step = rect.width / 3
    entities = []
    angle, rotation, lifetime = 0, 2, 50 * (config.q + 1)
    for x in range(3):
        for y in range(3):
            if not share_graphic:
                graphic = graphic.copy()
            entities.append(
                FunctionalParticle(game, graphic,
                                   rect.x + step * x, rect.y + step * y,
                                   step, step, angle,
                                   # give correct speeds depending on position in 3x3 grid
                                   ({0: -2, 1: 0, 2: 2}[x]), ({0: -2, 1: 0, 2: 2}[y]),
                                   rotation, lifetime=lifetime,
                                   # shade out only first element if are using the same graphic to avoid 9x faster
                                   # shading of particles
                                   shadeout=((x == 0 and y == 0) or not share_graphic) and config.q > QMEDIUM)
            )
    return entities


def h_particles(level, graphic, point, share_graphic=False, yvel=2):
    if share_graphic:
        graphic = graphic.copy()
    graphic_rect = graphic.get_rect()
    ent = []
    angle, rotation, lifetime = 0, 0, 100
    for i in range(3):
        if not share_graphic:
            graphic = graphic.copy()
        ent.append(
            FunctionalParticle(level, graphic, point[0], point[1], graphic_rect.width, graphic_rect.height, angle,
                               {0: -2, 1: 0, 2: 2}[i], yvel, rotation, lifetime=lifetime,
                               shadeout=(i == 0 or not share_graphic))
        )
    return ent


def spread_particles(level, graphic, point, xvel, yvel, dir_, share_graphic=True):
    if config.q <= QMEDIUM:
        return []
    if share_graphic:
        graphic = graphic.copy()
    w, h = graphic.get_rect().size
    ent = []
    angle, rotation, lifetime = 0, 0, 50
    for x in range(2):
        for y in range(1):
            if not share_graphic:
                graphic = graphic.copy()
            ent.append(
                FunctionalParticle(level, graphic, point[0]-w/2*(x+1), point[1]-h/2*(y+1), w, h, angle,
                                   {0: -xvel, 1: -xvel if dir_ == 'x' else xvel}[x], {0: -yvel, 1: -yvel if dir_ == 'y' else yvel}[y],
                                   rotation, lifetime=lifetime,
                                   shadeout=((x == 0 and y == 0) or not share_graphic))
            )
