from configparser import ConfigParser
from os import path, remove
from .log import log


VALID_GAMEMODES = ('normal', '1death', 'vvvvvv')


class Save:
    def __init__(self, filepath, template_filepath='./save/sav_template.ini'):
        self.filepath = filepath
        self.template_filepath = template_filepath

        if not path.isfile(filepath):
            filepath = template_filepath

        self.p = ConfigParser()
        self.p.read(filepath)

    @property
    def progress(self):
        return int(self.p['Save']['LevelProgress'])

    @progress.setter
    def progress(self, v):
        self.p['Save']['LevelProgress'] = str(int(v))

    @property
    def gamemode(self):
        v = self.p['Save']['Mode'].lower()
        assert v in VALID_GAMEMODES
        return v

    @gamemode.setter
    def gamemode(self, v):
        assert v.lower() in VALID_GAMEMODES
        self.p['Save']['Mode'] = v

    @property
    def deaths(self):
        return int(self.p['Save']['TotalDeaths'])

    @deaths.setter
    def deaths(self, v):
        self.p['Save']['TotalDeaths'] = str(int(v))

    @property
    def currentleveldeaths(self):
        return int(self.p['Save']['CurrentLevelDeaths'])

    @currentleveldeaths.setter
    def currentleveldeaths(self, v):
        self.p['Save']['CurrentLevelDeaths'] = str(int(v))

    @property
    def frames(self):
        return int(self.p['Save']['TotalFramesPlayed'])

    @frames.setter
    def frames(self, v):
        self.p['Save']['TotalFramesPlayed'] = str(int(v))

    @property
    def stepframes(self):
        return int(self.p['Save']['TotalStepFramesPlayed'])

    @stepframes.setter
    def stepframes(self, v):
        self.p['Save']['TotalStepFramesPlayed'] = str(int(v))

    @property
    def jumps(self):
        return int(self.p['Save']['TotalJumps'])

    @jumps.setter
    def jumps(self, v):
        self.p['Save']['TotalJumps'] = str(int(v))

    # noinspection PyMethodMayBeStatic
    def endlevel_update(self):
        # Empty function just waiting to have something useful implemented!

        # Still empty  :(

        if False:
            log("[INFO] endlevel_update() called!")

    def apply(self, game):
        game.current_level_index = self.progress

        game.load_current_level()

        if self.gamemode == 'normal' or self.gamemode == '1death':
            game.level.player.jumping_style = 0
        elif self.gamemode == 'vvvvvv':
            game.level.player.jumping_style = 1

    def save(self, filepath=None):
        if filepath is None:
            filepath = self.filepath
        with open(filepath, 'w') as f:
            self.p.write(f)
        log("[INFO] Saved {}".format(repr(filepath)))

    def delete(self):
        if path.isfile(self.filepath):
            remove(self.filepath)
            log("[INFO] Deleted {}".format(repr(self.filepath)))
        else:
            log("[ERR ] No file {} to delete".format(repr(self.filepath)))
