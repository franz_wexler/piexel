from pygame import draw, Color
from .const import TILE_EDGE
from .entities import Entity
from .easeanim import Anim
from .graphic import color_variant
from .storylib import Balloon


__all__ = ["OmniCircle"]


class OmniCircle(Entity):
    def __init__(self, level, x, y, r=TILE_EDGE*3):
        Entity.__init__(self, level, None, (x-r*1.2/2, y-r*1.2/2, r*1.2/2, r*1.2/2))
        self.pos = x, y
        self.r = r
        self.r2 = r * 0.3

        self.col = '#A44444'
        self.col2 = '#A04444'

        self.anim = Anim(r * 0.8, r * 1.2, 100)
        self.anim.enable()
        self.anim.frame_0_end = self.anim.enable
        self.anim.frame_max_end = self.anim.disable

        self.anim2 = Anim(self.r2 * 0.6, self.r2 * 1.4, 190)
        self.anim2.enable()
        self.anim2.frame_0_end = self.anim2.enable
        self.anim2.frame_max_end = self.anim2.disable

        self.anim3 = Anim(0, -30, 90)
        self.anim3.enable()
        self.anim3.frame_0_end = self.anim3.enable
        self.anim3.frame_max_end = self.anim3.disable

        self.anim4 = Anim(0, 20, 90)
        self.anim4.enable()
        self.anim4.frame_0_end = self.anim4.enable
        self.anim4.frame_max_end = self.anim4.disable

        level.omni_circle = self

    def update(self):
        self.anim.update()
        self.anim2.update()
        self.anim3.update()
        self.anim4.update()

    def draw(self, dest):
        draw.circle(dest, Color(color_variant(self.col, self.anim3.val)), self.pos, int(self.anim.val))
        draw.circle(dest, Color(color_variant(self.col2, self.anim4.val)), self.pos, int(self.anim2.val))

    def say(self, *texts, final_dismiss_callback=None, bind_movement=False, _i_start=0):
        def p_adddialog(txt):
            def _t():
                self.level.balloons.append(Balloon(self.level, self, txt))
                if "CRASH" in txt:
                    self.anim.halt()
                    self.anim2.halt()
                    self.anim3.halt()
                    self.anim4.halt()
            return _t

        for i, t in enumerate(texts):
            self.level.schedule_event(_i_start + i + 1, p_adddialog(t), "ocstoryline<{}>".format(repr(t)))

        if final_dismiss_callback is not None:
            self.level.schedule_event(_i_start + len(texts) + 1, final_dismiss_callback)

