from pygame import image, Rect, Surface
from random import randrange, random
from .const import TILE_EDGE, SCREEN_SIZE, MAP_SIZE
from .util import tiledv
from .graphic import arcade_strip, arcade_noise


__all__ = ['MonitorBlur', 'SlowDown']


class MonitorBlur:
    def __init__(self, level, kill_on_level_end=True):
        self.game = level.game
        if hasattr(level.game, 'overlay_effect') and isinstance(level.game.overlay_effect, MonitorBlur):
            self.image, self.rect = level.game.overlay_effect.image, level.game.overlay_effect.rect
            self.stripes = level.game.overlay_effect.stripes
        else:
            self.image = level.game.graphic.arcade_mask.copy()
            self.image.blit(arcade_noise(), (0, 0))
            self.rect = self.image.get_rect()
            self.stripes = [MonitorBlurStripe(self), MonitorBlurStripe(self, y=SCREEN_SIZE[1]//2),
                            MonitorBlurStripe(self, y=SCREEN_SIZE[1]//6)]
        self.kill_on_level_end = kill_on_level_end
        level.overlay_effect = self
        self.f = (0, 0)
        self.t = 0

    def update(self):
        for s in self.stripes:
            s.update()
        if self.t % 60 == 0 and random() < 0.6:
            self.f = (randrange(-SCREEN_SIZE[0], 0, TILE_EDGE//4), randrange(-SCREEN_SIZE[1], 0, TILE_EDGE//4))

    def draw(self, dest):
        for s in self.stripes:
            s.draw(dest)
        dest.blit(self.image, self.f)

    def new_stripe(self):
        self.stripes.append(MonitorBlurStripe(self))

    def end_level(self):
        self.game.overlay_effect = None

    @staticmethod
    def exit():
        pass


class MonitorBlurStripe:
    __slots__ = ['master', 'rect', 'image', 'yvel']

    def __init__(self, retro_screen_anim, h=-1, y=-1):
        self.master = retro_screen_anim
        if h == -1:
            h = randrange(int(tiledv(14)), int(tiledv(100)))
        if y == -1:
            y = -tiledv(100)
        self.rect = Rect((0, y), (self.master.rect.width, h))
        self.image = arcade_strip(self.rect.h/TILE_EDGE)
        self.yvel = tiledv(2)

    def update(self):
        self.rect.y += self.yvel
        if self.rect.top > self.master.rect.height:
            self.master.stripes.remove(self)
            self.master.new_stripe()

    def draw(self, dest):
        dest.blit(self.image, self.rect)


class SlowDown:
    def __init__(self, level):
        self.level = level

        self.alpha = 80
        self.surface = Surface(SCREEN_SIZE)

        level.overlay_effect = self
        level.force_drawing_view = True

    def update(self):
        self.level.view.set_alpha(self.alpha)
        if not self.level.paused and self.level.game.frame % 2 == 0:
            self.surface.blit(self.level.view, (0, 0))  # (randrange(SCREEN_SIZE[0]), randrange(SCREEN_SIZE[1])))

        # image.save(self.surface, 'debug1.png')
        # self.level.view.set_alpha(0)

    def draw(self, dest):
        self.level.game.screen.fill((0, 0, 0))
        # dest.fill((0, 0, 0))
        dest.blit(self.surface, (0, 0))
        self.level.game.screen.blit(self.level.view, (0, 0))

    def exit(self):
        self.level.force_drawing_view = False
