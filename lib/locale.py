from .const import LANG
from .config import config
from .log import log

PL_TRANSLATION = {"play": "graj",
                  "credits": "o grze",
                  "exit": "wyjdź z gry",

                  "continue game": "kontynuuj grę",
                  "new game": "nowa gra",
                  "cannot create more games": "nie można stworzyć więcej zapisów",
                  "choose a game mode": "wybierz tryb gry",
                  "normal": "normalny", "1 death": "1 śmierć", "vvvvvv": "vvvvvv",
                  "delete game": "usuń grę",
                  "no games to delete": "brak gier do usunięcia",
                  "cancel": "anuluj",
                  "back": "wróć",

                  "main menu": "menu główne",
                  "settings": "ustawienia",
                  "walkthrough": "rozwiązanie",
                  "restart level": "restartuj",
                  "resume": "wznów grę",

                  "sound": "dźwięk",
                  "on": "wł.", "off": "wył.",
                  "quality": "jakość", "low": "niska", "medium": "średnia", "high": "wysoka",
                  "resolution": "okno",
                  "fullscreen": "pełny ekran",
                  "controls": "sterowanie", "wasd": "wasd", "arrow keys": "strzałki",
                  "language": "język", "english": "angielski", "polish": "polski",
                  "save changes": "zapisz zmiany",

                  "you lost in 1 death mode!": "przegrałeś w trybie 1 śmierci!",
                  "retry": "spróbuj ponownie",
                  "death": "śmierć", "deaths": "śmierci", "on this level": "na tym poziomie",

                  "you completed the game!": "ukończyłeś grę!",

                  "1 death mode is not unlocked yet.": "tryb jednej śmierci zablokowany",
                  "complete game in normal mode": "ukończ grę w normalnym trybie",

                  "error": "błąd",
                  "too big resolution for fullscreen": "za duża rozdzielczość",
                  "ok": "ok",

                  "help on this element is not available": "pomoc dla tego elementu nie jest dostępna",

                  "press any key or click to continue": "wciśnij dowolny klawisz lub kliknij aby kontynuować"
                  }

EN_TOOLTIPS = {"": "help on this item is not available",

               "help": "enable context help system\n"
                       "after enabling click on any widget or\n"
                       "game object to display informations\n"
                       "about it.\n"
                       "press any key or click to close",

               "play": "start or continue playing the game",
               "settings": "customize game options like screen resolution,\n"
                           "quality and controls",
               "credits": "learn more about author and his research",
               "exit": "exit piexel and return to windows",

               "continueagame": "continue game from last level",
               "newgame": "open dialog to create new save",
               "deletegame": "open dialog to delete existing save",
               "saveslotback": "go back to main menu",

               "deleteagame": "permanently delete save\n"
                              "warning: this cannot be undone",
               "deleteslotback": "cancel",

               "modenormal": "create a save in normal mode\n"
                             "normal mode means standard story and normal gameplay",
               "mode1death": "create a save in 1 death mode\n"
                             "in 1 death mode the save is deleted if you die more than\n"
                             "one time a level.\n"
                             "\"failure is not an option. it comes bundled.\"\n"
                             "this mode is unlocked after completing normal mode",
               "modevvvvvv": "create a save in vvvvvv mode\n"
                             "vvvvvv mode is inspired by terry cavanagh's game 'vvvvvv'\n"
                             "in vvvvvv mode you cannot jump, but you can reverse gravity",
               "newsaveback": "cancel",

               "pause": "pause temporarily game\n"
                        "opens menu from which you can return to main menu, change\n"
                        "settings, see walkthrough and restart level.\n"
                        "while this menu is open the game is paused",

               "backtomainmenu": "return to main menu\n"
                                 "game is autosaved on beginning of each level",
               "walkthrough": "get help with passing this level\n"
                              "automatically solves the level, but it is considered as cheating",
               "restartlevel": "read level file again and reload level.\n"
                               "may be useful if you missed something on level and you can't go back",
               "unpause": "close this menu, unpause the game and resume playing",

               "sound": "turn sound on or off\n"
                        "turning sound off does not speed game up",
               "quality": "set game quality\n"
                          "in case of lags setting quality to low usually speeds game up\n"
                          "when there are no problems with game speed it is recommended\n"
                          "to set quality to high",
               "resolution": "set size of game window\n"
                             "bigger sizes may slow game down.\n"
                             "under windows system it is unrecommended to set this size to bigger\n"
                             "than screen resolution",
               "fullscreen": "turn full screen mode on or off\n"
                             "in full screen mode game window fills whole screen\n"
                             "few screen resolutions and game window sizes may cause problems\n"
                             "with proportions. if this happens, turn full screen mode off or change\n"
                             "window size",
               "controlstyle": "change keyboard keys used to control player\n"
                               "in 'wasd' you use keys w, a, s and d\n"
                               "in 'arrow keys' you use arrow keys",
               "lang": "change game language\n"
                       "warning: it may be difficult to change it if you don't speak chosen\n"
                       "language and save the changes",
               "savechanges": "save changes made in this menu",

               "retry1death": "go back to first level and try again\n"
                              "delete this save and start new one in 1 death mode",
               "backtomainmenu1death": "return to main menu\n"
                                       "delete this save and return to main menu",

               "gamecompleteok": "view credits, then exit to main menu",

               "errorok": "close this window",

               "entrance": "entrance\n"
                           "entrance acts like checkpoint - after death player returns to it",
               "lexit": "exit\n"
                        "acquiring exit on every level is your goal",
               "player": "player\n"
                         "you are controlling him.\n"
                         "your goal is to get to exit",
               "spike": "spike\n"
                        "player dies after touching them.\n"
                        "your task is to prevent this happening.\n"
                        "some spikes can move and target player",
               "block": "block\n"
                        "player can walk on blocks but can't pass through them\n"
                        "some blocks can move and crush player",
               "platform": "platform\n"
                           "player can walk on platforms and go down\n"
                           "some platforms can move",
               "mover": "conveyor\n"
                        "conveyors speed player up if he goes in right direction and slow him down if in opposite\n",
               "graviter": "gravity line\n"
                           "when player touches gravity line, his gravitation is reversed",
               "catapult": "catapult\n"
                           "*boiiing*",
               "collapsingplatform": "collapsing platform\n"
                                     "when player touches this platform, it collapses.\n"
                                     "it respawns after player dies",
               "cannon": "turret\n"
                         "it can aim and shoot at player",
               "dialogballoon": "dialog balloon\n"
                                "shows speech between player and npcs.\n"
                                "to close press any key or click anywhere.\n"
                                "if you missed a dialog from this level, try reloading level\n"
                                "(pause game and then click 'restart level')"
               }

PL_TOOLTIPS = {"": "pomoc dla tego elementu nie jest dostępna",

               "help": "włącza tryb pomocy kontekstowej\n"
                       "po włączeniu kliknij na dowolny element interfejsu\n"
                       "lub obiekt na poziomie aby uzyskać o nim szczegółową\n"
                       "pomoc.\n"
                       "po wyświetleniu pomocy kliknij w dowolnym miejscu\n"
                       "na ekranie lub naciśnij dowolny klawisz aby ją zamknąć",

               "play": "otwiera dialog wyboru zapisu\n"
                       "pozwala zarządzać zapisami, tworzyć je i usuwać\n"
                       "oraz kontynuować istniejące",
               "settings": "dostosuj ustawienia gry takie jak rozdzielczość ekranu,\n"
                           "jakość rozgrywki oraz klawisze sterowania",
               "credits": "dowiedz się więcej o autorze gry i źródłach z których\n"
                          "korzystał",
               "exit": "wyjdź z gry i wróć do windows\n"
                       "aby wrócić potem do tej aplikacji należy ją uruchomić\n"
                       "ponownie",

               "continueagame": "kontynuuj grę od ostatniego poziomu",
               "newgame": "otwórz dialog tworzenia nowego zapisu",
               "deletegame": "otwórz dialog usuwania istniejącego zapisu.\n"
                             "nieaktywne w przypadku braku gier do usunięcia",
               "saveslotback": "wróć do menu głównego",

               "deleteagame": "usuń bezpowrotnie zapis\n"
                              "uwaga: tej akcji nie można cofnąć",
               "deleteslotback": "anuluj usuwanie zapisu",

               "modenormal": "stwórz nowy zapis w normalnym trybie\n"
                             "normalny tryb oznacza standardową fabułę i normalną rozgrywkę",
               "mode1death": "stwórz nową grę w trybie 1 śmierci\n"
                             "w trybie 1 śmierci przegrywasz jeśli zginiesz więcej niż raz\n"
                             "na danym poziomie.\n"
                             "ten tryb gry jest odblokowywany po przejściu normalnego trybu",
               "modevvvvvv": "stwórz nową grę w trybie vvvvvv\n"
                             "tryb vvvvvv został zainspirowany grą terry'ego cavanagha 'vvvvvv'.\n"
                             "w trybie vvvvvv nie możesz skakać, a poziomy przechodzić musisz\n"
                             "odwracając grawitację gracza",
               "newsaveback": "anuluj",

               "pause": "wstrzymaj tymczasowo grę\n"
                        "otwiera menu z którego możesz wrócić do menu głównwgo, zmienić\n"
                        "ustawienia, zobaczyć rozwiązanie i wczytać ponownie poziom.\n"
                        "gdy to menu jest otwarte, gra jest wstrzymana",

               "backtomainmenu": "wróć do menu głównego\n"
                                 "gra jest zapisywana automatycznie na początku\n"
                                 "każdego poziomu",
               "walkthrough": "przejdź automatycznie poziom\n"
                              "gra jest w stanie przejść sama ten poziom, ale będzie\n"
                              "to zapisane jako oszustwo",
               "restartlevel": "wczytaj poziom ponownie z dysku. przydatne, gdy ominąłęś na\n"
                               "obecnym poziomie ważną część gry i nie masz możliwości powrotu",
               "unpause": "zamknij to menu i wznów grę",

               "sound": "włącz lub wyłacz dźwięk\n"
                        "wyłączenie dźwięku nie przyspiesza gry",
               "quality": "ustaw jakość gry\n"
                          "w wypadku zacinania się gry możesz zmniejszyć jakość, co zwykle\n"
                          "ją przyspiesza.\n"
                          "w przypadku braku problemów z wydajnością zalecane jest\n"
                          "ustawienie jakości na wysoką",
               "resolution": "ustaw wielkość okna gry\n"
                             "większe rozmiary mogą spowalniać grę.\n"
                             "pod systemem windows niezalecane jest używanie okna o wielkości\n"
                             "większej od rozdzielczości monitora.",
               "fullscreen": "włacz lub wyłącz tryb pełnego ekranu\n"
                             "w trybie pełnego ekranu okno gry zajmuje cały ekran.\n"
                             "pod niektórymi rozmiarami okna i rozdzielczościami ekranu obraz\n"
                             "może nie zachowywać proporcji, należy wtedy wyłączyć pełny\n"
                             "ekran lub zmienić rozmiar okna",
               "controlstyle": "zmień sposób sterowania graczem\n"
                               "w 'wasd' steruje się klawiszami w, a, s i d po lewej stronie\n"
                               "klawiatury.\n"
                               "w 'klawisze strzałek' steruje się klawiszami strzałek po prawej\n"
                               "stronie klawiatury",
               "lang": "zmień język gry\n"
                       "uwaga: to ustawienie w przypadku braku znajomosci wybranego\n"
                       "języka może byc trudno cofnąć po zapisaniu zmian",
               "savechanges": "zapisz zmiany dokonane w tym menu\n"
                              "aby anulować zmiany ustaw wszystkie opcje na wcześniejsze\n"
                              "wartości lub zamknij okno gry",

               "retry1death": "zacznij od nowa od początku gry\n"
                              "skasuj obecny zapis gry i rozpocznij nowy w trybie 1 śmierci",
               "backtomainmenu1death": "wróć do menu głównego\n"
                                       "skasuj obecny zapis gry i wróć do menu głównego",

               "gamecompleteok": "zobacz informacje o grze i wyjdź\n"
                                 "do menu głównego",

               "errorok": "zamknij okno o błędzie",

               "entrance": "wejście\n"
                           "pełni rolę punktu zapisu gry. po śmierci gracz jest\n"
                           "do niego z powrotem przenoszony",
               "lexit": "wyjście\n"
                        "dotarcie do wyjścia na każdym poziomie jest celem gry",
               "player": "gracz\n"
                         "ty sterujesz graczem, twoim celem jest dostanie\n"
                         "się nim do wyjścia",
               "spike": "kolec\n"
                        "gracz ginie po wpadnięciu na kolce.\n"
                        "twoim zadaniem jest ich unikać\n"
                        "niektóre kolce mogą się ruszać oraz wykrywać gracza",
               "block": "blok\n"
                        "gracz może chodzić po blokach, ale nie może przez\n"
                        "nie przechodzić.\n"
                        "niektóre bloki mogą się ruszać i przenosić gracza,\n"
                        "a także zabijać go jeśli zostaje nimi zmiażdżony",
               "platform": "platforma\n"
                           "gracz może chodzić po platformach, może po nich zejść\n"
                           "także w dół przytrzymując klawisz ruchu w dół.\n"
                           "niektóre platformy mogą się ruszać i przenosić gracza",
               "mover": "przenośnik\n"
                        "przenośniki przyspieszają gracza gdy ten chodzi po nich w odpowiednim\n"
                        "kierunku i spowalniają go, gdy w przeciwnym",
               "graviter": "linia zmiany grawitacji\n"
                           "gdy gracz odbija się od linii zmiany grawitacji\n"
                           "jego grawitacja zmienia kierunek",
               "catapult": "katapulta\n"
                           "gdy gracz wejdzie na katapultę jest przez nią wystrzeliwany",
               "collapsingplatform": "zapadająca się platforma\n"
                                     "gdy gracz wejdzie na taką platformę, ta po chwili się zapada.\n"
                                     "pojawia się ponownie po śmierci gracza lub zresetowaniu poziomu",
               "cannon": "wieżyczka\n"
                         "potrafi celować i strzelać w gracza",
               "dialogballoon": "balonik mowy\n"
                                "naciśnij dowolny klawisz lub kliknij w obszarze gry, aby kontynuować"}

EN_STORY = {0: ("oh, where am i?",
                "i think i am in a minimalistic game where\n"
                "the goal is to reach the door to the right",
                "anyway, this game has beautiful\n"
                "minimalistic design.\n"
                "it is worth playing!"),
            1: ("oh no!\n"
                "the ground has moved up!",
                "luckily, i can jump like in every\n"
                "platform game."),
            2: ("it looks like these are going to hurt me.\n"
                "i should avoid them.",),
            3: ("maybe animation of me dying is\n"
                "really nice, but you still should\n"
                "avoid throwing me into spikes.",),
            4: ("ok, the challenge begins.",),
            5: ("the challenge has begun.\n"
                "can i now go home?",),
            6: ("when i think about it...\n"
                "i don't remember my home.",
                "should i worry?",
                "of course not.\n"
                "i should complete the game."),
            7: ("a platform!",
                "i saw one in...",
                "in...",
                "i can't remember where, but i know\n"
                "exactly how it works.",
                "this would be too long to explain, try\n"
                "context help system. it helps you in\n"
                "every situation!"),
            8: ("i think this level is still easy.\n"
                "just remember you can press {s}\n"
                "to fall off of a platform.",),
            9: ("it may look like the game isn't\n"
                "counting my deaths, but it is.",
                "if you complete the game with low\n"
                "death count don't forget to try\n"
                "out 1 death mode.\n"
                "it is unlocked after you complete\n"
                "normal mode."),
            10: ("collapsing platforms!",
                 "this is getting better by the moment."),
            11: ("this level looks suspiciously simple.\n"
                 "something must be wrong with it.",
                 "like collapsing spikes."),
            12: ("these spikes are really dangerous!",
                 "however i feel something worse will come."),
            13: ("minimalistic conveyor belts?\n"
                 "someone is running out of ideas.",),
            14: ("i was right: the author of this level\n"
                 "added just 7 spikes. no creativity.\n"
                 "what difference make few spikes?",),
            15: ("i feel an evil presence watching me...",  # reference to Eye of Cthulhu from Terraria
                 "and shooting me with square bullets!"),
            16: ("turrets keep shooting. spikes keep\n"
                 "falling. platforms keep collapsing.\n"
                 "i keep dying.",),
            17: ("wow, is it a famous test element\n"
                 "shamefully stolen by author of\n"
                 "this game from terry cavanagh's\n"
                 "vvvvvv - gravity lines?",
                 "somehow i know it is.\n"
                 "why do i know things, but never\n"
                 "remember them?"),
            18: ("maybe i am winning in a long run,\n"  # reference to a Dilbert strip
                 "but i want to rest now.",),

            19: ("what... what's this?",
                 "a boss?",
                 "yes, probably boss.\n"
                 "how do i defeat it?"),
            19.1: ("hey, you there!\n"
                   "i'm here to defeat you!",),
            19.2: ("no, you aren't.",),
            19.3: ("no?",),
            19.4: ("no, i'm the final narrator to explain\n"
                   "why you don't remember anything.",),
            19.5: ("well...",
                   "why i don't remember anything, then?"),
            19.6: ("because it is a minimalist game.\n"
                   "you are just a square. without soul.\n"
                   "and memory.",),
            19.7: ("that's the explanation?\n"
                   "everything i need to know?",),
            19.8: ("yes.\n"
                   "now you can only exit this level,\n"
                   "end game, watch credits and support\n"
                   "the developer.",
                   "now proceed to the exit."),
            20: ("no!",
                 "this game is too short.\n"
                 "i want to play it longer."),
            20.1: ("i'm just a final narrator in game\n"
                   "with story composed mainly of\n"
                   "your monologue.\n"
                   "why should i care it's finally the\n"
                   "end?",),
            20.2: ("...well...",
                   "...i understand.\n"
                   "goodbye, narrator."),
            20.3: ("now proceed to the exit.",)
            }

PL_STORY = {0: ("gdzie jestem?",
                "chyba w minimalistycznej grze, gdzie celem jest\n"
                "dotarcie do drzwi na prawo.",
                "w każdym razie, ta gra ma przepiękny design.\n"
                "warto w nią zagrać!"),
            1: ("o nie!\n"
                "ziemia się podniosła!",
                "na szczęście mogę skakać, jak w każdej grze\n"
                "platformowej."),
            2: ("te kolce wyglądają na ostre.\n"
                "powinienem ich unikać.",),
            3: ("może animacja mojej śmierci jest naprawdę ładna,\n"
                "ale i tak powinieneś unikać wrzucania mnie na kolce.",),
            4: ("wyzwania się zaczynają.",),
            5: ("wyzwania się zaczęły.\n"
                "mogę już iść do domu?",),
            6: ("kiedy o tym myślę...\n"
                "nie pamiętam mojego domu.",
                "powinienem się martwić?",
                "oczywiście, że nie.\n"
                "powinienem ukończyć tę grę."),
            7: ("platforma!",
                "widziałem taką w...",
                "w...",
                "nie pamietam gdzie, ale wiem dokładnie jak działa.",
                "to by było długo do wyjaśniania, spróbuj użyć systemu\n"
                "pomocy kontekstowej. pomaga w każdej sytuacji!"),
            8: ("ten poziom też jest raczej łatwy.\n"
                "wystarczy pamiętać, że możesz nacisnąć {s}\n"
                "aby zejść z platformy.",),
            9: ("może wygląda na to, że ta gra nie liczy moich śmierci,\n"
                "ale je liczy.",
                "jeśli ukończysz grę z małą liczbą śmierci, nie zapomnij\n"
                "wypróbować trybu 1 śmierci.\n"
                "jest odblokowywany po ukończeniu normalnego trybu."),
            10: ("zapadające się platformy!",
                 "robi się coraz trudniej z każdą chwilą."),
            11: ("ten poziom wygląda podejrzanie prosto.\n"
                 "coś musi być z nim nie w porządku.",
                 "na przykład spadające kolce."),
            12: ("te kolce są naprawdę niebezpieczne!",
                 "ale czuję, że coś gorszego jeszcze się pojawi."),
            13: ("minimalistyczne przenośniki?\n"
                 "komuś się kończą pomysły.",),
            14: ("miałem rację: autor tego poziomu dodał tylko\n"
                 "7 kolców. brak kreatywności.\n"
                 "jaką różnicę robi kilka kolców?",),
            15: ("czuję, że znowu coś chce mnie zabić...",
                 "tym razem strzelając we mnie kwadratowymi nabojami!"),
            16: ("działa ciągle strzelają, platformy ciagle się zapdają.\n"
                 "kolce ciagle spadają.\n"
                 "ja ciągle ginę.",),
            17: ("czy to jest ten sławny element poziomu splagiatowany\n"
                 "przez autora piexela z gry terry'ego cavanagha\n"
                 "vvvvvv - linie zmiany grawitacji?",
                 "skądś wiem, że tak.\n"
                 "dlaczego ciagle wiem rzeczy, ale nigdy nie pamiętam\n"
                 "skąd?"),
            18: ("może i w większej perspektywie jestem zwycięzcą,\n"
                 "ale w mniejszej chciałbym odpocząć.",),

            19: ("co... co to jest?",
                 "boss?",
                 "tak, pewnie boss.\n"
                 "jak go się pokonuje?"),
            19.1: ("hej, ty tam!\n"
                   "jestem tu, aby cię pokonać!",),
            19.2: ("nie, nie jesteś.",),
            19.3: ("nie?",),
            19.4: ("nie, jestem narratorem zakończenia gry, który wyjaśnia\n"
                   "ci dlaczego nic nie pamiętasz.",),
            19.5: ("cóż...",
                   "dlaczego nic nie pamiętam, w takim razie?"),
            19.6: ("ponieważ to jest minimalistyczna gra.\n"
                   "jesteś tylko kwadratem bez duszy. bez pamięci.",),
            19.7: ("to jest to wyjaśnienie?\n"
                   "wszystko, co powinienem wiedzieć?",),
            19.8: ("tak.\n"
                   "teraz możesz tylko iść do wyjścia,\n"
                   "skończyć grę, obejrzeć napisy końcowe i wesprzeć\n"
                   "dewelopera.",
                   "teraz przejdź do wyjścia."),
            20: ("nie!",
                 "ta gra jest za krótka.\n"
                 "chcę w nią zagrać dłużej."),
            20.1: ("jestem tylko narratorem zakończenia gry w grze\n"
                   "z historią składającą się głównie z twojego monologu\n"
                   "dlaczego miałbym się martwić, że to nareszcie koniec?",),
            20.2: ("...cóż...",
                   "...rozumiem.\n"
                   "żegnaj, narratorze."),
            20.3: ("teraz przejdź do wyjścia.",)
            }


def l(str_en):
    return f(str_en.lower())


def f(str_en):
    if LANG == 'en':
        return str_en
    elif LANG == 'pl':
        if str_en in PL_TRANSLATION:
            return PL_TRANSLATION[str_en]
        else:
            log("[WARN] Missing translation string for {}".format(repr(str_en)))
            return str_en


def t(str_id, force_en=False):
    if LANG == 'en' or force_en:
        if str_id in EN_TOOLTIPS:
            return EN_TOOLTIPS[str_id]
        else:
            log("[WARN] No tooltip for {}".format(repr(str_id)))
            return "__missing__({})".format(repr(str_id))
    elif LANG == 'pl':
        if str_id in PL_TOOLTIPS:
            return PL_TOOLTIPS[str_id]
        else:
            log("[WARN] Missing translation of tooltip for {}".format(repr(str_id)))
            return t(str_id, True)


def m(lvl_id, force_en=False):
    if LANG == 'en' or force_en:
        if lvl_id in EN_STORY:
            y = EN_STORY[lvl_id]
        else:
            y = ()
    elif LANG == 'pl':
        if lvl_id in PL_STORY:
            y = PL_STORY[lvl_id]
        else:
            return m(lvl_id, True)
    else:
        return None

    y = list(y)
    for i, j in enumerate(y):
        y[i] = j.format(w=config.kw, a=config.ka, s=config.ks, d=config.kd)
    return y
