from pygame import image, Surface, Rect, Color
from .const import SCREEN_SIZE
from .config import LANG
from .graphic import rendertext, rendersize
from .easeanim import Anim, anim_linear


CREDITS_FILE = "./res/creditspl.txt" if LANG == 'pl' else "./res/creditsen.txt"


def get_credits_surface(filepath=CREDITS_FILE):
    big_size = 2
    small_size = 1

    color = Color('#EBEBEB')
    bg_color = Color('#808080')

    with open(filepath, encoding="utf-8") as f:
        st = str(f.read())
    w, h = SCREEN_SIZE[0], 0
    for ln in st.splitlines():
        if ln.startswith("#"):
            w2, h2 = rendersize(ln[1:], big_size)
        else:
            w2, h2 = rendersize(ln, small_size)
        h += h2
    h += SCREEN_SIZE[1]/2
    s = Surface((w, h+1))
    s.fill(bg_color)
    i = SCREEN_SIZE[1]/2
    for ln in st.splitlines():
        if ln.startswith("#"):
            w2, h2 = rendersize(ln[1:], big_size)
            s.blit(rendertext(ln[1:], big_size, color, bg_color, w), (w/2-w2/2, i))
        else:
            w2, h2 = rendersize(ln, small_size)
            s.blit(rendertext(ln, small_size, color, bg_color, w), (w/2-w2/2, i))
        i += h2
    return s


class CreditsMenu:
    def __init__(self, game):
        self.game = game
        self.image = get_credits_surface()

        self.anim = Anim(0, SCREEN_SIZE[1]/2-self.image.get_height(), 800, anim_linear)
        self.anim.enable()
        self.anim2 = Anim(0, 255, 30, anim_linear)
        self.anim2.enable()

    def update(self):
        self.anim.update()
        self.anim2.update()
        self.draw(self.game.screen)

    def draw(self, dest):
        self.image.set_alpha(self.anim2.val)
        dest.blit(self.image, (0, self.anim.val))

    @staticmethod
    def get_ui():
        return NotImplemented

    def click_event(self, event):
        self.dismiss()

    def mouse_motion_event(self, event):
        pass

    def dismiss(self):
        from .uit import MainMenu
        self.game.menu = MainMenu(self.game)

