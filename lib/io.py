from pygame import Rect
from math import sqrt
from .const import TILE_EDGE
from .entities import Entity
from .util import extval, tiledv


__all__ = ['EntryDoor', 'ExitDoor']


class Door(Entity):
    def __init__(self, level, x, y, animation_speed=2):
        Entity.__init__(self, level, None, (x, y, TILE_EDGE*3, TILE_EDGE*4))
        self.frame_rect = Rect(x+TILE_EDGE*0.5, y, TILE_EDGE*2, TILE_EDGE*4)  # 'frame' does refer to inner rect of door

        self.anim_frame = 0  # current frame of animation (distance between two wings / 2)
        self.anim_step = tiledv(animation_speed)  # how fast will door open and close
        self.anim_end = tiledv(16)  # how far the door will open from x center
        self.anim_length = int(self.anim_end / self.anim_step)
        # anim_end % anim_step should be 0

        # 0 if player is above the door, 1 if is entering the door and 2 if has entered (also in reverse order)
        # classes that inherit from door should change this
        self.rel_player_inter = 0

        self.state = 'closed'  # should be 'closed' or 'opened'

    def update(self):
        if self.state == 'closed':
            if self.anim_frame > 0:
                self.anim_frame -= self.anim_step
        elif self.state == 'opened':
            if self.anim_frame < self.anim_end:
                self.anim_frame += self.anim_step

    def predraw(self, dest):
        dest.blit(self.level.graphic.exit_bg, self.frame_rect)

    def draw(self, dest):
        dest.blit(self.level.graphic.exit_propack, (self.rect.x - TILE_EDGE*2, self.rect.y - TILE_EDGE*2))
        dest.blit(self.level.graphic.exit_door, (self.rect.left - self.anim_frame, self.rect.y))
        dest.blit(self.level.graphic.exit_door, (self.rect.right + self.anim_frame - self.rect.width/2, self.rect.y))
        if self.anim_frame == 0:
            dest.blit(self.level.graphic.exit_line, (self.rect.centerx, self.rect.y))

    def force_open(self):
        self.anim_frame = self.anim_end


class ExitDoor(Door):
    """Class representing exit from the level"""
    def __init__(self, level, x, y, **kw):
        """ExitDoor(level, level, wing_graphic, wings_sep_image, propack, posx, posy, **kw)

        Available **kw options:
          autoopen_distance: number (px) -- Distance from which door will open if player nearby
          animation_speed: number (px/frame) -- Speed of closing and opening door

        Background of exit should be drawed on level background
        """
        Door.__init__(self, level, x, y, extval(kw, 'animation_speed', 2))
        self.autoopen_distance = tiledv(extval(kw, 'autoopen_distance', 8*16))

        level.exit = self

    def update(self):
        Door.update(self)

        x, y = self.rect.center
        px, py = self.level.player.rect.center
        # check if exit should be opened to let player go
        if sqrt((abs(px - x) ** 2) + (abs(py - y) ** 2)) < self.autoopen_distance and self.rel_player_inter != 2:
            self.state = 'opened'
        else:
            self.state = 'closed'

        if self.frame_rect.contains(self.level.player.rect) and self.rel_player_inter == 0:
            self.rel_player_inter = 1
            self.level.player.zindex = 1
        elif (not self.frame_rect.colliderect(self.level.player.rect)) and self.rel_player_inter == 1:
            self.rel_player_inter = 2
            self.level.player.exited = True

            # try removing this line and holding jump key while exiting level
            self.level.player.b_xvel = self.level.player.b_yvel = self.level.player.gravity = 0
            self.level.player.disable_movement()

            if self.level.player.rect.x > self.rect.right:
                self.level.player.rect.x = self.rect.right

            self.level.end()


class EntryDoor(Door):
    def __init__(self, level, x, y, **kw):
        Door.__init__(self, level, x, y, extval(kw, 'animation_speed', 2))
        self.rel_player_inter = 1
        self.level.player.zindex = 1
        self.state = 'opened'

        level.entrance = self

        level.player.init_x, level.player.init_y = self.rect.x + TILE_EDGE*0.75, self.rect.y - TILE_EDGE*0.75
        level.player.rect.topleft = level.player.init_x, level.player.init_y
        level.player.rect_to_f()
        level.player.apply_nomove()

    def update(self):
        Door.update(self)
        if self.frame_rect.colliderect(self.level.player.rect):
            if self.rel_player_inter != 0:
                self.level.player.zindex = 1
                if self.frame_rect.contains(self.level.player.rect):
                    self.rel_player_inter = 0
                    self.level.player.zindex = 0
        else:
            self.state = 'closed'

    def re_anim(self):
        self.rel_player_inter = 1
        self.level.player.zindex = 1
        self.state = 'opened'


def _draw_doors(dest, level):
    level.entrance.draw(dest)
    if level.exit is not None:
        level.exit.draw(dest)


def _draw_ents(dest, ents):
    for e in ents:
        e.draw(dest)


def draw_io(dest, level, ents):
    level.entrance.predraw(dest)
    if level.exit is not None:
        level.exit.predraw(dest)
    if level.player.zindex == -1:
        _draw_doors(dest, level)
        _draw_ents(dest, ents)
    if level.player.zindex == 0:
        _draw_doors(dest, level)
        _draw_ents(dest, ents)
        level.player.draw(dest)
    elif level.player.zindex == 1:
        level.player.draw(dest)
        _draw_doors(dest, level)
        _draw_ents(dest, ents)
