from .testbase import *
from .io import *
from .gpu import *
from .tech import *
from .archived import *
from .advancedtriggers import *
from .movingessentials import *
from .bboss import *

from .anim import *
from .ui import *

from .graphic import *

from .util import log


class TestElements:
    # All test elements
    def __init__(self):
        i = 0
        for k, v in globals().items():
            if not k.startswith('_'):
                setattr(self, k, v)
                i += 1
        log("[INFO] Loaded {} elements to std".format(i))

std = TestElements()
