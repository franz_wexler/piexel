from itertools import chain
from .const import TILE_EDGE
from .entities import GravityDynamic
from .particles import collapse_particles
from .util import extval, address_str, take_snapshot, log


__all__ = ['Player']


class Player(GravityDynamic):
    """Class representing player in level

    Important: DO NOT add player to EntityGroup. Player must be updated last
    """
    def __init__(self, level, x=0, y=0, **kw):
        """Player(level, posx, posy, **kw) -> Player

        Available **kw options:
          godmode: bool -- If True, player is invulnerable
          noclipmode: bool -- If True, player is not affected by gravity nor blocks
        """
        y -= TILE_EDGE * 3
        GravityDynamic.__init__(self, level, level.graphic.player_surface, x, y, TILE_EDGE*1.5, TILE_EDGE*1.5,
                                jump_height=8.3, moving_speed=5.0, gravity=0.3, air_friction=0.5)
        self.godmode = extval(kw, 'godmode', False)
        self.noclipmode = extval(kw, 'noclipmode', False)

        # index of drawing player and doors' sprites;  0 if player is above, 1 if under them, -1 ifnot drawed
        self.zindex = 0

        self.snapshot_delay = 8
        self.snapshot_current_delay = -1

        level.player = self

    def update(self):
        if not self.noclipmode:
            GravityDynamic.update(self)
        else:
            self.update_noclip()

            self.zindex = 0

        if self.snapshot_current_delay >= 0:
            self.snapshot_current_delay -= 1
        if self.snapshot_current_delay == 0:
            take_snapshot()

        for b in chain(self.level.spikes, self.level.moving_spikes, self.level.greedy_spikes):
            if b.rect.colliderect(self.rect):
                self.init_kill()

    def update_noclip(self):
        if self.g_down:
            self.f_y += self.moving_speed
        elif self.g_up:
            self.f_y -= self.moving_speed
        if self.g_left:
            self.f_x -= self.moving_speed
        elif self.g_right:
            self.f_x += self.moving_speed
        self.f_to_rect()

    def init_kill(self, force=False):
        if self.godmode and not force:
            return

        self.level.macrofunc('preplayerdeath')

        if self.level.game.show_death_messages:
            log(" [INFO] Player {} is being killed".format(address_str(self)))

        if self.level.game.take_death_pictures:
            self.snapshot_current_delay = self.snapshot_delay

        self.level.entities.add(collapse_particles(self.level, self.level.graphic.player_gib, self.rect))

        d = False
        if self.level.game.save.gamemode == '1death' and self.level.game.save.currentleveldeaths >= 1:
            # great failure
            self.level.death1lose_menu.enable()
            self.rect.bottom = 0  # move off of the screen
            self.rect_to_f()
            self.level.schedule_event(10, lambda: setattr(self.level, 'paused', True))  # let player watch his blood

            self.level.game.save.delete()

            d = True
        else:
            self.goto_entrance()

        self.level.game.save.currentleveldeaths += 1
        self.level.game.save.deaths += 1
        if self.level.game.save.gamemode == '1death' and not d:
            self.level.game.save.save()
        self.level.update_deathscount_label()

        self.level.exit.rel_player_inter = 0  # fix bug pointed out by @TrueDobry

        self.level.macrofunc('playerdeath')

    def goto_entrance(self):
        # also revives removed_elements

        self.rect.x = self.level.entrance.rect.centerx - self.rect.w / 2
        self.rect.y = self.level.entrance.rect.top - TILE_EDGE
        self.rect_to_f()
        self.b_xvel = self.b_yvel = 0
        self.t_down = False
        self.reset_gravity()
        self.level.entrance.re_anim()
        self.apply_nomove()

        for e in chain(self.level.removed_elements.copy(), self.level.collapsing_platforms):
            e.revive()

    def apply_nomove(self, val=None):
        self.nomove_frames = val if val is not None else self.level.entrance.anim_length + 8

    def pack_translevel(self):
        return self.g_up, self.g_down, self.g_left, self.g_right, self.godmode, self.noclipmode, self.jumping_style

    def unpack_translevel(self, val):
        self.g_up, self.g_down, self.g_left, self.g_right, self.godmode, self.noclipmode, self.jumping_style = val
