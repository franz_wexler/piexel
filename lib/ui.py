from pygame import font, draw, key, Rect, Color, Surface, KMOD_SHIFT
from inspect import signature
from .const import TILE_EDGE, SCREEN_SIZE, MAP_SIZE
from .graphic import filled_rect
from .util import distance
from .easeanim import Anim
from .config import config, QMEDIUM


def num_args(func):
    return len(signature(func).parameters)


class RawTextOverlay:
    def __init__(self, font_size=TILE_EDGE*3/4, sep="  ", style='smp', text_color='#FFFFFF', bg_color='#808080'):
        self.font_size = int(font_size - 2)
        self.line_size = int(font_size)
        self.font = font.SysFont('Courier New', self.font_size)
        self.sep = sep
        assert style in ('dis', 'smp')
        self.style = style
        self.color = Color(text_color)
        self.bg_color = Color(bg_color)
        self._content = (())
        self.max_len = 0

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, val):
        self._content = val
        self.max_len = self.font.size(self.sep.join(max(self._content,
                                                        key=lambda item: self.font.size(self.sep.join(item)))))[0]

    def raw_content(self):
        return "\n".join([self.sep.join(row) for row in self.content])

    def draw(self, dest):
        if self.style == 'dis':
            draw.rect(dest, (192, 192, 192), Rect(0, 0, self.max_len, len(self.content) * self.line_size))
            for i, row in enumerate(self.content):
                dest.blit(self.font.render(self.sep.join(row), True, (255, 255, 255)), (1, i*self.line_size+1))
                dest.blit(self.font.render(self.sep.join(row), True, (128, 128, 128)), (0, i*self.line_size))
        elif self.style == 'smp':
            for i, row in enumerate(self.content):
                dest.blit(self.font.render(self.sep.join(row), False, self.color, self.bg_color), (0, i*self.line_size))


class BaseWidget:
    def __init__(self, level, x, y, w, h, description):
        self.level = level
        self.rect = Rect(x, y, w, h)
        self.description = description

    def update(self):
        pass

    def draw(self, dest):
        draw.rect(dest, Color('#FF0000'), self.rect)

    def lclick_event(self, e):
        pass

    def rclick_event(self, e):
        pass

    def mouse_hover_event(self, e):
        pass

    def mouse_unhover_event(self, e):
        pass


class PreButton:
    def __init__(self, callback=None, label_image=None, autorelease=True, description=""):
        self.callback = callback
        self.image = label_image
        self.autorelease = autorelease
        self.description = description


class Button(BaseWidget):
    def __init__(self, level, x, y, w, h, callback=None, label_image=None, autorelease=True, description=""):
        BaseWidget.__init__(self, level, x, y, w, h, description)

        self.bg_col = Color('#808080')
        self.bg_active_col = Color('#A0A0A0')
        self.hover_shade_col = Color('#FFFFFF')
        self.hover_shade_alpha = 0.1 * 255

        self.image = label_image
        self.callback = callback

        self.state = 0
        self.autorelease = autorelease

        self.anim_point = (0, 0)
        anim_time = 20
        self.anim = Anim(0, 0, anim_time)
        self.hovered = False

        self._hover_shade = Surface(self.rect.size)
        self._hover_shade.fill(self.hover_shade_col)
        self._hover_shade.set_alpha(self.hover_shade_alpha)

        self._s_template = Surface(self.rect.size)
        self._s_template.fill(self.bg_col)

    def lclick_event(self, event):
        game = self.level.game if hasattr(self.level, 'game') else self.level
        if game.context_help:
            game.show_tooltip(self.rect, self.description)
        else:
            point = event.pos
            if self.callback is not None:
                if num_args(self.callback) == 0:
                    self.callback.__call__()
                else:
                    self.callback.__call__(self)
            self.anim_point = (point[0] - self.rect.x, point[1] - self.rect.y)
            self.anim.end = int(max((distance(self.rect.topleft, point), distance(self.rect.topright, point),
                                     distance(self.rect.bottomleft, point), distance(self.rect.bottomright, point))) +
                                TILE_EDGE * 2)
            self.state = 1
            self.anim.enable()
            if self.autorelease:
                self.anim.frame_max_end = self.anim.disable

    def mouse_hover_event(self, event):
        self.hovered = True

    def mouse_unhover_event(self, event):
        self.hovered = False

    def update(self):
        self.anim.update()

    def draw(self, dest):
        if config.q > QMEDIUM:
            s = self._s_template.copy()
            if self.hovered:
                s.blit(self._hover_shade, (0, 0))
            if self.anim.dir == 1:
                draw.rect(s, self.bg_active_col,
                          Rect(self.anim_point[0]-self.anim.val, self.anim_point[1]-self.anim.val,
                               self.anim.val*2, self.anim.val*2))
            elif self.anim.dir == 2:
                s2 = Surface(self.rect.size)
                s2.fill(self.bg_active_col)
                s2.set_alpha(self.anim.val / self.anim.end * 255)
                s.blit(s2, (0, 0))
            if self.image is not None:
                s.blit(self.image, (0, 0))
            self.ext_draw(s)
            dest.blit(s, self.rect)
        else:
            dest.blit(self.image, self.rect)
            if self.hovered:
                dest.blit(self._hover_shade, self.rect)
            self.ext_lowdraw(dest)

    def ext_draw(self, s):
        pass

    def ext_lowdraw(self, dest):
        pass


class PreOptionSelect:
    def __init__(self, onchange=None, label_image=None, options=None, defval=None, description=""):
        self.onchange = onchange
        self.image = label_image
        self.options = options
        self.defval = defval
        self.description = description


class OptionSelect(Button):
    def __init__(self, level, x, y, w, h, onchange=None, label_image=None, options=None, defval=None, description=""):
        Button.__init__(self, level, x, y, w, h, onchange, label_image, True, description)

        if options is None:
            options = {None: level.graphic.ui_placeholder}
        self.option_keys = []
        self.option_graphics = []

        for k, v in options:
            self.option_keys.append(k)
            self.option_graphics.append(v)
        self.prev_option_index = len(self.option_keys) - 1
        self.active_option_index = self.option_keys.index(defval)

        self.scrollanim = Anim(0, self.rect.h, 10)

    def lclick_event(self, event):
        game = self.level.game if hasattr(self.level, 'game') else self.level
        if not game.context_help:
            self.prev_option_index = self.active_option_index

            if key.get_mods() & KMOD_SHIFT:
                self.active_option_index -= 1
                if self.active_option_index < 0:
                    self.active_option_index = len(self.option_keys) - 1
            else:
                self.active_option_index += 1
                self.active_option_index %= len(self.option_keys)

            self.scrollanim.frame = 0
            self.scrollanim.enable()

        Button.lclick_event(self, event)

    def update(self):
        Button.update(self)
        self.scrollanim.update()

    def ext_draw(self, s):
        so = self.option_graphics[self.active_option_index]
        s.blit(so, (self.rect.w - so.get_width(), self.scrollanim.val - self.rect.h))
        so = self.option_graphics[self.active_option_index]
        s.blit(so, (self.rect.w - so.get_width(), self.scrollanim.val))

    def ext_lowdraw(self, dest):
        so = self.option_graphics[self.active_option_index]
        dest.blit(so, (self.rect.right - so.get_width(), self.rect.y))

    @property
    def val(self):
        return self.option_keys[self.active_option_index]

    @val.setter
    def val(self, val):
        self.prev_option_index = self.active_option_index
        self.active_option_index = self.option_keys.index(val)


class PreLabel:
    def __init__(self, label_image=None):
        self.image = label_image


class Label(BaseWidget):
    def __init__(self, level, x, y, w, h, label_image=None):
        BaseWidget.__init__(self, level, x, y, w, h, "")
        self.bg_col = Color('#808080')

        self.image = label_image

    def update(self):
        pass

    def draw(self, dest):
        draw.rect(dest, self.bg_col, self.rect)
        if self.image is not None:
            dest.blit(self.image, self.rect)

    def rect_from_image(self):
        assert self.image is not None
        r = self.image.get_rect()
        self.rect.size = r.size


class Dialog:
    def __init__(self, level, item_w, item_h, num_items, shade=0.65, yoffset=0):
        self.level = level

        w, h = item_w, item_h * num_items
        x, y = SCREEN_SIZE[0]/2 - w/2, SCREEN_SIZE[1]/2 - h/2 + yoffset
        self.rect = Rect(x, y, w, h)

        self.widgets = []

        self.surface = filled_rect(None, MAP_SIZE)
        self.frame = Surface((w+TILE_EDGE*2, h+TILE_EDGE*2))
        self.frame.fill(Color('#808080'))
        self.frame_pos = (SCREEN_SIZE[0]/2 - self.frame.get_width()/2, SCREEN_SIZE[1]/2 - self.frame.get_height()/2 +
                          yoffset)
        self.shadeb = bool(shade)
        if shade:
            self.shade = filled_rect('#000000', MAP_SIZE)
            self.shade_level = shade
        self.anim = Anim(-SCREEN_SIZE[1], 0, 20)
        self.active = False

    def update(self):
        self.anim.update()
        for b in self.widgets:
            b.update()

        if config.q <= QMEDIUM:
            if self.active:
                self.anim.frame = self.anim.time
            else:
                self.anim.frame = 0

    def draw(self, dest):
        if self.anim.val > -SCREEN_SIZE[1]:
            if self.shadeb:
                if config.q > QMEDIUM:
                    self.shade.set_alpha(self.anim.frame / self.anim.time * 255 * self.shade_level)
                    dest.blit(self.shade, (0, 0))
                else:
                    dest.fill((83, 83, 83))

            if self.anim.val != 0:
                d = self.surface
            else:
                d = dest

            d.blit(self.frame, self.frame_pos)
            for b in self.widgets:
                b.draw(d)
            if d is self.surface:
                dest.blit(d, (0, self.anim.val))

    def lclick_event(self, event):
        for b in self.widgets:
            if b.rect.collidepoint(*event.pos):
                b.lclick_event(event)

    def mouse_hover_event(self, event):
        for b in self.widgets:
            b.mouse_unhover_event(event)
            if b.rect.collidepoint(*event.pos):
                b.mouse_hover_event(event)

    def mouse_unhover_event(self, event):
        for b in self.widgets:
            b.mouse_unhover_event(event)

    def enable(self):
        self.active = True
        self.anim.enable()

    def disable(self):
        self.active = False
        self.anim.disable()

    def toggle(self):
        if self.active:
            self.disable()
        else:
            self.enable()


class DialogW(Dialog):
    def __init__(self, level, prewidgets, w=TILE_EDGE*24, shade=0.65, yoffset=0):
        item_w, item_h = w, TILE_EDGE * 4
        Dialog.__init__(self, level, item_w, item_h, len(prewidgets), shade, yoffset)

        self.widgets = [Button(level, self.rect.x, self.rect.y + item_h*i, item_w, item_h, o.callback,
                               o.image, o.autorelease, o.description) if isinstance(o, PreButton) else
                        OptionSelect(level, self.rect.x, self.rect.y + item_h*i, item_w, item_h,
                                     o.onchange, o.image, o.options, o.defval, o.description) if isinstance(
                            o, PreOptionSelect) else
                        Label(level, self.rect.x, self.rect.y + item_h*i, item_w, item_h,
                              o.image) if isinstance(o, PreLabel) else
                        ValueError
                        for i, o in enumerate(prewidgets)]
