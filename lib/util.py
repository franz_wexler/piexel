from pygame import transform, font, key, mouse
import random
from math import atan2, degrees, cos, sin, ceil, sqrt
from .const import DISPLAY_MESSAGES, TILE_EDGE
from .log import log, pass_
from .winkey import key_press, VK_CONTROL, VK_ALT, VK_PRINTSCREEN

from platform import python_implementation, system, version
from sys import version_info

# is python implementation CPython
CPYTHON = python_implementation() == 'CPython'

# is game running on Microsoft Windows
WINDOWS = system() == 'Windows'

# is game running on Microsoft Windows 10
WINDOWS10 = WINDOWS and version()[:2] == '10'

# is python version 3 or above
PYTHON3 = version_info >= (3,)

# is python version 3.5 or above
PYTHON35 = version_info >= (3, 5)


def tiledv(val):
    return val / 16 * TILE_EDGE


def exps(val):
    return 1 if val else -1


def distance(point1, point2):
    return sqrt((abs(point1[0] - point2[0]) ** 2) + (abs(point1[1] - point2[1]) ** 2))


def toval(val, step, valto=0):
    if val > valto and val - step > valto:
        return val - step
    elif val < valto and val + step < valto:
        return val + step
    else:
        return valto


def expt(val, case_zero='random'):
    if val > 0:
        return 1
    elif val < 0:
        return -1
    else:  # val == 0
        if case_zero == 'random':
            return randd()
        else:
            return case_zero


def randd(include_zero=False):
    return random.choice((-1, 0, 1) if include_zero else (-1, 1))


def randrange_float(start, stop, step):
    return random.randint(0, int((stop - start) / step)) * step + start


def address_str(obj, place_atsign=True):
    obj_address = id(obj) if CPYTHON else random.randint(1, 268435455)
    return ("@" if place_atsign else "") + hex(obj_address).upper().replace("X", "x")


def extval(src, dkey, default):
    return src[dkey] if dkey in src else default


def blit_rotated(dest, surface, pos, angle):
    """blit_rotated(destination_surface, surface, center_pos: (x, y), angle_in_degrees)

    You can use Rect.topleft to get position
    """
    rotated = transform.rotate(surface, angle)
    new_size = rotated.get_rect().size
    new_pos = (pos[0] - new_size[0]/2, pos[1] - new_size[1]/2)
    dest.blit(rotated, new_pos)


def angle_towards(source_point, target_point, unit='deg'):
    """angle_towards(source: (x, y), target: (x, y), unit='deg') --> angle

    Returns angle for source point to point towards target
    unit should be 'deg' or 'rad' ('rad' is a bit faster)"""
    angle = atan2(target_point[1] - source_point[1], target_point[0] - source_point[0])
    if unit == 'rad':
        return angle
    elif unit == 'deg':
        return degrees(angle)
    else:
        raise ValueError("Invalid unit: {}, should be 'rad' or 'deg'".format(repr(unit)))


def speed_at_angle(angle, linear_speed):
    return linear_speed * cos(angle), linear_speed * sin(angle)


def speed_towards(source_point, target_point, linear_speed):
    return speed_at_angle(angle_towards(source_point, target_point), linear_speed)


def render_debug(dest, pos, text):
    ffont = font.Font(font.match_font('Courier'), 16)
    dest.blit(ffont.render(text, True, (0, 0, 0)), pos)


def state_str(bool_, type_="true/false", sep="/"):
    type_ = type_.split(sep, 1)
    return type_[0] if bool_ else type_[1]


def take_snapshot():
    if key.get_focused() and mouse.get_focused():
        key_press(VK_CONTROL, VK_ALT, VK_PRINTSCREEN)  # take screenshot of current window to clipboard
        if DISPLAY_MESSAGES:
            print("[INFO] Screenshot of Piexel is copied to clipboard")
        elif DISPLAY_MESSAGES:
            print("[INFO] No focus on window, screenshot rejected")


def reduce_rects(obj_list):
    for i in range(2):  # repeat two times to make sure both axis has been checked
        for b in obj_list:
            reduce_rect_with_others(b, obj_list)


def reduce_rect_with_others(obj, obj_list):
    for b in obj_list.copy():
        if (b.rect.top == obj.rect.top and b.rect.bottom == obj.rect.bottom and
                (b.rect.right == obj.rect.left or b.rect.left == obj.rect.right) or
                b.rect.left == obj.rect.left and b.rect.right == obj.rect.right and
                (b.rect.bottom == obj.rect.top or b.rect.top == obj.rect.bottom)):
            if not (hasattr(obj, 'reduce_condition') and not obj.reduce_condition(b)):
                obj.rect = obj.rect.union(b)
                obj_list.remove(b)
                if hasattr(b, 'kill'):  # b is an entity and must be removed
                    b.kill()

                if hasattr(obj, 'reduce_update'):
                    obj.reduce_update()


def line_collideline(begin1, end1, begin2, end2, return_='bool'):
    x1, y1 = begin1
    x2, y2 = end1
    x3, y3 = begin2
    x4, y4 = end2
    a1, b1 = y2 - y1, x1 - x2
    c1 = (x2 * y1) - (x1 * y2)
    r3 = ((a1 * x3) + (b1 * y3) + c1)
    r4 = ((a1 * x4) + (b1 * y4) + c1)
    if r3 != 0 and r4 != 0 and r3*r4 >= 0:
        return False  # don't intersect
    a2, b2 = y4 - y3, x3 - x4
    c2 = (x4 * y3) - (x3 * y4)
    r1 = (a2 * x1) + (b2 * y1) + c2
    r2 = (a2 * x2) + (b2 * y2) + c2
    if r1 != 0 and r2 != 0 and r1*r2 >= 0:
        return False  # don't intersect

    if return_ == 'bool':
        return True  # do intersect
    elif return_ == 'pos':
        denom = (a1 * b2) - (a2 * b1)
        offset = -denom/2 if denom < 0 else denom/2
        num = (b1 * c2) - (b2 * c1)
        x = (num - offset) / denom if num < 0 else (num + offset) / denom
        num = (a2 * c1) - (a1 * c2)
        y = (num - offset) / denom if num < 0 else (num + offset) / denom
        return x, y


def rect_collideline(rect, beginpoint, endpoint):
    return (line_collideline(beginpoint, endpoint, rect.topleft, rect.topright) or
            line_collideline(beginpoint, endpoint, rect.topright, rect.bottomright) or
            line_collideline(beginpoint, endpoint, rect.bottomright, rect.bottomleft) or
            line_collideline(beginpoint, endpoint, rect.bottomleft, rect.topleft))


def rect_list_line_collision(rectlist, begin, end):
    for rect in rectlist:
        if rect_collideline(rect.rect, begin, end):
            return True
    return False
