from pygame import Rect
from itertools import chain
from glob import glob
from string import ascii_lowercase
from os import path, remove
from .const import TILE_EDGE, SCREEN_SIZE, LANG
from .locale import l
from .ui import DialogW, PreButton, PreOptionSelect, PreLabel
from .log import log, pass_
from .config import config, QMEDIUM
from .graphic import get_logo, ql
from .scout import Save
from .cursors import set_loading
from .easeanim import ease_out_bounce
from .credits import CreditsMenu


class Menu:
    def __init__(self, game):
        self.game = game
        self.graphic = game.graphic

        self.error_box = None

    def update(self):
        if self.error_box is not None:
            self.error_box.update()

        if self.error_box is not None:
            self.error_box.draw(self.game.screen)

    def get_ui(self):
        if self.error_box is not None and self.error_box.anim.frame == self.error_box.anim.time:
            return self.error_box
        else:
            return NotImplemented

    def click_event(self, event):
        i = self.get_ui()
        if event.button == 1 and i.rect.collidepoint(*event.pos):
            i.lclick_event(event)

    def mouse_motion_event(self, event):
        i = self.get_ui()
        if i.rect.collidepoint(*event.pos):
            i.mouse_hover_event(event)
        else:
            i.mouse_unhover_event(event)

    def dismiss_error(self):
        self.error_box.disable()


class MainMenu(Menu):
    def __init__(self, game):
        Menu.__init__(self, game)

        self.settings_menu = settings_menu(self)
        self.ui = main_menu_menu(self)

        self.saveslots = [i[i.rfind('save') + 4:i.rfind('.ini')] for i in glob('./save/save*.ini')]
        self.saveslot_select = saveslot_select(self, self.saveslots)
        self.new_save_select = new_save_select(self)
        self.deleteslot = deleteslot_select(self, self.saveslots)

        self.logo_surf = get_logo(self.graphic.bg_color)
        self.logo_rect = Rect(SCREEN_SIZE[0] / 2 - self.logo_surf.get_width() / 2, TILE_EDGE * 2.2,
                              self.logo_surf.get_width(), self.logo_surf.get_height())

    def update(self):
        self.game.screen.fill((83, 83, 83))

        self.ui.update()
        self.settings_menu.update()
        self.saveslot_select.update()
        self.new_save_select.update()
        self.deleteslot.update()

        self.ui.draw(self.game.screen)
        self.game.screen.blit(self.logo_surf, self.logo_rect)
        self.settings_menu.draw(self.game.screen)

        self.saveslot_select.shadeb = config.q > QMEDIUM
        self.saveslot_select.draw(self.game.screen)
        self.new_save_select.draw(self.game.screen)
        self.deleteslot.draw(self.game.screen)

        Menu.update(self)

    def get_ui(self):
        r = Menu.get_ui(self)
        if r is not NotImplemented:
            return r
        elif self.settings_menu.active:
            return self.settings_menu
        elif self.saveslot_select.active:
            if self.new_save_select.active:
                return self.new_save_select
            elif self.deleteslot.active:
                return self.deleteslot
            else:
                return self.saveslot_select
        else:
            return self.ui

    def play_game(self, id_):
        self.game.menu = None

        self.game.save = Save("./save/save{}.ini".format(id_))
        self.game.save.apply(self.game)

        config.active_game = id_

    def create_save(self, gamemode):
        c = None
        for ch in ascii_lowercase:
            if ch not in self.saveslots:
                c = ch
                break
        self.game.save = Save("./save/save{}.ini".format(c))
        self.game.save.gamemode = gamemode
        self.game.save.apply(self.game)
        self.game.save.save()

        self.saveslots.append(c)

        self.reload_saveslots()

        self.play_game(c)

    def delete_save(self, id_):
        filepath = "./save/save{}.ini".format(id_)
        if path.isfile(filepath):
            remove(filepath)
        else:
            log("[ERR ] No file {} to remove".format(repr(filepath)))

        self.saveslots.remove(id_)

        self.reload_saveslots()

    def reload_saveslots(self):
        a, b = self.saveslot_select.anim, self.saveslot_select.active
        self.saveslot_select = saveslot_select(self, self.saveslots)
        self.saveslot_select.anim, self.saveslot_select.active = a, b

        a, b = self.deleteslot.anim, self.deleteslot.active
        self.deleteslot = deleteslot_select(self, self.saveslots)
        self.deleteslot.anim, self.deleteslot.active = a, b


def main_menu_menu(level):
    g = level.graphic

    def show_credits():
        level.game.menu = CreditsMenu(level.game)

    h = DialogW(level, (PreButton(lambda: level.saveslot_select.enable(), g.play_buttonl, description="play"),
                        PreButton(level.settings_menu.toggle, g.settings_buttonl, description="settings"),
                        PreButton(show_credits, g.credits_buttonl, description="credits"),
                        PreButton(level.game.exit, g.exit_buttonl, description="exit")),
                shade=False, yoffset=TILE_EDGE * 5)
    h.enable()
    h.anim.frame = h.anim.time
    return h


def saveslot_select(level, saveslots):
    g = level.graphic

    def p_continue(id_):
        return lambda: level.play_game(id_)

    def p_delete():
        if len(level.saveslots) == 0:
            level.error_box = nogamestodelete_error(level)
        else:
            level.new_save_select.disable()
            level.deleteslot.enable()

    def p_create():
        if len(level.saveslots) < 5:
            level.new_save_select.enable()
        else:
            level.error_box = toomanygames_error(level)

    return DialogW(level, [PreButton(p_continue(i), ql(g.bg_color, " " + l("continue game") + " " + str(i)),
                                     description="continueagame") for i in saveslots] +
                   [PreButton(p_create, g.new_save_buttonl, description="newgame"),
                    PreButton(p_delete, g.deletelevell if len(level.saveslots) > 0 else g.deletelevell_d,
                              description="deletegame"),
                    PreButton(lambda: level.saveslot_select.disable(), g.backl, description="saveslotback")],
                   TILE_EDGE * 48)


def deleteslot_select(level, saveslots):
    g = level.graphic

    def p_delete(id_):
        return lambda: (level.delete_save(id_), level.deleteslot.disable())

    return DialogW(level, [PreButton(p_delete(i), ql(g.bg_color, " " + l("delete game") + " " + str(i)),
                                     description="deleteagame") for i in saveslots] +
                   [PreButton(lambda: level.deleteslot.disable(), g.cancel_buttonl, description="deleteslotback")],
                   TILE_EDGE * 50)


def new_save_select(level):
    g = level.graphic

    def p_create_save(gamemode):
        def _p_create_save():
            level.create_save(gamemode)
            level.new_save_select.disable()

        return _p_create_save

    def wdeath():
        if config.death1mode_unlocked:
            p_create_save('1death')()
        else:
            level.error_box = death1notavailable(level)

    return DialogW(level, (PreLabel(g.choosegamemodel),
                           PreButton(p_create_save('normal'), g.normall, description="modenormal"),
                           PreButton(wdeath, g.death1l if config.death1mode_unlocked else g.death1l_d,
                                     description="mode1death"),
                           # PreButton(p_create_save('vvvvvv'), g.vvvvvvl, description="modevvvvvv"),
                           PreButton(lambda: level.new_save_select.disable(), g.cancel_buttonl,
                                     description="newsaveback")),
                   w=TILE_EDGE * 36)


def pause_menu(level):
    def reload_level():
        nonlocal level

        level.game.load_current_level()
        level = level.game.level  # now level is different level

        # create illusion that menu is not bound to level
        level.pause_menu.anim.frame = level.pause_menu.anim.time
        level.pause_menu.disable()

    g = level.graphic
    return DialogW(level, (PreButton(level.back_to_menu, g.backtomenu_buttonl, description="backtomainmenu"),
                           PreButton(level.settings_menu.toggle, g.settings_buttonl, description="settings"),
                           PreButton(level.autosolve, g.walkthrough_buttonl, description="walkthrough"),
                           PreButton(reload_level, g.restartlevel_buttonl, description="restartlevel"),
                           PreButton(level.unpause, g.resume_buttonl, description="unpause")), w=TILE_EDGE * 28)


def settings_menu(level):
    g = level.graphic

    music, tile_edge, quality, fullscreen, control_style, lang = (config.music, config.tile_edge, config.quality,
                                                                  config.fullscreen, config.control_style, config.lang)

    def set_sound(w):
        nonlocal music
        music = w.val

    def set_resolution(w):
        nonlocal tile_edge
        tile_edge = w.val

    def set_quality(w):
        nonlocal quality
        quality = w.val

    def set_fullscreen(w):
        nonlocal fullscreen
        fullscreen = w.val

    def set_controls_style(w):
        nonlocal control_style
        control_style = w.val

    def set_lang(w):
        nonlocal lang
        lang = w.val

    def save_changes():
        set_loading()

        (config.music, config.tile_edge, config.quality, config.fullscreen, config.control_style,
         config.lang) = music, tile_edge, quality, fullscreen, control_style, lang
        config.save()
        config.apply(level)
        level.settings_menu.disable()

    return DialogW(level, (PreOptionSelect(set_sound, g.soundl, ((True, g.onl), (False, g.offl)), config.music,
                                           "sound"),
                           PreOptionSelect(set_quality, g.qualityl, (('low', g.lowl), ('medium', g.mediuml),
                                                                     ('high', g.highl)),
                                           config.quality, "quality"),
                           PreOptionSelect(set_resolution, g.resolutionl, ((8, g.x512x288), (16, g.x1024x576),
                                                                           (20, g.x1280x720), (24, g.x1536x864),
                                                                           (30, g.x1920x1080)),
                                           config.tile_edge, "resolution"),
                           PreOptionSelect(set_fullscreen, g.fullscreenl, ((True, g.onl), (False, g.offl)),
                                           config.fullscreen, "fullscreen"),
                           PreOptionSelect(set_controls_style, g.controlsl, (('wasd', g.wasdl), ('arrows', g.arrowsl)),
                                           config.control_style, "controlstyle"),
                           PreOptionSelect(set_lang, g.languagel, (('en', g.enl), ('pl', g.pll)), config.lang, "lang"),
                           PreButton(save_changes, g.savel, description="savechanges")), TILE_EDGE * 36)


def game_complete_box(level):
    g = level.graphic

    if level.game.save is not None:
        d = level.game.save.deaths
        t_ = round(level.game.save.frames / (60 * 60))
    else:
        d = t_ = 0

    def watch_credits():
        level.game.menu = CreditsMenu(level.game)

    return DialogW(level, (PreLabel(g.youcompletedgamel),
                           PreLabel(ql(g.bg_color,
                                       " " + str(d) + ((" śmierć" if d == 1 else " śmierci") if LANG == 'pl' else
                                                       (" death" if d == 1 else " deaths")))),
                           PreLabel(ql(g.bg_color,
                                       " " + str(t_) + ((" minuta gry" if t_ == 1 else
                                                        (" minuty gry" if t_ % 10 in (2, 3, 4) else
                                                         " minut gry")) if LANG == 'pl' else
                                                        (" minute of playing" if t_ == 1 else " minutes of playing")))),
                           PreButton(watch_credits, g.okl, description="gamecompleteok")), w=TILE_EDGE*42)


def errorbox(level, *msgs):
    h = DialogW(level, list(chain((PreLabel(msg) for msg in msgs), (PreButton(level.dismiss_error,
                                                                              level.graphic.okl,
                                                                              description="errorok"),))),
                TILE_EDGE * 56)
    h.enable()
    return h


def fullscreen_error(level):
    g = level.graphic
    return errorbox(level, g.errorl, g.toobigresolutionforfullscreenl)


def toomanygames_error(level):
    g = level.graphic
    return errorbox(level, g.errorl, g.cannotcreatemoregamesl)


def nogamestodelete_error(level):
    g = level.graphic
    return errorbox(level, g.errorl, g.nogamestodeletel)


def death1notavailable(level):
    g = level.graphic
    return errorbox(level, g.errorl, g.death1modenotunlockedl, g.death1modenotunlockedl2)


def death1lose_menu(level):
    g = level.graphic
    d = DialogW(level, (PreLabel(g.youlosel),
                        PreButton(lambda: MainMenu(level.game).create_save(level.game.save.gamemode),
                                  g.retry_buttonl, description="retry1death"),
                        PreButton(level.back_to_menu, g.backtomenu_buttonl,
                                  description="backtomainmenu1death")),
                w=TILE_EDGE * 48, shade=0.85)
    d.anim.func = ease_out_bounce
    d.anim.time = 160
    return d
