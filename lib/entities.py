from itertools import chain
from pygame import sprite, Rect
from .graphic import draw_tile_pattern
from .util import extval, tiledv, toval
from .storylib import Balloon
from .pydiff import infinity


class Entity(sprite.Sprite):
    __slots__ = ['level', 'image', 'rect']

    def __init__(self, level, graphic, rect):
        """Entity(level, graphic, rect) -> Entity"""
        sprite.Sprite.__init__(self)
        self.level = level
        self.image, self.rect = graphic, (Rect(rect) if rect is not None else None)

    def draw(self, dest):
        dest.blit(self.image, self.rect)

    def update(self, *args):
        pass


def draw_patternized(self, dest):
    draw_tile_pattern(dest, self.image, self.rect)


class EntityGroup(sprite.Group):
    def draw(self, dest, quickdraw=False):
        if quickdraw:
            sprite.Group.draw(self, dest)
        else:
            for s in self.sprites():
                s.draw(dest)


class GravityDynamic(Entity):
    __slots__ = ['f_x', 'f_y', 'jump_height', 'moving_speed', 'max_falling_speed', 'gravity', 'air_friction', 'b_xvel',
                 'b_yvel', 'p_xvel', 'p_yvel', 'g_up', 'g_down', 'g_left', 'g_right', 't_up', 't_down', 't_left',
                 't_right', 'allow_movement', 'jumping_style']

    def __init__(self, level, graphic, x, y, w, h, **kw):
        """GravityDynamic(level, graphic, posx, posy, width, height, **kw):

        Available **kw options:
          jump_height: number (initial px/frame) -- Initial x speed of entity on jump
          moving_speed: number (px/frame) -- Speed of walking
          max_falling_speed: number (px/frame) -- Maximal speed of moving, should not be greater than tile size
          gravity: number (px/frame) -- Speed of accelerating entity with gravity
          air_friction: number (px/frame) -- Force of slowing entity down
        """
        Entity.__init__(self, level, graphic, (x, y, w, h))
        self.f_x, self.f_y = x, y  # x and y stored as float
        self.jump_height = tiledv(extval(kw, 'jump_height', 0))
        self.moving_speed = tiledv(extval(kw, 'moving_speed', 0))
        self.max_falling_speed = tiledv(extval(kw, 'max_falling_speed', 15))
        self.gravity = tiledv(extval(kw, 'gravity', 0.3))
        self.air_friction = tiledv(extval(kw, 'air_friction', 0.5))
        self.b_xvel = self.b_yvel = 0  # base xvel and yvel, excluding self-movement
        self.p_xvel = self.p_yvel = 0  # movement from moving platforms

        # is going up (jumping), down (platform slippering), left, right
        # would be very useful for creating some NPCs but there are none
        self.g_up = self.g_down = self.g_left = self.g_right = False

        # is touching something solid up, down, left, right
        # True means that does touch but False doesn't mean that it isn't
        self.t_up = self.t_down = self.t_left = self.t_right = False

        self.jumpsave_frames = 0  # after leaving ground player after short time can still jump
        self.jumpsave_time = extval(kw, 'jumpsave_time', 4)

        self.allow_movement = True  # If False, self-movement will be disabled
        self.jumping_style = 0  # 0 for normal, 1 for VVVVVV-like

        self.nomove_frames = 0  # initial nomove_frames for player is applied by entrance: see ./io.py

    @property
    def xvel(self):
        vel = self.b_xvel + self.p_xvel
        if self.allow_movement:
            if self.g_left:
                vel -= self.moving_speed
            elif self.g_right:
                vel += self.moving_speed
        return vel

    @property
    def yvel(self):
        return self.b_yvel + self.p_yvel

    def reset_g(self):
        self.g_up = self.g_down = self.g_left = self.g_right = False
        self.t_up = self.t_down = self.t_left = self.t_right = False

    def update(self):
        if self.nomove_frames > 0:
            self.allow_movement = False
            self.nomove_frames -= 1
        else:
            self.allow_movement = True

        self.b_xvel = toval(self.b_xvel, self.air_friction)

        # SOME code (NOT ALL) for y axis MUST be executed before x axis
        if not self.t_down:
            self.b_yvel += self.gravity  # gravity acceleration
            if self.b_yvel > self.max_falling_speed:
                self.b_yvel = self.max_falling_speed  # prevent falling too fast

        if self.g_up and self.allow_movement:  # wants to jump
            if ((self.t_down and self.gravity > 0) or (self.t_up and self.gravity < 0) or
                    self.jumpsave_frames > 0):  # can jump
                self.jumpsave_frames = 0

                if self is self.level.player:  # we're famous!
                    self.level.game.save.jumps += 1

                if self.jumping_style == 0:
                    if (self.gravity > 0 and 0 < self.b_yvel < 1) or (self.gravity < 0 and 0 > self.b_yvel > -1):
                        self.b_yvel = 0  # fix for some rounding problems

                    self.b_yvel -= self.jump_height  # jump
                elif self.jumping_style == 1:
                    self.reverse_gravity()

        self.f_x += self.xvel  # moving object (axis x)
        self.rect.x = self.f_x

        if self.xvel != 0:
            self.t_left = self.t_right = False
            for b in chain(self.level.blocks, self.level.movers, self.level.moving_blocks, self.level.balloons):
                if b.rect.colliderect(self.rect):
                    if self.xvel > 0:
                        self.rect.right = b.rect.left
                        self.t_right = True
                        self.b_xvel = 0
                    elif self.xvel < 0:
                        self.rect.left = b.rect.right
                        self.t_left = True
                        self.b_xvel = 0
                    self.f_x = self.rect.left
                    break
        if not self.g_up:  # do not move by few pixels if GravityDynamic is jumping
            for p in chain(self.level.moving_platforms, self.level.movers, self.level.moving_blocks):
                if p.outer_rect.colliderect(self.rect) and ((self.rect.bottom <= p.rect.top and self.gravity > 0) or
                                                            (self.rect.top >= p.rect.bottom and self.gravity < 0)):
                    self.p_xvel = p.xvel
                    break
            else:
                self.p_xvel = 0
        else:
            self.p_xvel = 0

        if self is self.level.player and self.xvel != 0:
            self.level.game.save.stepframes += 1

        self.f_y += self.yvel  # moving object (axis y)
        self.rect.y = self.f_y

        self.t_up = self.t_down = False
        for b in chain(self.level.blocks, self.level.moving_blocks, self.level.balloons):
            if b.rect.colliderect(self.rect):
                if self.yvel > 0:
                    self.rect.bottom = b.rect.top
                    self.t_down = True
                    self.b_yvel = 0
                    if hasattr(b, 'on_step'):
                        b.on_step(self)
                elif self.yvel < 0:
                    self.rect.top = b.rect.bottom
                    self.t_up = True
                    self.b_yvel = 0
                    if hasattr(b, 'on_step'):
                        b.on_step(self)
                self.f_y = self.rect.top
                break
        for p in chain(self.level.platforms, self.level.collapsing_platforms, self.level.moving_platforms,
                       self.level.movin_platforms):
            if p.rect.inflate(0, 1).colliderect(self.rect):
                if self.gravity > 0 and self.yvel > 0:
                    if self.rect.bottom - self.yvel * 2 <= p.rect.top:
                        if not self.g_down:
                            self.rect.bottom = p.rect.top
                            self.f_y = self.rect.top
                            self.t_down = True
                        if hasattr(p, 'on_step'):
                            p.on_step(self)
                        self.b_yvel = 0
                elif self.gravity < 0 and self.yvel < 0:
                    if self.rect.top - self.yvel * 2 > p.rect.bottom:
                        if not self.g_down:
                            self.rect.top = p.rect.bottom + 1
                            self.f_y = self.rect.top
                            self.t_up = True
                        if hasattr(p, 'on_step'):
                            p.on_step(self)
                        self.b_yvel = 0
                break
        if (self.t_down and self.gravity > 0) or (self.t_up and self.gravity < 0):  # is on ground
            self.jumpsave_frames = self.jumpsave_time
        elif self.jumpsave_frames > 0:
            self.jumpsave_frames -= 1

    def disable_movement(self):
        self.nomove_frames = infinity

    def enable_movement(self):
        self.nomove_frames = 0

    def rect_to_f(self):
        self.f_x, self.f_y = self.rect.topleft

    def f_to_rect(self):
        self.rect.x, self.rect.y = self.f_x, self.f_y

    def reverse_gravity(self):
        self.gravity = -self.gravity
        self.jump_height = -self.jump_height

    def reset_gravity(self):
        self.gravity = abs(self.gravity)
        self.jump_height = abs(self.jump_height)

    def schedule_motion(self, motion):
        """schedule_motion(motion: [(time, {attr: val, ...}), ...]"""

        def schedule_motion_auto(values_):
            def _t():
                for key, val in values_.items():
                    key = {'g_up': 'g_up', 'u': 'g_up', 'g_down': 'g_down', 'd': 'g_down',
                           'g_left': 'g_left', 'l': 'g_left', 'g_right': 'g_right', 'r': 'g_right'}[key]
                    setattr(self, key, val)

            return _t

        for time, values in motion:
            self.level.schedule_event(time, schedule_motion_auto(values))

    def say(self, *texts, final_dismiss_callback=None, bind_movement=True, _i_start=0):
        if self.level.game.save is not None and self.level.game.save.gamemode == '1death':
            return

        canmove = self.nomove_frames != infinity
        self.disable_movement()

        def p_adddialog(txt):
            def _t():
                self.level.balloons.append(Balloon(self.level, self, txt))

            return _t

        for i, t in enumerate(texts):
            self.level.schedule_event(_i_start + i + 1, p_adddialog(t), "storyline<{}>".format(repr(t)))

        if final_dismiss_callback is not None:
            self.level.schedule_event(_i_start + len(texts) + 1, final_dismiss_callback)
        if canmove and bind_movement:
            self.level.schedule_event(_i_start + len(texts) + 1, self.enable_movement)
