from pygame import draw, Surface, Rect, Color
from .graphic import get_textbaloon, rendertext
from .easeanim import Anim, Anim2D, ease_out_back
from .const import SCREEN_SIZE, TILE_EDGE
from .locale import m, l


class Balloon:
    def __init__(self, level, ent, text, dismiss_callback=None, anim_time=20):
        self.bgcol = Color('#F2F2F2')
        self.bordercol = Color('#CCCCCC')
        self.textcol = Color('#404040')

        self.level = level

        self.surf = get_textbaloon(text + "\n", self.textcol, self.bgcol)
        self.presskeysurf = rendertext(l("Press any key or click to continue"), 1, Color('#909090'), self.bgcol)

        self.orientation = 1 if ent.rect.x > SCREEN_SIZE[0] / 2 else 0  # 0 if to the right, 1 if to the left

        self.rect = Rect(self.surf.get_rect())
        if self.orientation == 0:
            self.rect.x = ent.rect.right + TILE_EDGE * 2
        else:
            self.rect.right = ent.rect.left - TILE_EDGE * 2
        self.rect.bottom = ent.rect.top - TILE_EDGE

        if self.orientation == 0:
            self.point = (ent.rect.right + TILE_EDGE, ent.rect.top + TILE_EDGE // 4)
        else:
            self.point = (ent.rect.left - TILE_EDGE, ent.rect.top + TILE_EDGE // 4)

        # balloon body animation
        self.anim = Anim2D((0, self.rect.width / 2), (0, self.rect.height / 2), anim_time, ease_out_back)
        self.anim.enable()

        # balloon tail animation
        self.anim2 = Anim(0, TILE_EDGE, anim_time, ease_out_back)
        self.anim2.enable()

        # pressanykeytocontinue-text blinking animation
        self.anim3 = Anim(0, 255, 60)
        self.anim3.frame_max_end = lambda: (self.anim3.disable(), setattr(self.anim3, 'begin', 100),
                                            setattr(self.anim3, 'frame_max_end', self.anim3.disable),
                                            setattr(self.anim3, 'time', 40),
                                            setattr(self.anim3, 'frame', self.anim3.time))
        self.anim3.frame_0_end = self.anim3.enable
        self.anim3.enable()

        self.dismissed = False
        self.dismiss_callback = dismiss_callback

    def update(self):
        self.anim.update()
        self.anim2.update()
        self.anim3.update()

    def draw(self, dest):
        w, h = self.anim.vals
        x, y = self.rect.centerx - w, self.rect.centery - h
        w *= 2
        h *= 2
        s = Surface((w, h))
        s.fill(self.bgcol)
        w2, h2 = self.surf.get_size()

        # draw body
        s.blit(self.surf, (w / 2 - w2 / 2, h / 2 - h2 / 2))

        self.presskeysurf.set_alpha(int(self.anim3.val))
        s.blit(self.presskeysurf,
               (w / 2 - w2 / 2 + TILE_EDGE, h / 2 + h2 / 2 - self.presskeysurf.get_height() - TILE_EDGE / 2))

        # draw border
        draw.rect(s, self.bordercol, Rect(0, 0, w - TILE_EDGE // 16, h - TILE_EDGE // 16), TILE_EDGE // 8)

        dest.blit(s, (x, y))

        if self.orientation == 0:
            p1 = (x + TILE_EDGE, y + h - TILE_EDGE // 8)
            p2 = (x + TILE_EDGE + self.anim2.val, y + h - TILE_EDGE // 8)
        else:
            p1 = (x + w - TILE_EDGE, y + h - TILE_EDGE // 8)
            p2 = (x + w - TILE_EDGE - self.anim2.val, y + h - TILE_EDGE // 8)

        # draw tail
        if self.anim2.val > 4:  # don't draw tail if is too thin
            draw.polygon(dest, self.bgcol, (self.point, p1, p2), )
            draw.line(dest, self.bordercol, self.point, p1, TILE_EDGE // 8)
            draw.line(dest, self.bordercol, self.point, p2, TILE_EDGE // 8)

        if self.anim.frame == 0 and self.dismissed:
            self.kill()

    def dismiss(self):
        self.dismissed = True
        self.anim.disable()
        self.anim2.disable()

    def kill(self):
        self.level.balloons.remove(self)

        if self.dismiss_callback is not None:
            self.dismiss_callback()
