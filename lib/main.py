from pygame import display, draw, time, mixer, mouse, key, event, init, quit, error, Color
from pygame.constants import *
from os import system
from os.path import isfile
from glob import glob
from ctypes import windll
from math import sqrt
from collections import namedtuple
from .const import SCREEN_SIZE, MAP_SIZE, TILE_EDGE
from .config import config
from .easeanim import Anim2D
from .graphic import GeneratedGraphic
from .ui import RawTextOverlay
from .lvlmgr import Level, LevelPlaceholder
from .anim import MonitorBlur
from .util import address_str, state_str, log
from .goto import allow_goto, label, goto
from .macros import MacroRecorder
from .pydiff import InterruptedError
from .uit import MainMenu, CreditsMenu
from .cursors import set_cursor, set_loading
from .ui import Button
from .chelp import ToolTip
from sys import exit

class Game:
    def __init__(self):
        init()

        set_loading()

        self.screen = display.set_mode(SCREEN_SIZE)
        display.set_caption("Piexel")
        windll.shell32.SetCurrentProcessExplicitAppUserModelID('piexel-foundation.piexel.piexel-the-game.ver-0-0-1')
        self.timer = time.Clock()
        self.graphic = GeneratedGraphic()

        self.debug = RawTextOverlay()
        self.debug_enabled = config.dev_mode
        log("[INFO] Press / (K_SLASH) to show/hide debug panel")
        self.show_debug = False
        self.show_detailed_debug = False
        self.show_ruler = False
        self.show_death_messages = False
        self.take_death_pictures = False

        self.macro_recorder = None

        self.fps = 0
        self.target_fps = config.target_fps
        self.target_tick_updates = config.target_ups
        self.frame = 0

        self.break_mainloop = False

        self.level = None
        self.last_level = LevelPlaceholder()
        self.allow_level_passing = True

        self.menu = MainMenu(self)

        self.save = None

        self.anim = Anim2D((0, -SCREEN_SIZE[0]), (0, 0), 40)
        self.anim.frame_max_end = self.anim.reset

        self.level_dir = "./levels/release/"
        self.level_queue = [lvl.replace('\\', '/') for lvl in sorted(glob("{}/*.txt".format(self.level_dir)))]
        self.level_macros = [(lvlpath.replace('.txt', '.py') if isfile(lvlpath.replace('.txt', '.py')) else None)
                             for lvlpath in self.level_queue]
        default_def = "./levels/templates/sp_64x36_template.ini"
        self.level_defs = [(lvlpath.replace('.txt', '.ini') if isfile(lvlpath.replace('.txt', '.ini')) else default_def)
                           for lvlpath in self.level_queue]
        self.current_level_index = 0
        self.load_current_level()

        if config.open_paused:
            self.menu.play_game(config.active_game)

            self.level.pause()
            self.level.pause_menu.anim.frame = self.level.pause_menu.anim.time
            self.level.settings_menu.anim.frame = self.level.settings_menu.anim.time
            self.level.settings_menu.disable()
            config.open_paused = False
            config.save()

        config.apply(self.level)

        self.ui = [Button(self, 0, 0, TILE_EDGE * 2, TILE_EDGE * 2, self.toggle_context_help,
                          self.level.graphic.help_button, description="help")]
        self.context_help = False
        self.tooltip = None

        if config.music:
            self.load_soundtrack()

        log("[INFO] Game instance {} created".format(address_str(self)))

    @staticmethod
    def load_soundtrack():
        if mixer.get_init():
            if not mixer.music.get_busy():
                mixer.music.load([x for x in glob("./music/*.mid")][0])  # loads only first file
                mixer.music.play(-1)
                mixer.music.set_volume(0.6)

    def next_level(self):
        self.current_level_index += 1
        self.save.progress = self.current_level_index
        self.save.save()

        if self.current_level_index >= len(self.level_queue):  # no next level
            self.current_level_index = len(self.level_queue) - 1
            log("[INFO] No next level found")
            return False
        else:

            self.last_level = self.level

            self.anim.enable()

            self.save.currentleveldeaths = 0
            self.load_current_level()

            self.last_level.player.reset_g()

            if self.last_level.paused:
                self.level.paused = True
            return True

    def prev_level(self):
        # no animation for previous level is provided: use for debug only
        self.current_level_index -= 1
        self.save.progress = self.current_level_index
        self.save.save()
        if self.current_level_index < 0:
            self.current_level_index = 0
            log("[INFO] No previous level found")
            return False
        else:
            self.load_current_level()

            self.save.currentleveldeaths = 0
            return True

    def load_current_level(self):
        data = None
        if self.level is not None:
            data = self.level.player.pack_translevel()
        self.level = Level(self, self.level_queue[self.current_level_index], self.level_defs[self.current_level_index],
                           self.level_macros[self.current_level_index])
        if data is not None:
            self.level.player.unpack_translevel(data)
        if self.last_level is not None and self.last_level.solved_with_cheating:
            self.level.player.reset_g()

    @allow_goto
    def mainloop(self):
        while True:
            self.timer.tick(self.target_fps)
            self.fps = round(self.timer.get_fps())
            if self.context_help:
                set_cursor('help')
            else:
                set_cursor('arrow')
            for e in event.get():
                if e.type == QUIT:
                    self.exit()
                elif e.type == KEYDOWN or e.type == KEYUP:
                    if not self.level.solving:
                        key_state = (e.type == KEYDOWN)
                        if e.key == config.k_w:
                            self.level.player.g_up = key_state
                        elif e.key == config.k_s:
                            self.level.player.g_down = key_state
                        elif e.key == config.k_a:
                            self.level.player.g_left = key_state
                        elif e.key == config.k_d:
                            self.level.player.g_right = key_state
                    if e.type == KEYDOWN:
                        dismiss_balloon = True  # game prevents it only in most needed places
                        if self.debug_enabled:
                            if e.key == K_o:
                                self.level.player.godmode = not self.level.player.godmode  # toggle god mode
                            elif e.key == K_p:
                                self.level.player.noclipmode = not self.level.player.noclipmode  # toggle noclip mode
                            elif e.key == K_r:
                                self.load_current_level()  # reload level
                            elif e.key == K_LEFTBRACKET:
                                self.prev_level()
                            elif e.key == K_RIGHTBRACKET:
                                self.next_level()
                            elif e.key == K_i:
                                self.level.player.init_kill()
                            elif e.key == K_SLASH:
                                self.target_fps = config.target_fps if self.target_fps != config.target_fps else 0
                            elif e.key == K_0:
                                self.level.paused = not self.level.paused

                                dismiss_balloon = False
                            elif e.key == K_9:
                                self.level.paused = False
                                self.level.update()
                                self.level.paused = True

                                dismiss_balloon = False
                            elif e.key == K_e:
                                self.level.player.reverse_gravity()
                            elif e.key == K_BACKQUOTE:
                                self.show_detailed_debug = not self.show_detailed_debug
                            elif e.key == K_1:
                                self.show_ruler = not self.show_ruler
                            elif e.key == K_b:
                                if self.macro_recorder is None:
                                    self.macro_recorder = MacroRecorder(self)
                                else:
                                    self.macro_recorder = None
                            elif e.key == K_u:
                                self.allow_level_passing = not self.allow_level_passing
                            elif e.key == K_n:
                                if self.macro_recorder is not None:
                                    self.macro_recorder.play()
                                    self.macro_recorder = None
                            elif e.key == K_MINUS:
                                self.show_debug = not self.show_debug
                            elif e.key == K_l:
                                if self.level.overlay_effect is None:
                                    MonitorBlur(self.level)
                                else:
                                    self.level.overlay_effect.exit()
                                    self.level.overlay_effect = None

                        if e.key == K_BACKSLASH and config.dev_mode:
                            self.debug_enabled = not self.debug_enabled
                            if not self.debug_enabled:
                                # turn cheats off if disabled
                                self.level.player.godmode = False
                                self.level.player.noclipmode = False
                                self.show_detailed_debug = False
                                self.allow_level_passing = True
                                self.show_debug = False
                            else:
                                self.show_debug = True

                        elif self.level is not None and self.level.balloons and dismiss_balloon:
                            for i in self.level.balloons:
                                i.dismiss()
                        elif isinstance(self.menu, CreditsMenu):
                            self.menu.dismiss()

                    if self.tooltip is not None:
                        self.tooltip.dismiss()

                elif e.type == MOUSEBUTTONUP:
                    self.mouse_click_event(e)
                elif e.type == MOUSEMOTION:
                    self.mouse_motion_event(e)
            self.update()
            self.frame += 1
            if self.break_mainloop:
                log("[INFO] Restarting game (break_mainloop = True)")
                self.exit(False)
                system("py -3 ./game.py")

    def update(self):
        if self.menu is not None:
            self.menu.update()
        else:
            for _ in range(self.target_tick_updates):
                self.level.update()
            if self.show_detailed_debug:
                self.level.draw_debug_overlay()

            if self.last_level is not None:
                self.last_level.update()
                if self.show_detailed_debug:
                    self.last_level.draw_debug_overlay()

            self.anim.update()
            if self.level.draw_by_view:
                if self.last_level is not None and self.last_level.view is not None:
                    self.screen.blit(self.last_level.view, self.anim.vals)
                if self.level.view is not None:  # first frame of animation
                    self.screen.blit(self.level.view, (self.anim.x.val + SCREEN_SIZE[0], self.anim.y.val))
            else:
                self.last_level = None

            if self.macro_recorder is not None:
                self.macro_recorder.update()

        for i in self.ui:
            i.update()
            i.draw(self.screen)

        if self.tooltip is not None:
            self.tooltip.update()
            self.tooltip.draw(self.screen)

        if self.show_ruler:
            self.draw_ruler()

        if self.show_debug and mouse.get_pos()[1] > TILE_EDGE*3/4*8:
            self.debug.content = (("[game]",
                                   "fps={} (target={})".format(self.fps, self.target_fps),
                                   "updates={} (totalups={})".format(self.target_tick_updates,
                                                                     self.fps * self.target_tick_updates),
                                   "frame={}".format(self.frame),
                                   "  [player]",
                                   "pos={},{}".format(self.level.player.f_x, self.level.player.f_y),
                                   "vel={},{}".format(self.level.player.xvel, self.level.player.yvel),
                                   "nomove={}".format(self.level.player.nomove_frames),
                                   str(self.level.player.t_down)),
                                  ("[level]",
                                   "dir={}(maps={})".format(self.level_dir, len(self.level_queue)),
                                   "name={}(index={})".format(self.level.name, self.current_level_index)),
                                  ("O:god mode", "P:noclip mode", "[:previous level", "]:next level",
                                   "/:change framerate", "-:hide debug display", "L:test effect"),
                                  ("I:force death", "E:reverse gravity", "`:wireframe",
                                   "B:record macros", "0:force pause", "9:next frame", "U:disable level passing",
                                   "R:reload level"),
                                  ("\\:close debug display and disable cheats",),
                                  ("[events]",
                                   "scheduled={}".format(len(self.level.events)),
                                   "next={} (in={})".format(self.level.events[0].name, self.level.events[0].time)
                                   if len(self.level.events) > 0 else "none"),
                                  ("[macrorecording]",
                                   "enabled={}".format(state_str(self.macro_recorder is not None, "1/0")),
                                   "B:record", "N:play"),)
            self.debug.draw(self.screen)

        display.flip()

    def mouse_click_event(self, e):
        if self.tooltip is not None:
            if self.ui[0].rect.collidepoint(*e.pos):
                self.ui[0].lclick_event(e)
            self.tooltip.dismiss()
        else:
            for i in self.ui:
                if e.button == 1 and i.rect.collidepoint(*e.pos):
                    i.lclick_event(e)
                    break
            else:
                if self.menu is not None:
                    self.menu.click_event(e)
                else:
                    self.level.click_event(e)

    def mouse_motion_event(self, e):
        for i in self.ui:
            if i.rect.collidepoint(*e.pos):
                i.mouse_hover_event(e)
                break
            else:
                i.mouse_unhover_event(e)
        else:
            if self.menu is not None:
                self.menu.mouse_motion_event(e)
            else:
                self.level.mouse_motion_event(e)

    def toggle_context_help(self):
        self.context_help = not self.context_help

    def disable_context_help(self):
        self.context_help = False

    def show_tooltip(self, rect, text, typ='ui'):
        self.tooltip = ToolTip(self, rect, text, typ)
        self.disable_context_help()

    def draw_ruler(self):
        c = Color('#FFFF00')
        for x in range(MAP_SIZE[0]):
            draw.line(self.screen, c, (x * TILE_EDGE, 0), (x * TILE_EDGE, SCREEN_SIZE[1]))
        for y in range(MAP_SIZE[1]):
            draw.line(self.screen, c, (0, y * TILE_EDGE), (SCREEN_SIZE[0], y * TILE_EDGE))

    def end_level(self):
        self.update()
        if self.save is not None:
            self.save.endlevel_update()
        if self.macro_recorder is None:
            if not self.next_level():
                self.level.game_complete_box.enable()
                self.save.delete()
                config.death1mode_unlocked = True
                config.save()
        else:
            self.load_current_level()
            self.macro_recorder.play()

    def toggle_fullscreen(self):
        screen = display.get_surface()
        screensave = screen.convert()
        caption = display.get_caption()
        cursor = mouse.get_cursor()

        size = screen.get_size()
        flags = screen.get_flags()
        bits = screen.get_bitsize()

        try:
            display.quit()
            display.init()
            screen = display.set_mode(size, flags ^ FULLSCREEN, bits)
        except error:
            display.init()
            screen = display.set_mode(size, flags, bits)
            success = False
            log("[WARN] Failed to make full screen: No video mode large enough")
        else:
            success = True
        screen.blit(screensave, (0, 0))
        display.set_caption(*caption)
        key.set_mods(0)
        mouse.set_cursor(*cursor)

        if hasattr(self, 'level'):
            self.level.player.reset_g()
        self.screen = screen

        return success

    @staticmethod
    def exit(really=True, code=0):
        log("[INFO] Quitting pygame")
        quit()
        if really:
            log("[INFO] Ending proccess with code {}".format(repr(code)))
            exit(code)

    @staticmethod
    def crash():
        raise InterruptedError("Process was interrupted by unknown reason")
