from pygame import draw, Surface, Color, Rect
from collections import Iterable, OrderedDict
from math import ceil
from random import random
from .const import TILE_EDGE, MAP_SIZE, SCREEN_SIZE
from .font import init_font, rendertext, rendersize
from .locale import l
from .log import log
from .util import address_str


class GeneratedGraphic:
    def __init__(self, tile_color='#808080', platform_color='#808080', bg_tile_color='#FFFFFF', bg_color='#EBEBEB',
                 player_color='#485BFF', exit_bg_color='#DFDADA', exit_door_color='#C0C0C0', accent_color='#A6A6A6'):
        self.tile_color, self.platform_color, self.bg_tile_color = Color(tile_color), platform_color, bg_tile_color
        self.bg_color, self.player_color, self.exit_bg_color = Color(bg_color), player_color, exit_bg_color
        self.exit_door_color, self.accent_color = Color(exit_door_color), accent_color
        self.mannequin_color = '#707070'

        self.window_icon = filled_rect(player_color, (1 / TILE_EDGE, 1 / TILE_EDGE))

        self.tile_surface = draw_(tile_color, ((0, 0), (1, 0), (1, 1), (0, 1)))
        self.platform_surface = draw_(platform_color, ((0, 0), (1, 0), (1, 0.5), (0, 0.5)))
        self.bg_tile_surface = draw_(bg_tile_color, ((0, 0), (1, 0), (1, 1), (0, 1)))

        self.bg_surface = filled_rect(bg_color, MAP_SIZE)
        self.tls_surface = filled_rect(None, MAP_SIZE)

        self.mover_right = draw_multicolor(((accent_color, ((0, 0), (1, 0), (1, 1), (0, 1))),
                                            (tile_color, ((0, 0), (0.5, 0), (1, 0.5), (0.5, 1), (0, 1), (0.5, 0.5)))))
        self.mover_left = draw_multicolor(((accent_color, ((0, 0), (1, 0), (1, 1), (0, 1))),
                                           (tile_color, ((0.5, 0), (1, 0), (0.5, 0.5), (1, 1), (0.5, 1), (0, 0.5)))))

        self.spike_down = draw_(tile_color, ((0, 0), (1, 0), (0.5, 1)))
        self.spike_up = draw_(tile_color, ((0, 1), (1, 1), (0.5, 0)))
        self.spike_right = draw_(tile_color, ((0, 0), (0, 1), (1, 0.5)))
        self.spike_left = draw_(tile_color, ((1, 0), (1, 1), (0, 0.5)))

        self.moving_platform_3tiles = draw_(tile_color, ((0, 0), (3, 0), (3, 0.5), (2, 0.5), (2, 1), (1, 1), (1, 0.5),
                                                         (0, 0.5)), (3, 1))
        self.moving_platform_5tiles = draw_(tile_color, ((0, 0), (5, 0), (5, 0.5), (3, 0.5), (3, 1), (2, 1), (2, 0.5),
                                                         (0, 0.5)), (5, 1))

        self.rail_horiz = draw_(bg_tile_color, ((0, 0.7), (1, 0.7), (1, 1), (0, 1)))
        self.rail_vert = draw_(bg_tile_color, ((0.35, 0), (0.65, 0), (0.65, 1), (0.35, 1)))

        self.player_surface = player_surface(player_color)
        self.player_gib = draw_(player_color, ((0, 0), (0.666, 0), (0.666, 0.666), (0, 0.666)), (0.666, 0.666))

        self.exit_bg = filled_rect(exit_bg_color, (2, 4))
        self.exit_door = filled_rect(exit_door_color, (1.5, 4))
        self.exit_line = filled_rect(bg_color, (0.1, 4))
        self.exit_propack = draw_(bg_color, (((0, 0), (7, 0), (7, 2), (0, 2)),
                                             ((0, 0), (2, 0), (2, 6), (0, 6)),
                                             ((5, 0), (7, 0), (7, 6), (5, 6))), (7, 6))

        self.catapult_plate = draw_(tile_color, ((3, 0), (6, 0), (6, 0.5), (3, 0.5)), (6, 0.5))

        self.plasma_cannon = draw_(tile_color, ((0, 0), (3, 0), (2, 1), (1, 1)), (3, 1))

        self.platform_cuted = draw_(tile_color, ((0, 0), (1.5, 0), (1.5, 0.5), (0, 0.5)), (2, 1))

        self.greedy_spike = draw_(tile_color, (((0, 0.25), (1, 0.25), (0.5, 1.25)),
                                               ((0.375, 0), (0.65, 0), (0.65, 0.25), (0.375, 0.25))), (1, 1.25))
        self.greedy_stand = draw_(bg_color, ((0.375, 0.75), (0.65, 0.75), (0.65, 1), (0.375, 1)))

        self.graviter_horiz = draw_(exit_door_color, ((0, 0), (1, 0), (1, 0.3), (0, 0.3)))
        self.graviter_vert = draw_(exit_door_color, ((0, 0), (0.3, 0), (0.3, 1), (0, 1)))

        self.bullet = draw_(accent_color, ((0, 0), (1, 0), (1, 1), (0, 1)), (1, 1))
        self.bullet_debris = draw_(accent_color, ((0, 0), (1, 0), (1, 1), (0, 1)), (0.75, 0.75))
        self.cannon_riffle = draw_(tile_color, ((0, 2), (1, 2), (1, 4), (0, 4)), (1, 4))

        self.arcade_mask = arcade_mask((SCREEN_SIZE[0] * 2, SCREEN_SIZE[1] * 2))
        self.arcade_strips = (arcade_strip(2), arcade_strip(1), arcade_strip(4))

        init_font((1, 1.4, 2, 3, 4, 5, 6, 7, 8.5, 9, 10))

        self.pause_button = draw_(bg_color, (((0.5, 0.5), (0.9, 0.5), (0.9, 1.5), (0.5, 1.5)),
                                             ((1.1, 0.5), (1.5, 0.5), (1.5, 1.5), (1.1, 1.5))), (2, 2))
        self.help_button = draw_(bg_color, (((0.4, 0.4), (1.6, 0.4), (1.6, 0.6), (0.4, 0.6)),
                                            ((1.4, 0.6), (1.6, 0.6), (1.6, 1), (1.4, 1)),
                                            ((0.9, 1), (1.6, 1), (1.6, 1.2), (0.9, 1.2)),
                                            ((0.9, 1.4), (1.1, 1.4), (1.1, 1.6), (0.9, 1.6))), (2, 2))
        self.backtomenu_buttonl = button_imagetext((bg_color, (((0.8, 2), (2, 0.8), (3.2, 2)),
                                                               ((1.2, 2), (2.8, 2), (2.8, 3.2), (2.6, 3.2), (2.6, 2.4),
                                                                (2, 2.4), (2, 3.2), (1.2, 3.2))), (4, 4)),
                                                   l("Main Menu"))
        self.settings_buttonl = button_imagetext((bg_color, (((0.8, 3.2), (1, 3.2), (2.8, 1.6), (2.2, 1.4),
                                                              (0.8, 2.8)), ((2.4, 1.6), (2.8, 1.6), (2.8, 2), (2.4, 2)),
                                                             ((2.8, 1.6), (3.2, 1.2), (3.2, 1.6), (2.8, 2)),
                                                             ((2, 1.2), (2.4, 1.2), (2.4, 1.6), (2, 1.6)),
                                                             ((2, 1.2), (2.4, 1.2), (2.8, 0.8), (2.4, 0.8))), (4, 4)),
                                                 l("Settings"))
        self.walkthrough_buttonl = button_imagetext((bg_color, (((0.8, 0.8), (3.2, 0.8), (3.2, 1.2), (0.8, 1.2)),
                                                                ((2.8, 1.2), (3.2, 1.2), (3.2, 2), (2.8, 2)),
                                                                ((1.8, 2), (3.2, 2), (3.2, 2.4), (1.8, 2.4)),
                                                                ((1.8, 2.8), (2.2, 2.8), (2.2, 3.2), (1.8, 3.2))),
                                                     (4, 4)), l("Walkthrough"))
        self.restartlevel_buttonl = button_imagetext((bg_color, (), (4, 4)), l("Restart level"))
        self.resume_buttonl = button_imagetext((bg_color, ((0.8, 0.8), (3.2, 2), (0.8, 3.2)), (4, 4)), l("Resume"))
        self.ui_placeholder = draw_(bg_color, ((0.8, 0.8), (3.2, 0.8), (3.2, 3.2)), (2, 2))
        self.onl = button_imagetext((bg_color, (), (0, 4)), l("ON") + " ")
        self.offl = button_imagetext((bg_color, (), (0, 4)), l("OFF") + " ")

        self.soundl = ql(bg_color, " " + l("Sound"))
        self.qualityl = ql(bg_color, " " + l("Quality"))
        self.lowl = ql(bg_color, l("Low") + " ")
        self.mediuml = ql(bg_color, l("Medium") + " ")
        self.highl = ql(bg_color, l("High") + " ")
        self.resolutionl = ql(bg_color, " " + l("Resolution"))
        self.x512x288 = ql(bg_color, "512x288 ")
        self.x1024x576 = ql(bg_color, "1024x576 ")
        self.x1280x720 = ql(bg_color, "1280x720 ")
        self.x1536x864 = ql(bg_color, "1536x864 ")
        self.x1920x1080 = ql(bg_color, "1920x1080 ")
        self.fullscreenl = ql(bg_color, " " + l("Fullscreen"))
        self.controlsl = ql(bg_color, " " + l("Controls"))
        self.wasdl = ql(bg_color, l("WASD") + " ")
        self.arrowsl = ql(bg_color, l("arrow keys") + " ")
        self.languagel = ql(bg_color, " " + l("Language"))
        self.enl = ql(bg_color, l("English") + " ")
        self.pll = ql(bg_color, l("Polish") + " ")
        self.savel = ql(bg_color, " " + l("Save changes"))

        self.errorl = ql(bg_color, " " + l("Error"))
        self.toobigresolutionforfullscreenl = ql(bg_color, " " + l("Too big resolution for fullscreen"))
        self.okl = ql(bg_color, " " + l("OK"))

        self.play_buttonl = button_imagetext((bg_color, ((0.8, 0.8), (3.2, 2), (0.8, 3.2)), (4, 4)), l("Play"))
        self.credits_buttonl = button_imagetext((bg_color, (), (4, 4)), l("Credits"))
        self.exit_buttonl = button_imagetext((bg_color, (), (4, 4)), l("Exit"))
        self.new_save_buttonl = button_imagetext((bg_color, (((1.8, 0.8), (2.2, 0.8), (2.2, 3.2), (1.8, 3.2)),
                                                             ((0.8, 1.8), (0.8, 2.2), (3.2, 2.2), (3.2, 1.8))), (4, 4)),
                                                 l("New game"))
        self.deletelevell = button_imagetext((bg_color, (), (4, 4)), l("Delete game"))
        self.deletelevell_d = button_imagetext((accent_color, (), (4, 4)), l("Delete game"))
        self.backl = button_imagetext((bg_color, (((0.8, 1.8), (0.8, 2.2), (3.2, 2.2), (3.2, 1.8)),
                                                  ((1.8, 0.8), (1.8, 1.2), (1.2, 1.8), (0.8, 1.8)),
                                                  ((1.8, 3.2), (1.8, 2.8), (1.2, 2.2), (0.8, 2.2))), (4, 4)), l("Back"))
        self.cancel_buttonl = button_imagetext((bg_color, (((0.8, 3.2), (1.15, 3.2), (3.2, 1.15), (3.2, 0.8),
                                                            (2.85, 0.8), (0.8, 2.85)),
                                                           ((0.8, 0.8), (1.15, 0.8), (3.2, 2.85), (3.2, 3.2),
                                                            (2.85, 3.2),
                                                            (0.8, 1.15))), (4, 4)), l("Cancel"))

        self.choosegamemodel = ql(bg_color, " " + l("Choose a game mode"))
        self.normall = ql(bg_color, " " + l("Normal"))
        self.death1l = ql(bg_color, " " + l("1 Death"))
        self.death1l_d = ql(accent_color, " " + l("1 Death"))
        self.vvvvvvl = ql(bg_color, " vvvvvv")

        self.death1modenotunlockedl = ql(bg_color, " " + l("1 Death Mode is not unlocked yet."))
        self.death1modenotunlockedl2 = ql(bg_color, " " + l("Complete game in normal mode"))

        self.youlosel = ql(bg_color, " " + l("You lost in 1 death mode!"))
        self.retry_buttonl = button_imagetext((bg_color, (), (4, 4)), l("Retry"))

        self.youcompletedgamel = ql(bg_color, " " + l("You completed the game!"))

        self.cannotcreatemoregamesl = ql(bg_color, " " + l("Cannot create more games"))
        self.nogamestodeletel = ql(bg_color, " " + l("No games to delete"))

        n1, n2 = self.get_nerdy_stats()
        log("[INFO] GeneratedGraphic instance {} created with {} image(s) taking about {} bytes of memory".format(
            address_str(self), n1, round(n2 * 3, -3)))

    def get_bg_surface(self, size):
        return filled_rect(self.bg_color, size)

    def get_nerdy_stats(self):
        attrs = (getattr(self, x) for x in dir(self))
        surfattrs = list(filter(lambda x: isinstance(x, Surface), attrs))
        num_surfaces = len(surfattrs)  # number of surfaces
        total_size = 0  # total area of all surfaces
        for x in surfattrs:
            r = x.get_size()
            total_size += r[0] * r[1]
        return num_surfaces, total_size


def to_color(color):
    if isinstance(color, Color):
        return color
    else:
        return Color(color)


def player_surface(color):
    # separate function to allow drawing players in different colors
    s = filled_rect(color, (1.5, 1.5))
    draw.rect(s, Color(color_variant(color, -12)), Rect(0, 0, s.get_width(), s.get_height()), TILE_EDGE // 16)
    return s


def draw_(color, pointlists, size=(1, 1), alpha=255):
    """draw(color, pointlists: (((x1, y1), (x2, y2), ...), ...), size=(1, 1): (x, y), alpha=255) -> Surface"""
    surface = Surface((TILE_EDGE * size[0], TILE_EDGE * size[1]))
    surface.fill((255, 0, 255))
    surface.set_colorkey((255, 0, 255))
    surface.set_alpha(alpha)
    if len(pointlists) != 0:
        if not isinstance(pointlists[0][0], Iterable):
            pointlists = (pointlists,)
        for pointlist in pointlists:
            draw.polygon(surface, Color(color),
                         [(i[0] * TILE_EDGE, i[1] * TILE_EDGE) for i in pointlist if i not in ((), None)])
    return surface.convert()


def draw_multicolor(pointdict, size=(1, 1), alpha=255):
    """draw_multicolor(pointdict: {color: ((x1, y1), (x2, y2), ...), ...}, size=(1, 1): (x, y), alpha=255)"""
    surface = Surface((TILE_EDGE * size[0], TILE_EDGE * size[1]))
    surface.fill((255, 0, 255))
    surface.set_colorkey((255, 0, 255))
    surface.set_alpha(alpha)
    for color, pointlist in OrderedDict(pointdict).items():
        draw.polygon(surface, Color(color), [(i[0] * TILE_EDGE, i[1] * TILE_EDGE) for i in pointlist])
    return surface.convert()


def filled_rect(color, size=(1, 1)):
    """filled_rect(color, size=(1, 1): (x, y)) -> Surface, None color for transparent"""
    surface = Surface((size[0] * TILE_EDGE, size[1] * TILE_EDGE))
    surface.fill(to_color(color) if color is not None else (255, 0, 255))
    if color is None:
        surface.set_colorkey((255, 0, 255))
    return surface.convert()


def spiked_moving_block(graphic_instance, size=(1, 1)):
    size = (size[0] * TILE_EDGE, size[1] * TILE_EDGE)
    surface = Surface(size)
    surface.fill((255, 0, 255))
    surface.set_colorkey((255, 0, 255))
    draw_tile_pattern(surface, graphic_instance.spike_left, Rect(0, 0, TILE_EDGE, size[1]))
    draw_tile_pattern(surface, graphic_instance.spike_right, Rect(size[0] - TILE_EDGE, 0, TILE_EDGE, size[1]))
    draw_tile_pattern(surface, graphic_instance.tile_surface, Rect(TILE_EDGE, 0, size[0] - TILE_EDGE * 2, size[1]))
    return surface.convert()


def draw_tile_pattern(dest, surface, rect):
    if surface is not None:
        size = (ceil(rect.width / TILE_EDGE), ceil(rect.height / TILE_EDGE))
        s = filled_rect(None, size)
        for x in range(size[0]):
            for y in range(size[1]):
                s.blit(surface, (x * TILE_EDGE, y * TILE_EDGE))
        dest.blit(s, rect)


def color_variant(hex_color, brightness_offset=1):
    if len(hex_color) != 7:
        raise ValueError("Invalid hex color code: {}".format(hex_color))
    rgb_hex = [hex_color[x:x + 2] for x in (1, 3, 5)]
    new_rgb_int = [int(hex_value, 16) + brightness_offset for hex_value in rgb_hex]
    new_rgb_int = [min([255, max([0, i])]) for i in new_rgb_int]
    return "#" + "".join([hex(int(i))[2:] for i in new_rgb_int])


def arcade_mask(size=SCREEN_SIZE):
    s = Surface(size)
    s.lock()
    s.fill(Color('#FFFFFF'))
    c = Color('#704214')
    for i in range(size[1] * 4):
        draw.line(s, c, (0, i * TILE_EDGE / 4), (size[0], i * TILE_EDGE / 4), TILE_EDGE // 8)
    s.set_alpha(0.5 * 256)
    s.unlock()
    return s.convert()


def arcade_strip(height=32):
    s = Surface((SCREEN_SIZE[0], height * TILE_EDGE))
    s.fill(Color('#000000'))
    s.set_alpha(0.3 * 256)
    return s.convert()


def arcade_noise(w=MAP_SIZE[0] * 2, h=MAP_SIZE[1] * 2):
    s = Surface((w * TILE_EDGE, h * TILE_EDGE))
    s.lock()
    s.fill(Color('#000000'))
    c = Color('#FFFFFF')
    for x in range(w * TILE_EDGE // 4):
        for y in range(h * TILE_EDGE // 4):
            if random() < 0.5:
                draw.rect(s, c, Rect(x * 4, y * 4, 4, 4))
    s.set_alpha(0.2 * 255)
    s.unlock()
    return s.convert()


def button_imagetext(draw_arguments, text, size=3):
    s1 = draw_(*draw_arguments)
    s2 = rendertext(text, size, to_color(draw_arguments[0]))
    s = Surface((s1.get_width() + s2.get_width(), s1.get_height()))
    s.fill((255, 0, 255))
    s.set_colorkey((255, 0, 255))
    s.blit(s1, (0, 0))
    s.blit(s2, (s1.get_width(), TILE_EDGE * 0.5))
    return s.convert()


def ql(col, text, size=3):
    return button_imagetext((col, (), (0, max(size, 2))), text, size)


def get_tooltip(text, col=Color('#808080'), bgcol=Color('#F2F2F2'), bordercol=Color('#CCCCCC')):
    w = int(max(rendersize(line, 1)[0] for line in text.splitlines()) + TILE_EDGE * 2)
    h = (text.count("\n") + 1) * TILE_EDGE * 1 + TILE_EDGE
    s = Surface((w, h))
    s.fill(bgcol)
    draw.rect(s, bordercol, Rect(0, 0, w - TILE_EDGE // 16, h - TILE_EDGE // 16), TILE_EDGE // 8)
    s.blit(rendertext(text, 1, col, bgcol, w - TILE_EDGE * 2), (TILE_EDGE, TILE_EDGE / 2))
    return s


def get_textbaloon(text, col=Color('#808080'), bgcol=Color('#F2F2F2')):
    w = int(max(rendersize(line, 1)[0] for line in text.splitlines() + [l("Press any key or click to continue")]) +
            TILE_EDGE * 2)
    h = (text.count("\n") + 1 + 1.5) * TILE_EDGE + TILE_EDGE
    s = Surface((w, h))
    s.fill(bgcol)
    s.blit(rendertext(text, 1, col, bgcol, w - TILE_EDGE * 2), (TILE_EDGE, TILE_EDGE / 2))
    return s


def get_logo(col, w=TILE_EDGE * 25.5):
    s = Surface((w, TILE_EDGE * 11))
    s.fill((255, 0, 255))
    s.set_colorkey((255, 0, 255))
    s.blit(rendertext("piexel", 8.5, col), (0, 0))
    s.blit(rendertext("the game", 4, col), (0, TILE_EDGE * 7))
    return s
