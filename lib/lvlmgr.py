from pygame import display, draw, Color
from itertools import chain
from collections import Iterable, OrderedDict
from .const import MAP_SIZE, TILE_EDGE, SCREEN_SIZE
from .util import log, address_str
from .entities import EntityGroup
from .graphic import filled_rect, ql, GeneratedGraphic
from .io import draw_io
from .testelem import std
from .lll import extract_level_name, parse_tiledef, parse_level_save, parse_macro, reduce_all
from .ui import Button, Label
from .uit import pause_menu, settings_menu, death1lose_menu, game_complete_box, MainMenu
from .config import config, QMEDIUM
from .cursors import set_loading
from .locale import l, m
from .solt import get_solution


class Event:
    def __init__(self, time, callback, name=None):
        self.time = time
        self.callback = callback
        self.name = name if name is not None else callback.__name__

    def __repr__(self):
        return "<Event {}>".format({'time': self.time, 'callback': self.callback, 'name': self.name})

    def __call__(self, level=None):
        if level is not None:
            self.callback.__call__(level)
        else:
            self.callback.__call__()

    call = __call__


class Level:
    __name__ = "Level"  # code inspection workaround

    def __init__(self, game, filepath, def_filepath, macro_filepath=None, overlay_effect=None, max_entities=300):
        set_loading()

        self.game = game
        self.graphic = game.graphic
        display.set_icon(self.graphic.window_icon)

        self.events = []

        self.entities = EntityGroup()
        self.max_entities = max_entities

        self.paused = False

        self.player = self.entrance = self.exit = None

        self.blocks = []  # <Block>
        self.platforms = []  # <Platform>
        self.barrier_blocks = []  # <Rect>
        self.spikes = []
        self.triggers = []  # <TriggerKill>

        self.moving_platforms = []  # <MovingPlatform>
        self.movin_platforms = []
        self.moving_blocks = []  # <MovingBlock>
        self.particles = []  # <Particle>
        self.catapults = []  # <Catapult>
        self.collapsing_platforms = []  # <CollapsingPlatform(active)>
        self.movers = []  # <Mover>
        self.greedy_spikes = []  # <GreedySpike>
        self.graviters = []  # <GraviterVertical, GraviterHorizontal>
        self.cannons = []  # <PlasmaCannon>
        self.bullets = []  # <Bullet>
        self.moving_spikes = []  # <MovingSpike>
        self.living_objects = []
        self.removed_elements = []  # <CollapsingPlatform(collapsed)>
        self.updatables = []

        self.omni_circle = None

        self.balloons = []

        self.afterdraw = []  # objects which need to be drawed after other elements

        self.path = filepath
        self.name = extract_level_name(self, filepath)
        self.sizex, self.sizey = 64, 36

        self.view = filled_rect('#000000', (self.sizex, self.sizey))  # empty surface: ignore hard-coded color
        self.bg = self.graphic.get_bg_surface((self.sizex, self.sizey))

        self.macro = parse_macro(self, macro_filepath)
        self.macrofunc('preinit')

        self.data = parse_tiledef(self, def_filepath)
        parse_level_save(self, filepath)

        if self.exit is None:
            log(" [WARN] No exit from level found: press ']' to go to next level")
        if self.player is None:
            log(" [ERR ] No player on map")

        reduce_all(self.blocks, self.triggers, self.barrier_blocks, self.movers, self.platforms, self.graviters,
                   self.moving_spikes, self.moving_blocks, self.spikes, self.movin_platforms)

        for e in self.afterdraw:
            e.afterdraw()
        del self.afterdraw

        if 'PreventScroll' in self.data['Label']:
            game.anim.disable()
            if self.exit is not None:
                self.exit.force_open()

        self.macrofunc('init')

        if game.macro_recorder is not None:
            log(" [INFO] Resetting macro recorder timestamps")
            game.macro_recorder.time = 0

        self.overlay_effect = overlay_effect

        self.settings_menu = settings_menu(self)
        self.pause_menu = pause_menu(self)
        self.death1lose_menu = death1lose_menu(self)
        self.game_complete_box = game_complete_box(self)

        self.error_box = None

        self.ui = [Button(self, SCREEN_SIZE[0] - TILE_EDGE * 2, 0, TILE_EDGE * 2, TILE_EDGE * 2, self.toggle_pause,
                          self.graphic.pause_button, description="pause"),
                   Label(self, TILE_EDGE*2, 0, TILE_EDGE*32, TILE_EDGE*2)]
        if self.game.save is not None and self.game.save.gamemode == '1death':
            self.update_deathscount_label()
        else:
            del self.ui[1]

        self.force_drawing_view = False
        self.draw_by_view = True

        self.solved_with_cheating = False
        self.solving = False

        self.player.say(*m(self.game.current_level_index))

        log("[INFO] Loading map finished: Level instance {} created with {} static, {} dynamic rectangle(s)".format(
            address_str(self), len(list(chain(self.blocks, self.spikes, self.platforms))), len(self.get_all())))

    def get_all(self):
        # all elements which need to be updated and drawed (except dialogs), keeping order
        return list(chain(self.movers, self.graviters, self.moving_platforms, self.moving_blocks, self.moving_spikes,
                          self.catapults, self.collapsing_platforms, self.bullets, self.cannons,
                          self.living_objects, self.movin_platforms, self.updatables, self.greedy_spikes,
                          self.particles, self.balloons, (self.omni_circle,) if self.omni_circle is not None else ()))

    def update(self):

        all_ = self.get_all()

        if self.game.anim.x.val == 0 and self.game.anim.y.val == 0 and not self.force_drawing_view:
            d = self.game.screen
            self.view = None
            self.draw_by_view = False
        else:
            if self.view is None:
                self.view = filled_rect('#000000', (self.sizex, self.sizey))
            d = self.view
            self.draw_by_view = True or config.q > QMEDIUM

        if self.paused:
            if config.q > QMEDIUM:  # don't draw scene if quality is low while game is paused
                self.draw_scene(d, all_)
        else:  # update only if level is not paused
            self.macrofunc('preupdate')
            for u in all_:
                u.update()
            self.entrance.update()
            if self.exit is not None:
                self.exit.update()
            self.player.update()
            for e in chain(self.triggers, self.catapults, self.graviters):
                e.update()
            self.macrofunc('update')

            self.draw_scene(d, all_)

        if self.overlay_effect is not None:  # effect should be drawed after in-game buttons but before dialogs
            self.overlay_effect.update()
            self.overlay_effect.draw(d)

        for i in (self.pause_menu, self.settings_menu, self.death1lose_menu, self.game_complete_box, self.error_box):
            if i is not None:
                i.update()
                i.draw(d)

        if not self.balloons:  # pseudo-pause when dialog is opened
            self.update_events()

        self.game.save.frames += 1

    def draw_scene(self, d, all_):
        d.blit(self.bg, (0, 0))

        self.macrofunc('predraw')
        draw_io(d, self, all_)  # also draws game objects
        self.macrofunc('draw')

        for i in self.ui:
            i.update()
            i.draw(d)

    def draw_debug_overlay(self):
        d = self.game.screen
        bg = Color('#FFFFFF')
        for color, list_ in zip(('#FF0000', '#00FF00', '#0000FF', '#FFFF00', '#00FFFF', '#FFF000', '#FF0000',
                                 '#FFFFAA'),
                                (self.blocks, self.triggers, self.platforms, self.barrier_blocks, self.spikes,
                                 chain(self.greedy_spikes, self.collapsing_platforms, self.movers, self.moving_spikes,
                                       self.moving_platforms, self.graviters, self.cannons, self.living_objects),
                                 self.moving_blocks, self.movin_platforms)):
            color = Color(color)
            for b in list_:
                if hasattr(b, 'outer_rect'):
                    attr = 'outer_rect'
                else:
                    attr = 'rect'
                draw.rect(d, bg, getattr(b, attr))
                draw.rect(d, color, getattr(b, attr), 1)
        t = Color('#FF00FF')
        for h in self.cannons:
            draw.line(d, t, self.player.rect.center, h.rect.center)

    def get_ui(self):
        if self.error_box is not None:
            i = (self.error_box,)
        elif self.death1lose_menu.active:
            i = (self.death1lose_menu,)
        elif self.game_complete_box.active:
            i = (self.game_complete_box,)
        elif self.paused:
            if self.settings_menu.active:
                i = (self.settings_menu,)
            else:
                i = (self.pause_menu,)
        else:
            i = self.ui
        return i

    def click_event(self, event):
        if event.button == 1:
            func = 'lclick_event'
        elif event.button == 2:
            func = 'mclick_event'
        elif event.button == 3:
            func = 'rclick_event'
        else:
            func = None
        if func is not None:
            for i in self.get_ui():
                if i.rect.collidepoint(*event.pos) and hasattr(i, func):
                    getattr(i, func).__call__(event)
                    break
            else:
                if func == 'lclick_event':
                    if self.game.context_help and not self.paused:
                        r, s = self.get_game_object_tooltip(event.pos)
                        if r is not None and s is not None:
                            self.game.show_tooltip(r, s, 'el')
                    elif self.balloons and not self.game.context_help:
                        for i in self.balloons:
                            i.dismiss()
                        return

    def mouse_motion_event(self, event):
        for i in self.get_ui():
            if i.rect.collidepoint(*event.pos) and hasattr(i, 'mouse_hover_event'):
                i.mouse_hover_event(event)
            elif hasattr(i, 'mouse_unhover_event'):
                i.mouse_unhover_event(event)

    def get_game_object_tooltip(self, pos):
        x, y = pos
        r = s = None
        if self.entrance.rect.collidepoint(x, y):
            r = self.entrance.rect
            s = "entrance"
        if self.exit.rect.collidepoint(x, y):
            r = self.exit.rect
            s = "lexit"

        for t, n in zip((chain(self.spikes, self.moving_spikes, self.greedy_spikes),
                         chain(self.platforms, self.moving_platforms, self.movin_platforms),
                         chain(self.blocks, self.moving_blocks),
                         self.movers,
                         self.graviters,
                         self.catapults,
                         self.collapsing_platforms,
                         self.cannons,
                         self.balloons),
                        ("spike",
                         "platform",
                         "block",
                         "mover",
                         "graviter",
                         "catapult",
                         "collapsingplatform",
                         "cannon",
                         "dialogballoon")):
            for i in t:
                if i.rect.collidepoint(x, y):
                    r, s = i.rect, n

        if self.player.rect.collidepoint(x, y):
            r = self.player.rect
            s = "player"

        return r, s

    def end(self):
        self.macrofunc('preendlevel')
        if self.game.allow_level_passing:
            self.game.end_level()
        else:
            self.game.load_current_level()  # reload level

    def macrofunc(self, name):
        if self.macro is not None:
            try:
                f = getattr(self.macro, name)
            except AttributeError:
                pass
            else:
                f.__call__(level=self, std=std)

    def schedule_event(self, time, callback, name=None):
        self.events.append(Event(time, callback, name))

    def cancel_event(self, name):
        for i, e in enumerate(self.events):
            if e.name == name:
                del self.events[i]
                return

    def update_events(self):
        for e in self.events.copy():
            e.time -= 1
            if e.time <= 0:
                e.call()
                self.events.remove(e)

    def sort_events(self):
        self.events.sort(key=lambda item: item.time)

    def pause(self):
        self.paused = True
        self.pause_menu.enable()

    def unpause(self):
        self.paused = False
        self.pause_menu.disable()

    def toggle_pause(self):
        if self.paused:
            self.unpause()
        else:
            self.pause()

    def del_error(self):
        self.error_box = None

    def dismiss_error(self):
        if self.error_box is not None:
            self.error_box.disable()

            # self.del_error HAS __name__ attribute
            # noinspection PyTypeChecker
            self.schedule_event(self.error_box.anim.time, self.del_error)

    def back_to_menu(self):
        self.pause()
        self.game.menu = MainMenu(self.game)

    def schedule_dialog(self, dialog_ord_dict, bind_movement=True):
        """schedule_dialog(((attr: str, dialog_id: str), ...), bind_movement=True)"""
        if self.game.save.gamemode == '1death':
            return

        if bind_movement:
            self.player.disable_movement()

        i = 0
        for k, v in dialog_ord_dict:
            o = getattr(self, k)

            v = v if isinstance(v, Iterable) and not isinstance(v, str) else (v,)
            o.say(*m(v[0]), bind_movement=False, _i_start=i)

            i += len(v) * 2

        if bind_movement:
            self.schedule_event(i + 1, self.player.enable_movement)

    def update_deathscount_label(self):
        if self.game.save.gamemode == '1death':
            i = self.game.save.currentleveldeaths
            self.ui[1].image = ql(self.graphic.bg_color, "{} {} {} ".format(i, l("deaths") if i != 1 else l("death"),
                                                                            l("on this level")), 1.4)
            self.ui[1].rect_from_image()
            self.ui[1].rect.right = SCREEN_SIZE[0] - TILE_EDGE * 2

    def autosolve(self):
        self.solved_with_cheating = True
        self.solving = True

        self.reset_to_initial()
        self.player.goto_entrance()
        self.player.schedule_motion(get_solution(self.game.current_level_index))

        self.unpause()

    def reset_to_initial(self):
        for i in self.cannons:
            i.current_angle = 0


class LevelPlaceholder:
    solved_with_cheating = False

    def __init__(self, *ignore, **kw):
        self.view = filled_rect(kw['color'] if 'color' in kw else '#000000', MAP_SIZE)

    def update(self):
        pass
