def quine():
    s = 'def quine():\n    s = {}\n    print(s.format(repr(s)))'
    print(s.format(repr(s)))


def cheat_quine():
    with open(__file__) as source:
        s = str(source.read())
        i = s.find('def cheat_quine')
        print(s[i:i+148])


def quine_file():
    s = "def quine():\n    s = 'def quine():\\n    s = {{}}\\n    print(s.format(repr(s)))'\n    print(s.format(repr(s)))\n\n\ndef cheat_quine():\n    with open(__file__) as source:\n        s = str(source.read())\n        i = s.find('def cheat_quine')\n        print(s[i:i+148])\n\n\ndef quine_file():\n    s = {s}\n    print(s.format(s=repr(s)))"
    print(s.format(s=repr(s)))


def cheat_quine_file():
    print(str(open(__file__).read()))
