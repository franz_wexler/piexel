from pygame import draw, Rect, Surface
from itertools import chain
from random import random
from .const import TILE_EDGE
from .config import config, QMEDIUM
from .testbase import Lie, SpikeUp, SpikeDown, SpikeLeft, SpikeRight, Block
from .util import blit_rotated, expt, exps, randd, tiledv
from .entities import Entity, GravityDynamic, draw_patternized
from .particles import FunctionalParticle
from .easeanim import Anim2D, ease_in_out
from .graphic import draw_tile_pattern, spiked_moving_block


__all__ = ['MovingPlatform', 'RailHorizontal', 'RailVertical',
           'Catapult', 'CollapsingPlatform', 'Mover', 'GreedySpike', 'GreedySpikeStand',
           'GraviterVertical', 'GraviterHorizontal', 'LiedGraviterVertical', 'LiedGraviterHorizontal',
           'GraviterSpikeDown', 'GraviterSpikeUp', 'GraviterSpikeLeft', 'GraviterSpikeRight']


class MovingPlatform(Entity):
    __slots__ = ['f_x', 'f_y', 'xvel', 'yvel', 'outer_rect']

    def __init__(self, level, x, y, w, xvel, yvel=0):
        """MovingPlatform(level, graphic, posx, posy, width, xvel, yvel=0) -> MovingPlatform

        Important: Platforms with yvel different than 0 WON'T WORK properly
        """
        graphic = level.graphic.moving_platform_3tiles if w < TILE_EDGE * 5 else level.graphic.moving_platform_5tiles
        Entity.__init__(self, level, graphic, (x, y, w, TILE_EDGE/2-1))
        self.f_x, self.f_y = x, y
        self.xvel, self.yvel = tiledv(xvel), tiledv(yvel)
        self.outer_rect = Rect(x, y - 1, w, TILE_EDGE/2+2)  # rect is used to check if entity is on platform

        level.bg.blit(level.graphic.rail_horiz if xvel != 0 else level.graphic.rail_vert, (x, y))

        level.moving_platforms.append(self)

    def update(self):
        self.f_x += self.xvel
        self.rect.x = self.f_x
        self.outer_rect.x = self.f_x
        for b in chain(self.level.blocks, self.level.barrier_blocks):
            if b.rect.colliderect(self.rect):
                if self.xvel > 0:
                    self.rect.right = b.rect.left
                elif self.xvel < 0:
                    self.rect.left = b.rect.right
                self.f_x = self.rect.left
                self.xvel = -self.xvel
                break

        self.f_y += self.yvel
        self.rect.y = self.f_y
        self.outer_rect.y = self.f_y - 1
        for b in chain(self.level.blocks, self.level.barrier_blocks):
            if b.rect.colliderect(self.rect):
                if self.yvel > 0:
                    self.rect.bottom = b.rect.top
                elif self.yvel < 0:
                    self.rect.top = b.rect.bottom
                self.yvel = -self.yvel
                return


class RailHorizontal(Lie):
    texture = 'rail_horiz'


class RailVertical(Lie):
    texture = 'rail_vert'


class Catapult(Entity):
    __slots__ = ['xjump', 'yjump', 'direction', 'anim_current_angle', 'anim_step', 'anim_end', 'anim_state']

    def __init__(self, level, x, y, xjump, yjump, animation_speed=2):
        Entity.__init__(self, level, level.graphic.catapult_plate, (x, y - 1, TILE_EDGE * 3, TILE_EDGE))
        self.xjump, self.yjump = tiledv(xjump), tiledv(yjump)
        self.direction = 'left' if xjump < 0 else 'right'

        inspector = GravityDynamic(self.level, None, self.rect.centerx, self.rect.y - 1, 1, 1,
                                   gravity=0.3, air_friction=0.5)
        inspector.b_xvel, inspector.b_yvel = self.xjump, self.yjump
        i = 0
        # note: first part of condition doesn't always work
        while (not (inspector.t_left or inspector.t_right or inspector.t_down)) and i < 120:
            h = inspector.rect.center
            inspector.update()
            if i % 6 < 3:
                draw.line(self.level.bg, (255, 255, 255), h, inspector.rect.center, 4)
            i += 1

        self.anim_current_angle = 0
        self.anim_step = animation_speed
        self.anim_end = 60
        self.anim_state = 0

        level.bg.blit(level.graphic.tile_surface, (x, y))

        level.catapults.append(self)

    def update(self):
        if self.rect.colliderect(self.level.player.rect):
            self.anim_state = 1
            self.level.player.rect.bottom = self.rect.top
            self.level.player.b_xvel = self.xjump
            self.level.player.b_yvel = self.yjump
        if self.anim_state == 1:
            if self.anim_current_angle >= self.anim_end:
                self.anim_state = 2
            else:
                self.anim_current_angle += self.anim_step * 2
        elif self.anim_state == 2:
            if self.anim_current_angle <= 0:
                self.anim_state = 0
            else:
                self.anim_current_angle -= self.anim_step

    def draw(self, dest):
        if self.direction == 'left':
            blit_rotated(dest, self.image, (self.rect.x, self.rect.y + TILE_EDGE * 0.25 + 1), self.anim_current_angle)
        elif self.direction == 'right':
            blit_rotated(dest, self.image, (self.rect.right, self.rect.y + TILE_EDGE * 0.25 + 1),
                         180 - self.anim_current_angle)


class CollapsingPlatform(Entity):
    __slots__ = ['collapse_delay', 'collapse_delay_left', 'anim_frame', 'anim_step']

    def __init__(self, level, x, y, collapse_delay=8):
        """CollapsingPlatform(level, x, y, **kw) -> CollapsingPlatform"""
        Entity.__init__(self, level, level.graphic.platform_cuted.copy(), (x, y, tiledv(24), tiledv(8)))
        self.collapse_delay = collapse_delay
        self.collapse_delay_left = collapse_delay

        self.anim_frame = 255  # fade-in
        self.anim_step = 255 / 20 + random() * 2 * randd()

        level.collapsing_platforms.append(self)

    def update(self):
        if self.collapse_delay_left < self.collapse_delay:
            self.collapse_delay_left -= 1
        if self.collapse_delay_left <= 0:
            self.kill()

        if self.anim_frame < 255:
            self.anim_frame += self.anim_step
            if self.anim_frame > 255:
                self.anim_frame = 255
        self.image.set_alpha(self.anim_frame)

    def on_step(self, obj=None):
        self.collapse_delay_left -= 1

    def kill(self):
        self.level.collapsing_platforms.remove(self)
        self.level.removed_elements.append(self)

        d = expt(self.level.player.xvel, None)
        if d is None:
            d = exps(self.level.player.rect.centerx > self.rect.centerx)
        d *= 2
        self.level.entities.add(FunctionalParticle(self.level, self.image.copy(), self.rect.x, self.rect.y,
                                                   TILE_EDGE * 1.5, TILE_EDGE * 0.5,
                                                   0, 0, 4, d, gravity=0.2, lifetime=100, shadeout=True))

    def revive(self):
        if self in self.level.removed_elements:
            self.level.removed_elements.remove(self)
            self.level.collapsing_platforms.append(self)

        if self.collapse_delay_left < self.collapse_delay:
            self.anim_frame = 0
            if config.q <= QMEDIUM:
                self.anim_frame = 255

            self.collapse_delay_left = self.collapse_delay


class Mover(Entity):
    __slots__ = ['direction', 'outer_rect', 'xvel', 'anim_step', 'anim_frame', 'anim_max']

    def __init__(self, level, x, y, vel):
        self.direction = 'left' if vel < 0 else 'right'
        Entity.__init__(self, level, level.graphic.mover_left if vel < 0 else level.graphic.mover_right,
                        (x, y, TILE_EDGE, TILE_EDGE))
        self.outer_rect = Rect(x, y - 1, TILE_EDGE, TILE_EDGE + 2)
        self.xvel = tiledv(vel)  # called xvel instead of vel because is used by DynamicEntity as xvel

        Block(level, x, y, TILE_EDGE, TILE_EDGE)

        level.movers.append(self)

        # code below is used only for fancy animation
        self.anim_step = tiledv(1 * exps(vel > 0))
        self.anim_frame = 0
        self.anim_max = TILE_EDGE

    def update(self):
        self.anim_frame += self.anim_step
        if self.xvel > 0 and self.anim_frame > self.anim_max:
            self.anim_frame = 0
        elif self.anim_frame < 0:
            self.anim_frame = self.anim_max

    def draw(self, dest):
        s = Surface((TILE_EDGE, TILE_EDGE))
        s.blit(self.image, (self.anim_frame - TILE_EDGE, 0))
        s.blit(self.image, (self.anim_frame, 0))
        s.blit(self.image, (self.anim_frame + TILE_EDGE, 0))  # blit self.image three times to create looped scroll
        draw_tile_pattern(dest, s, self.rect)

    def reduce_condition(self, other):
        return self.xvel == other.xvel

    def reduce_update(self):
        self.outer_rect.width = self.rect.width


class GreedySpike(Entity):
    __slots__ = ['init_y', 'anim_frame', 'anim_step', 'anim_direction']

    def __init__(self, level, x, y, vel=14):
        Entity.__init__(self, level, level.graphic.greedy_spike, (x, y, TILE_EDGE, TILE_EDGE))

        self.init_y = self.rect.y
        self.anim_frame = 0
        self.anim_step = tiledv(vel)
        self.anim_direction = 'none'

        GreedySpikeStand(level, x, y - TILE_EDGE)

        level.greedy_spikes.append(self)

    def update(self):
        if self.anim_direction == 'none' and (self.rect.left < self.level.player.rect.right and
                                              self.level.player.rect.left < self.rect.right and
                                              self.level.player.rect.y >= self.init_y):  # player is under the spike
            for i in chain(self.level.blocks, self.level.platforms, self.level.collapsing_platforms,
                           self.level.barrier_blocks):
                if (self.init_y < i.rect.y < self.level.player.rect.y and
                        i.rect.right > self.rect.left and i.rect.left < self.rect.right):
                    break
            else:  # no obstacles between player and spike
                self.anim_direction = 'down'  # target player
        elif self.anim_direction == 'down':
            self.anim_frame += self.anim_step
            for p in chain(self.level.blocks, self.level.platforms, self.level.barrier_blocks,
                           self.level.moving_platforms, self.level.movers, self.level.collapsing_platforms):
                if p.rect.colliderect(self.rect):  # reverse direction if collides with something solid
                    self.rect.bottom = p.rect.top
                    self.anim_frame = self.rect.y - self.init_y
                    self.anim_direction = 'up'
                    break
        if self.anim_direction == 'up':
            self.anim_frame -= self.anim_step / 3
            if self.rect.y <= self.init_y:  # back at initial position
                self.anim_direction = 'none'
                self.anim_frame = 0

        self.rect.y = self.init_y + self.anim_frame

    def draw(self, dest):
        dest.blit(self.image, (self.rect.x, self.rect.y - TILE_EDGE // 4 - 1))

    def fire(self):
        """Manually fire spike"""
        self.anim_direction = 'down'


class GreedySpikeStand(Lie):
    texture = 'greedy_stand'


class Graviter(Entity):
    __slots__ = ['level', 'rel_player']

    def __init__(self, level, graphic, x, y, w=TILE_EDGE, h=TILE_EDGE):
        Entity.__init__(self, level, graphic, (x, y, w, h))

        self.level = level

        self.rel_player = 0

        level.graviters.append(self)

    def draw(self, dest):
        draw_tile_pattern(dest, self.image, self.rect)


class GraviterVertical(Graviter):
    __slots__ = []

    def __init__(self, level, x, y, w=TILE_EDGE*0.3, h=TILE_EDGE):
        Graviter.__init__(self, level, level.graphic.graviter_vert, x+TILE_EDGE*0.35, y, w, h)

    def update(self):
        if self.level.player.rect.colliderect(self.rect):
            if self.rel_player == 0:
                self.rel_player = 1
                self.level.player.reverse_gravity()
                self.level.player.b_yvel = 0
        else:
            self.rel_player = 0


class LiedGraviterVertical(Lie):
    texture = 'graviter_vert'
    shift = (0.35*TILE_EDGE, 0)


class GraviterSpikeUp:
    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE):
        LiedGraviterVertical(level, x, y, w, h)
        SpikeUp(level, x, y, w, h)


class GraviterSpikeDown:
    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE):
        LiedGraviterVertical(level, x, y, w, h)
        SpikeDown(level, x, y, w, h)


class GraviterHorizontal(Graviter):
    __slots__ = []

    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE*0.3):
        Graviter.__init__(self, level, level.graphic.graviter_horiz, x, y+0.35*TILE_EDGE, w, h)

    def update(self):
        if self.level.player.rect.colliderect(self.rect):
            if self.rel_player == 0:
                self.rel_player = 1
                if expt(self.level.player.gravity) == expt(self.level.player.yvel):  # player is not jumping
                    self.level.player.reverse_gravity()
                self.level.player.b_yvel = 0
        else:
            self.rel_player = 0


class LiedGraviterHorizontal(Lie):
    texture = 'graviter_horiz'
    shift = (0, 0.35*TILE_EDGE)


class GraviterSpikeLeft:
    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE):
        LiedGraviterHorizontal(level, x, y, w, h)
        SpikeLeft(level, x, y, w, h)


class GraviterSpikeRight:
    def __init__(self, level, x, y, w=TILE_EDGE, h=TILE_EDGE):
        LiedGraviterHorizontal(level, x, y, w, h)
        SpikeRight(level, x, y, w, h)

